# Общая теория алгоритмов
## Лицензия
```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```

Полный текст лицензии ([LICENSE](/LICENSE.txt))

## Контактная информация
Авторы:

- Борисов Алексей Игоревич

   - Почта [aiborisov84@gmail.com](mailto:aiborisov84@gmail.com)
   - Доска публикаций [Хабр](https://habr.com/ru/users/ai_borisov/)
   - Telegram канал [@general_algorithm_theory](https://t.me/general_algorithm_theory)

- список открыт к пополнению...

## Вики

- [Полное описание теории проекта](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/wikis/home)

## Структура проекта

Исходные материалы книги

- Markdown теоретической книги (разбито по главам) `ai_borisov_CIT.git\book\BookTheory`
- Markdown черновиков публикаций (статей практической книги) `ai_borisov_CIT.git\book\BookPractice`
- Markdown записей рукописных материалов `ai_borisov_CIT.git\book\Notes`

Сверстанные материалы книги

- Публикуемый Markdown Wiki с объединенным оглавлением и индексом по терминам `ai_borisov_CIT.git\wiki`
- Автогенерируемый Markdown Wiki с объединенным оглавлением и индексом по терминам `ai_borisov_CIT.git\out\wiki`
- Автогенерируемый Html&Pdf с объединенным оглавлением и индексом по терминам `ai_borisov_CIT.git\out\.export`

Тестовые программные проекты

- Проекты моделирования работы _памяти_ _эвольвера_ на основе _цепочек_ `ai_borisov_CIT.git\prj\aipy_ChainModel`, `ai_borisov_CIT.git\prj\cm_football`

## Автоматизация верстки

- Верстка Wiki версии книги `ai_borisov_CIT.git\book\!scripts\Book.py theory_to_wiki`
- Верстка Pandoc↦Latex↦Pdf версии книги `ai_borisov_CIT.git\book\!scripts\Book.py theory_to_pdf --prepare-latex --override` используется в GitLab CI на основе образа `registry.gitlab.com/islandoftex/images/texlive:latest`
- Верстка Html↦Pdf версии книги `ai_borisov_CIT.git\book\!scripts\Book.py theory_to_pdf --override`
- Верстка Html↦Pdf версии отдельных глав (например главы 2.4) `ai_borisov_CIT.git\book\!scripts\Book.py theory_to_pdf theory_to_html --filter 2_4_ --override`