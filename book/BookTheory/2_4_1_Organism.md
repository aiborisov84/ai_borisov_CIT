#### 2.4.1. Организм

Чтобы указать, что _алгоритмическая система_ включает **алгоритм** _самокопирования_, будем использовать термин _организм_.

++_Организм_++[^2-4--organism] - **класс** _алгоритмической системы_, содержащей _внутреннее_ множество не _частых_ _работников_ и _материалов_, определяющих **алгоритм** $`\mathtt{AlgSelfCopy}`$, _процесс_ _исполнения_ которого из _материалов_, представленных _частыми_ _объектами_ _среды_, формирует новое множество _объектов_ _эквивалентное_ исходному _внутреннему_ множеству _работников_ и _материалов_. _Внутреннее_ множество _объектов_, составляющих _организм_, будем обозначать префиксом $`\mathbb{Org}`$.

<important>

Сформировав _организм_ вселенная «**сохранилась**» и начала исследования по поиску эффективных и _полезных_ методов изменения _организмов_, демонстрируя первое применение программной технологии «наследование». Для отрасли программных технологий текущего времени будет _полезен_ разбор и пополнение арсенала приемами, обнаруженными в ходе этих естественных исследований. Вероятно, знание этих приемов будет _полезно_ и специалистам других областей, создающим и модифицирующим **алгоритмы** в широком значении, предлагаемом текущей работой.

</important>

<todo>

Разобрать на основании каких **признаков** **пространства** может быть объяснено то, что некоторые усложнения _конструкции_ _организма_ оказываются _полезными_ изменениями.

</todo>

Для упрощения дальнейших записей необходимо ввести отношение ++_Трансформация_++($`\mathbf{Transform}`$), свидетельствующее об изменении _связей_ внутри множества _объектов_:

```math
\begin{gathered}
\mathbf{Transform}(\mathbb{Src}_{\mathbf{t1}}, \mathbb{Dst}_{\mathbf{t2}})
{\ \equiv}\\
{\equiv\ }
\overline{\mathbb{Src}_{\mathbf{t1}} \sim \mathbb{Dst}_{\mathbf{t2}}}
{\ \land\ }
{\langle \mathbb{Src}_{\mathbf{t1}} : \mathbf{Elm} \rangle} =
{\langle \mathbb{Dst}_{\mathbf{t2}} : \mathbf{Elm} \rangle} 
\end{gathered}
```

Это отношение является основой **алгоритма** $`\mathtt{AlgTransform}`$.

```math
\mathtt{AlgTransform}^{\mathbf{t1}\rightarrow\mathbf{t2}} \left( \mathbb{ObjW}{\ \cup\ }\mathbb{ObjM}
\right)
```

```math
\mathbf{Worker}_{\mathtt{AlgTransform}} \left( 
\mathbb{ObjW}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgTransform}} \left( 
\mathbb{ObjM}
\right)
```

```math
\mathbf{Result}_{\mathtt{AlgTransform}}
{\equiv\ }
\mathbf{Transform}(\mathbb{ObjM}_{\mathbf{t1}}, \mathbb{ObjR}_{\mathbf{t2}})
```

Для упрощения формул в работе будет предложено несколько вариантов альтернативной записи **алгоритма**. Первая такая сокращенная запись представлена далее:

```math
\begin{aligned}
& \mathtt{AlgTransform}^{\mathbf{t1}\rightarrow\mathbf{t2}} \left( \mathbb{ObjW}{\ \cup\ }\mathbb{ObjM}
\right) \equiv 
\\
& \begin{cases}
& \mathbf{Worker} \left( 
\mathbb{ObjW}
\right)
\\
& \mathbf{Material} \left( 
\mathbb{ObjM}
\right)
\\
& \mathbf{Transform}(\mathbb{ObjM}_{\mathbf{t1}}, \mathbb{ObjR}_{\mathbf{t2}})
\end{cases}
\end{aligned}
```

Для обозначения трансформирующих **алгоритмов** целесообразно использовать запись, которая детально проработана в химии и идеально подходит для **алгоритмов**, рассматриваемых в следующей части работы.

```math
\mathbb{ObjM}_{\mathbf{t1}} 
\xrightarrow
[\mathtt{Transform}]
{\mathbb{ObjW}}
\mathbb{ObjR}_{\mathbf{t2}} + \dots
```

При использовании этой записи иногда будет необходимо обозначать различные _экземпляры_ для нескольких участвующих _объектов_ одного _класса_. Делать это будем предварительным верхним индексом $`^{1}\mathbb{Obj}_{\mathbf{t1}}`$, $`^{1}\mathbb{Obj}_{\mathbf{t2}}`$, $`^{2}\mathbb{Obj}_{\mathbf{t2}}`$

В указанной форме упрощается запись некоторых **алгоритмов**, но есть ограничения применимости, в силу которых основной вариант математической записи тоже будет необходим. Запишем кратко **алгоритм** $`\mathtt{AlgSelfCopy}`$:

```math
\mathbb{Objs}^\star_{\mathbf{t1}} 
\xrightarrow
[\mathtt{SelfCopy}]
{^{1}\mathbb{OrgObjs}}
{^{2}\mathbb{OrgObjs}}_{\mathbf{t2}}
+
\dots
```

В качестве примера $`\mathtt{AlgSelfCopySample}`$ рассмотрим двухэтапный **алгоритм** и возможность его **синтеза** **группировкой** двух **алгоритмов** _самокопирования_.

```math
\begin{aligned}
& \mathtt{AlgSelfCopySample}^{\mathbf{t1}\rightarrow\mathbf{t3}} \left( 
\mathbb{Org}{\ \cup\ }\mathbb{Objs}^\star
\right) \equiv \\
& \begin{cases}
% ------------------------------------
   & \mathbf{Worker} \left( \mathbb{Org} \right) \\
   & \mathbf{Material} \left( \mathbb{Objs}^\star \right) \\
   & \mathbb{Org} \equiv \lbrace {^1}\mathbb{Org1}, {^1}\mathbb{Org2} \rbrace \\
   & \mathbb{Objs}^\star \equiv \lbrace {^{1-4}}\mathbb{ObjM}^\star \rbrace \\[10pt]
% ------------------------------------
   & \begin{aligned}
      & \mathtt{AlgTransform1}^{\mathbf{t1}\rightarrow\mathbf{t2}} \left( \mathbb{Org1}{\ \cup\ }\mathbb{ObjM}^\star \right) \equiv \\
      & \begin{cases}
      & \mathbf{Worker} \left( {^1}\mathbb{Org1} \right) \\
      & \mathbf{Transform}({^{1-2}}\mathbb{ObjM}^\star_{\mathbf{t1}}, {^{1-2}}\mathbb{ObjI}_{\mathbf{t2}})
      \end{cases}
   \end{aligned} \\[30pt]
% ------------------------------------
   & \begin{aligned}
      & \mathtt{AlgTransform2}^{\mathbf{t2}\rightarrow\mathbf{t3}} \left( \mathbb{Org1}{\ \cup\ }\mathbb{ObjM}^\star{\ \cup\ }\mathbb{ObjI} \right) \equiv \\
      & \begin{cases}
      & \mathbf{Worker} \left( {^1}\mathbb{Org1} \right) \\
      & \mathbf{Transform}({^3}\mathbb{ObjM}^\star_{\mathbf{t2}}{\ \cup\ }{^1}\mathbb{ObjI}_{\mathbf{t2}}, {^2}\mathbb{Org1}_{\mathbf{t3}})
      \end{cases}
   \end{aligned} \\[30pt]
% ------------------------------------
   & \begin{aligned}
      & \mathtt{AlgTransform3}^{\mathbf{t2}\rightarrow\mathbf{t3}} \left( \mathbb{Org2}{\ \cup\ }\mathbb{ObjM}^\star{\ \cup\ }\mathbb{ObjI} \right) \equiv \\
      & \begin{cases}
      & \mathbf{Worker} \left( {^1}\mathbb{Org2} \right) \\
      & \mathbf{Transform}({^4}\mathbb{ObjM}^\star_{\mathbf{t2}}{\ \cup\ }{^2}\mathbb{ObjI}_{\mathbf{t2}}, {^2}\mathbb{Org2}_{\mathbf{t3}})
      \end{cases}
   \end{aligned}
% ------------------------------------
\end{cases}
\end{aligned}
```

Запишем используемые алгоритмы в форме _трансформаций_.

```math
\mathbb{ObjM}^\star_{\mathbf{t1}} 
\xrightarrow
[\mathtt{Transform1}]
{\mathbb{Org1}}
\mathbb{ObjI}_{\mathbf{t2}} + \dots
```

```math
\mathbb{ObjM}^\star_{\mathbf{t2}} +
\mathbb{ObjI}_{\mathbf{t2}}
\xrightarrow
[\mathtt{Transform2}]
{^{1}\mathbb{Org1}}
{^{2}\mathbb{Org1}}_{\mathbf{t3}} + \dots
```

```math
\mathbb{ObjM}^\star_{\mathbf{t2}} +
\mathbb{ObjI}_{\mathbf{t2}}
\xrightarrow
[\mathtt{Transform3}]
{^{1}\mathbb{Org2}}
{^{2}\mathbb{Org2}}_{\mathbf{t3}} + \dots
```

Если объединить эти три _трансформации_ в одну, то получим:

```math
4\mathbb{ObjM}^\star_{\mathbf{t1}}
\xrightarrow
[\mathtt{SelfCopySample} \lbrace 2\mathbb{ObjI}_{\mathbf{t2}} \rbrace]
{^{1}\mathbb{Org1} + ^{1}\mathbb{Org2}}
{^{2}\mathbb{Org1}}_{\mathbf{t3}} +
{^{2}\mathbb{Org2}}_{\mathbf{t3}} + \dots
```

— или еще короче:

```math
4\mathbb{ObjM}^\star_{\mathbf{t1}}
\xrightarrow
[\mathtt{SelfCopy} \lbrace 2\mathbb{ObjI}_{\mathbf{t2}} \rbrace]
{^{1}\mathbb{Org}}
{^{2}\mathbb{Org}}_{\mathbf{t3}} + \dots
```

Просто увидеть, что **алгоритм** $`\mathtt{AlgSelfCopySample}`$ можно разделить на два отдельных **алгоритма** _самокопирования_ $`\mathtt{AlgSelfCopy1}`$ и $`\mathtt{AlgSelfCopy2}`$.

```math
2\mathbb{ObjM}^\star_{\mathbf{t1}}
\xrightarrow
[\mathtt{SelfCopy1} \lbrace \mathbb{ObjI}_{\mathbf{t2}} \rbrace]
{^{1}\mathbb{Org1}}
{^{2}\mathbb{Org1}}_{\mathbf{t3}} + \dots
```

```math
\mathbb{ObjM}^\star_{\mathbf{t1}} +
\mathbb{ObjI}_{\mathbf{t1}}
\xrightarrow
[\mathtt{SelfCopy2}]
{^{1}\mathbb{Org2}}
{^{2}\mathbb{Org2}}_{\mathbf{t2}} + \dots
```

Естественно предположить обратный _процесс_ слияния изначально существующих двух **алгоритмов** $`\mathtt{AlgSelfCopy1}`$ и $`\mathtt{AlgSelfCopy2}`$ в один $`\mathtt{AlgSelfCopySample}`$. И это слияние является **синтезом** **алгоритма** методом **группировки**, но дополнительным свойством этого **синтеза** является наличие _самокопируемости_ в результате. Отметим эту отличительную особенность такого **синтеза** и введём термин для его обозначения — _медленный синтез_. Особенностям и результатам _медленного синтеза_ посвящена следующая глава.


[^2-4--organism]:
    Примеры _организма_:

    * вирус,
    * бактерия,
    * вирусная компьютерная программа,
    * человек,
    * научное сообщество,
    * человечество.
