### 3.1. Этап медленного синтеза

++_Медленный синтез_++[^3-1--slow-synthesis] - это **класс** **синтеза** **алгоритма**, в котором выполнены следующие условия:

- использование существующих в **пространстве** **алгоритмов** разного типа (в том числе и **алгоритмов** _самокопирования_) в качестве основы,
- специальное изменение **структуры** _опорных_ _объектов_ избранных **алгоритмов** (_организма_ для **алгоритма** _самокопирования_),
- получение в результате изменения нового _организма_.

Методы _медленного синтеза_ применимы к различным наборам базовых **алгоритмов** и _организмов_, и характерными для этих методов являются:

* низкая эффективность как отношение количества успешных результатов к общему числу проб,
* большое количество попыток, компенсирующее низкую эффективность,
* отсутствие целевой направленности **синтеза** на этапе получения нового **алгоритма**, и наличие этого направления в выборе _полезных_ из массы генерируемых вариантов;
* функция оценки _полезности_ **синтезированного** **алгоритма** в _соревнованиях_ на основе его обусловленности наиболее _частыми_ _материалами_.

**Сохранение** _организма_ осуществляется _самокопированием_ каждого его _под-алгоритма_. В результате _самокопирования_ в _близком_ соседстве образуются _опорные_ _объекты_ _под-алгоритмов_ и в их _локали_ уменьшается _представленность_ _объектов_-_материалов_. Тем самым формируется _локаль_ с избытком _опорных_ _работников_ и недостатком _опорных_ _материалов_. Согласно предпосылке _частых_ _материалов_: они еще есть в **пространстве**, но вне _локали_ _организма_. «Граница» _локали_ становится пропускным каналом этих _материалов_ внутрь _организма_.

Здесь если рассмотреть в качестве _прикладной области_ **алгоритмической** **модели** наше трехмерное пространство, то в случае симметрии _параметров_ по всем направлениям полученный способ упаковки _локали_ _организма_ будет близок к шарообразной. Для шара с увеличением его линейного размера, согласно закону «квадрата-куба», объём (и количество _работников_) растёт пропорционально кубу этого размера, а площадь поверхности, ограничивающей этот шар - пропорционально квадрату. При данном способе упаковки контакт с _частыми_ _материалами_ **пространства** доступен в основном _работникам_, расположенным в _локали_ _организма_ _близко_ к его внешней поверхности.

<todo>

Оценить необходимость внесения в **признаки** **пространства** законов измерения и структуры параметра _близость_. Можно рассматривать **пространство** с наличием этого уточнения как _специализацию_ базового.

</todo>

Такое устройство **пространства** **модели** делает целесообразным _процесс_ _деления_ _организма_, предполагающий для увеличенного в размере _организма_ с наличием всех _опорных_ _работников_ в количестве превышающем единицу разделение его _локали_ на две под-_локали_, в каждой из которой формируется _организм_ с полным составом классов _опорных_ _работников_ исходного _организма_. Результирующие два _организма_ получат при этом меньший размер и большую поверхность, с более эффективным обеспечением _работников_ _материалами_, чем исходный _организм_.

++_Деление_++[^3-1--organism-division] - _специализация_ _самокопирования_ _организма_, производящая _самокопирование_ всех _под-алгоритмов_ _организма_ и последующее разделение его _локали_ на две _локали_, которые содержат весь набор классов _опорных_ _объектов_ _под-алгоритмов_, как и исходный _организм_, и даже при отсутствии впоследствии _близости_ этих разделенных _локалей_ каждая из них является **сохраненным** **экземпляром** _организма_.

Для описания происходящего при разных методах _медленного синтеза_ и для других _процессов_, рассматриваемых далее в этой работе, необходима запись формулы _процесса_, сопоставляющая некоторое исходное состояние, задаваемое _конструкцией_ _объектов_, и наступление события, описываемое дополнительным условием, для которых указывается гарантированное результирующее состояние, получаемое в **пространстве**:

```math
\begin{matrix}
&\left<\text{Исходное~состояние}\right>
\\ 
& \mathord{\xrightarrow{\hspace{12em}}}
\\
&\left<\text{Событие}\right>
\\ 
& \mathord{\xlongequal{\hspace{12em}}}
\\ 
&\left<\text{Состояние-результат}\right>
\end{matrix}
```

Дополнительно является необходимым обозначение для описания _локали_ $`\mathbb{Loc1}`$, содержащей _опорные_ _объекты_ некоторого **алгоритма**, например, множество всех _резидентных_ не _частых_ _работников_ _организма_ $`\mathbb{Org1W1}+\mathbb{Org1W2}`$:

```math
\mathbb{Loc1}
\left\lbrace
  \begin{matrix}
  & \mathbb{Obj1}^\star
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1W1}}
  & \mathbb{ObjI} + \mathbb{Org1W1}
  \\
  \\
  & \mathbb{Obj2}^\star + \mathbb{ObjI}
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org1W2}}
  & \mathbb{Org1W2}
  \end{matrix}
\right.
```

Для многих методов _медленного синтеза_ _процесс_ заключается в контакте двух _локалей_, содержащих _опорные_ _объекты_ некоторых **алгоритмов** и _организмов_, и слияние этих _локалей_ в одну результирующую _локаль_, объединяющую все эти _опорные_ _объекты_.


```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
  \begin{aligned}
    \quad 
    & \mathbb{Obj1}^\star
      \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
      \mathbb{Org1}
  \end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
  \begin{aligned}
    \quad 
    & \mathbb{Obj2}^\star 
      \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
      \mathbb{Org2}
  \end{aligned}
\right.
\\
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{24em}}}
```
```math
\begin{aligned}
& \mathbf{Near}(\mathbb{Loc1}, \mathbb{Loc2}) 
\\
\end{aligned}
```
```math
\mathord{\xlongequal{\hspace{24em}}}
```
```math
\begin{aligned} 
& \mathbb{LocC}
\left\lbrace
  \begin{aligned}
    \quad
    & \mathbb{Obj1}^\star
      \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
      \mathbb{Org1}
    \\ \\
    & \mathbb{Obj2}^\star 
      \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
      \mathbb{Org2}
  \end{aligned}
\right.
\end{aligned}
```

Введем для такого _процесса_ сокращенную форму записи.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy}]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

#### 3.1.1. Зарождение организма

++_Зарождение_++[^3-1--organism-genesis] - метод _медленного синтеза_, основанный на **группировке** при контакте нескольких **алгоритмов**, _представленных_ своими _опорными_ объектами, в результате которого появляется _организм_.

(не курица и не яйцо :smile:)

- _Зарождение_ **группировкой** _конструкторов_. (1) - _полезно_; (2) - _полезно_. !!! _Полезно_ каждой стороне.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{Transform1}]{\mathbb{Obj1}}
  & \mathbb{Obj2}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{Transform2}]{\mathbb{Obj2}}
  & \mathbb{Obj1}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy}]{{^1}\mathbb{Obj1}+{^1}\mathbb{Obj2}}
  & {^2}\mathbb{Obj1} + {^2}\mathbb{Obj2}
\end{matrix}
```

- _Зарождение_ **группировкой**, замыкающей цепочку _трансформаций_

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{Transform1}]{\mathbb{Obj1}}
  & \mathbb{ObjI}
  & (1)
\\
+& 
\\
  & \mathbb{ObjI} 
  & \xrightarrow[\mathtt{Transform2}]{\mathbb{Obj2}^\star}
  & \mathbb{Obj1}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI} \rbrace ]{{^1}\mathbb{Obj1}+\mathbb{Obj2}^\star}
  & {^2}\mathbb{Obj1}
\end{matrix}
```

#### 3.1.2. Встраивание в организм

++_Встраивание_++[^3-1--organism-implant] - метод _медленного синтеза_, основанный на **группировке** при контакте внешнего **алгоритма**, _представленного_ своими _опорными_ объектами, и всех _под-алгоритмов_ некоторого _организма_.

- _Встраивание_ _трансформации_

```math
\begin{matrix}
  & \mathbb{Obj1}^\star + \mathbb{ObjI}
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{Transform1}]{}
  & \mathbb{ObjI}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI} \rbrace]{{^1}\mathbb{Org1}}
  & {^2}\mathbb{Org1}
\end{matrix}
```

- _Встраивание_ внешнего работника

```math
\begin{matrix}
  & \mathbb{Obj1}^\star + \mathbb{ObjI}
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{Transform1}]{\mathbb{Obj3}^\star}
  & \mathbb{ObjI}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI} \rbrace]{{^1}\mathbb{Org1} + \mathbb{Obj3}^\star}
  & {^2}\mathbb{Org1}
\end{matrix}
```

- _Встраивание_-симбиоз

```math
\begin{matrix}
  & \mathbb{Obj1}^\star + \mathbb{ObjI1}
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1} + \mathbb{ObjI2}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star + \mathbb{ObjI2} 
  & \xrightarrow[\mathtt{Transform1}]{}
  & \mathbb{ObjI1}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI1}+ \mathbb{ObjI2} \rbrace]{{^1}\mathbb{Org1}}
  & {^2}\mathbb{Org1}
\end{matrix}
```

#### 3.1.3. Слияние организмов

++_Слияние_++[^3-1--organism-merge] - метод _медленного синтеза_, основанный на **группировке** всех _под-алгоритмов_, имеющихся при контакте у двух _организмов_, и последующем исключении потерявших _самокопируемость_ в **синтезированном** _организме_.

- Независимое _слияние_. (1) - не _вредно_; (2) - не _вредно_

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy}]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

- Конкурирующее _слияние_. В ситуации достаточности ресурса $`\mathbb{Obj1}^\star`$: (1) - не _вредно_; (2) - не _вредно_.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy}]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

- Зависимое _слияние_. (1) - не _вредно_; (2) - _полезно_.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1} + \mathbb{ObjI}
  & (1)
\\
+& 
\\
  & \mathbb{ObjI} 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI} \rbrace ]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

- _Слияние_-симбиоз. (1) - _полезно_; (2) - _полезно_. !!! Обоюдо-_полезно_

```math
\begin{matrix}
  & \mathbb{Obj1}^\star + \mathbb{ObjI1}
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1} + \mathbb{ObjI2}
  & (1)
\\
+& 
\\
  & \mathbb{Obj2}^\star + \mathbb{ObjI2} 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2} + \mathbb{ObjI1}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI1}+\mathbb{ObjI2} \rbrace ]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

- Паразитное _слияние_. (1) - _вредно_; (2) - _полезно_.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star
  & \xrightarrow[\mathtt{Transform1}]{\mathbb{Org1}}
  & \mathbb{ObjI}
\\
  &&&& (1)
\\
  & \mathbb{Obj2}^\star + \mathbb{ObjI}
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
\\
+& 
\\
  & \mathbb{ObjI} 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & 2\mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{ObjI} \rbrace ]{{^1}\mathbb{Org1}+{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org1} + {^2}\mathbb{Org2}
\end{matrix}
```

- _Слияние_ поеданием. (1) - _вредно_; (2) - _полезно_.

```math
\begin{matrix}
  & \mathbb{Obj1}^\star
  & \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
  & \mathbb{Org1}
  & (1)
\\
+& 
\\
  & \mathbb{Org1} 
  & \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
  & \mathbb{Org2}
  & (2)
\\
\\ \hline 
\\ 
  & \mathbb{Obj1}^\star 
  & \xrightarrow[\mathtt{SelfCopy} \lbrace \mathbb{Org1} \rbrace ]{{^1}\mathbb{Org2}}
  & {^2}\mathbb{Org2}
\end{matrix}
```

#### 3.1.4. Заимствование работников между организмами

++_Заимствование_++[^3-1--organism-adoption] - метод _медленного синтеза_, основанный на контакте двух _организмов_ и передаче от _организма_-донора _организму_-акцептору _работников_, **сохраняющих** _самокопируемость_ в _организме_-акцепторе. (_специализация_ _слияния_)

Возможно два варианта _заимствования_:

- _Заимствование_ автономного _самокопирующегося_ _работника_

```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
  \begin{aligned}
    \quad & {^1}\mathbb{Org1W2}
    \\
    & {^2}\mathbb{Org1W2}
    \\ \\
    & \mathbb{Obj1}^\star
      \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1W1}}
      \mathbb{ObjI} + \mathbb{Org1W1}
    \\ \\
    & \mathbb{Obj2}^\star + \mathbb{ObjI}
      \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org1W2}}
      \mathbb{Org1W2}
  \end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
  \begin{aligned}
    \quad & \mathbb{Obj3}^\star 
      \xrightarrow[\mathtt{SelfCopy3}]{\mathbb{Org2W1}}
      \mathbb{Org2W1}
  \end{aligned}
\right.
\\
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{24em}}}
```
```math
\begin{aligned}
& {^2}\mathbb{Org1W2} \in \mathbb{Loc2}
\\ 
\end{aligned}
```
```math
\mathord{\xlongequal{\hspace{24em}}}
```
```math
\begin{aligned} 
& \mathbb{Loc1}
\left\lbrace
  \begin{aligned}
    \quad & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
      \xrightarrow[\mathtt{SelfCopy4} \lbrace \mathbb{ObjI} \rbrace ]{{^1}\mathbb{Org1W1}+{^1}\mathbb{Org1W2}}
      {^2}\mathbb{Org1W1} + {^3}\mathbb{Org1W2}
  \end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
  \begin{aligned}
    \quad & \mathbb{Obj3}^\star + \mathbb{Obj2}^\star 
      \xrightarrow[\mathtt{SelfCopy5} \lbrace \mathbb{ObjI} \rbrace ]{{^1}\mathbb{Org2W1}+{^2}\mathbb{Org1W2}}
      {^2}\mathbb{Org2W1} + {^4}\mathbb{Org1W2}
  \end{aligned}
\right.
\end{aligned}
```

- _Заимствование_ _работника_, _самокопирование_ которого обеспечивается частью, имеющейся у обоих контактирующих _организмов_

```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad & {^1}\mathbb{OrgW}
  \\
  & {^2}\mathbb{OrgW}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{OrgC}}
    \mathbb{OrgC}
  \\ \\
  & \mathbb{Obj2}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{OrgC},\mathbb{OrgW}}
    \mathbb{OrgW}
\end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy3}]{\mathbb{OrgC}}
    \mathbb{OrgC}
  \\ \\
  & \mathbb{Obj2}^\star
    \xrightarrow[\mathtt{SelfCopy4}]{\mathbb{OrgC},\mathbb{Org}}
    \mathbb{Org}
\end{aligned}
\right.
\\
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{24em}}}
```
```math
\begin{aligned}
& {^2}\mathbb{OrgW} \in \mathbb{Loc2} 
\\
\end{aligned}
```
```math
\mathord{\xlongequal{\hspace{24em}}}
```
```math
\begin{aligned} 
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
    \xrightarrow[\mathtt{SelfCopy5}]{{^1}\mathbb{OrgC}+{^1}\mathbb{OrgW}}
    {^2}\mathbb{OrgC} + {^3}\mathbb{OrgW}
\end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{Obj1}^\star + \mathbb{Obj2}^\star 
    \xrightarrow[\mathtt{SelfCopy6}]{{^1}\mathbb{OrgC}+{^1}\mathbb{Org}+{^2}\mathbb{OrgW}}
    {^2}\mathbb{OrgC} + {^2}\mathbb{Org} + {^4}\mathbb{OrgW}
\end{aligned}
\right.
\end{aligned}
```

#### 3.1.5. Изменение части организма

++_Модификация_++[^3-1--organism-modification] - метод _медленного синтеза_, основанный на **синтезе** _под-алгоритма_ в _организме_ на основе уже имеющихся _опорных_ _объектов_ этого _организма_ и с возможным последующим исключением тех _под-алгоритмов_, которые в результате потеряли свойства _самокопируемости_.


- _Модификация_-исключение, делающая не-_исполнимым_ _под-алгоритм_ _организма_, например, из-за изменения _параметров_ в **пространстве** или вызванного _взаимодействием_ удаления _работника_ или нарушения его _конструкции_. В следующей формуле используется запись $`\forall \Big\lbrace\neg \mathbf{Eq}(\mathbb{Loc1}, \mathbb{obj1})\Big\rbrace`$, которая задаёт требование отсутствия в _локали_ $`\mathbb{Loc1}`$ _объекта_ эквивалентного $`\mathbb{obj1}`$, не указывая способа достижения этого состояния. Таким способом может быть разрушения _связи_ в этом _объекте_ или изъятие _объекта_ из указанной _локали_ в ситуации, когда это был последний _экземпляр_ этого _класса_ _эквивалентности_ в _локали_.
```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{obj} \in \mathbb{OrgW2}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{OrgW1}}
    \mathbb{OrgW1}
  \\ \\
  & \mathbb{Obj2}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{OrgW2}}
    \mathbb{OrgW2}
\end{aligned}
\right.
\\ 
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{18em}}}
```
```math
\begin{aligned}
& \forall \Big\lbrace\neg \mathbf{Eq}(\mathbb{Loc1}, \mathbb{obj})\Big\rbrace
\\
\end{aligned}
```
```math
\mathord{\xlongequal{\hspace{18em}}}
```
```math
\begin{aligned} 
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{OrgW1}}
    \mathbb{OrgW1}
\end{aligned}
\right.
\end{aligned}
```

- _Модификация_ **случайным поиском** с изменением _связей_ в _опорных_ _объектах_ _организма_ и обнаружением нового _взаимодействия_ у появившихся и существовавших классов _объектов_ _организма_.

```math
\begin{aligned}
& \mathbb{OrgW1} \equiv 
\underset{org\_w1}{\mathbb{Macro}} 
\langle \mathbb{obj1}, \mathbb{obj2} \rangle 
\lbrace \mathbf{Link}(\mathbb{obj1}, \mathbb{obj2}) \rbrace
\\
& \mathbb{OrgW2} \equiv \underset{org\_w2}{\mathbb{Macro}}
\langle \mathbb{obj1}, \mathbb{obj1} \rangle 
\lbrace \mathbf{Link}(\mathbb{obj1}, \mathbb{obj1}) \rbrace
\end{aligned}
```
```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad 
  & {^1}\mathbb{OrgW1}\langle {^1}\mathbb{obj1}, {^1}\mathbb{obj2} \rangle
  \\
  & {^2}\mathbb{OrgW1}\langle {^2}\mathbb{obj1}, {^2}\mathbb{obj2} \rangle
  \\
  & {^3}\mathbb{OrgW1}
  \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{OrgW1}}
    \mathbb{OrgW1}
\end{aligned}
\right.
\\ 
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{18em}}}
```
```math
\begin{aligned}
\neg & \mathbf{Link}({^1}\mathbb{obj1}, {^1}\mathbb{obj2}) {\ \wedge\ }
\\
{\ \wedge\ } \neg & \mathbf{Link}({^2}\mathbb{obj1}, {^2}\mathbb{obj2}) {\ \wedge\ }
\\
{\ \wedge\ } & \mathbf{Link}({^1}\mathbb{obj1}, {^2}\mathbb{obj1}) 
\end{aligned}
```
```math
\begin{matrix}
\exists\ {^1}\mathbb{OrgW2}\langle {^1}\mathbb{obj1}, {^2}\mathbb{obj1} \rangle :
\\ \\
\mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{OrgW2}}
    \mathbb{OrgW2}
\end{matrix}
```
```math
\mathord{\xlongequal{\hspace{18em}}}
```
```math
\begin{aligned} 
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{{^3}\mathbb{OrgW1}}
    \mathbb{OrgW1}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{{^1}\mathbb{OrgW2}}
    \mathbb{OrgW2}
\end{aligned}
\right.
\end{aligned}
```

- _Модификация_ **группировкой**, обеспечивается построением _макро-алгоритма_ на основе уже имеющихся _под-алгоритмов_ _организма_.

```math
\begin{aligned}
& \mathbb{OrgWC} \equiv 
\underset{org\_wc}{\mathbb{Macro}} 
\langle \mathbb{obj1}, \mathbb{obj2} \rangle 
\lbrace \mathbf{Link}(\mathbb{obj1}, \mathbb{obj2}) \rbrace
\end{aligned}
```
```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad 
  & {^1}\mathbb{obj1},  {^2}\mathbb{obj1}
  \\
  & {^1}\mathbb{obj2}, {^2}\mathbb{obj2}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{obj1}}
    \mathbb{obj1}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{obj2}}
    \mathbb{obj2}
\end{aligned}
\right.
\\ 
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{18em}}}
```
```math
\begin{aligned}
& \mathbf{Link}({^2}\mathbb{obj1}, {^2}\mathbb{obj2}) 
\end{aligned}
```
```math
\begin{matrix}
  \exists\ {^1}\mathbb{OrgWC}\langle {^2}\mathbb{obj1}, {^2}\mathbb{obj2} \rangle :
  \\ \\
  \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy3}]{\mathbb{OrgWC}}
    \mathbb{OrgWC}
\end{matrix}
```
```math
\mathord{\xlongequal{\hspace{18em}}}
```
```math
\begin{aligned} 
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad 
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy1}]{{^1}\mathbb{obj1}}
    \mathbb{obj1}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy2}]{{^1}\mathbb{obj2}}
    \mathbb{obj2}
  \\ \\
  & \mathbb{Obj1}^\star
    \xrightarrow[\mathtt{SelfCopy3}]{{^1}\mathbb{OrgWC}}
    \mathbb{OrgWC}
\end{aligned}
\right.
\end{aligned}
```

#### 3.1.6. Союз организмов

++_Союз_++[^3-1--organism-union] - метод _медленного синтеза_, _специализация_ **группировки**, объединяющая несколько _организмов_ с **сохранением** их внутренней _конструкции_ и **синтезом** **алгоритмов** их _взаимодействия_.

_Союз_ во многом сходен со _слиянием_ за исключением отличия, заключающегося в сохранении _локалей_ исходных _организмов_. Например, уже рассмотренное ранее _слияние_-симбиоз можно модифицировать и в результате получить следующий _союз_-симбиоз:

```math
\begin{aligned}
& \mathbb{Loc1}
\left\lbrace
\begin{aligned}
  \quad
  & \mathbb{Obj1}^\star + \mathbb{ObjI1}
    \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
    \mathbb{Org1} + \mathbb{ObjI2}
\end{aligned}
\right.
\\ \\
& \mathbb{Loc2}
\left\lbrace
\begin{aligned}
  \quad
  & \mathbb{Obj2}^\star + \mathbb{ObjI2} 
    \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
    \mathbb{Org2} + \mathbb{ObjI1}
\end{aligned}
\right.
\\
\end{aligned}
```
```math
\mathord{\xrightarrow{\hspace{24em}}}
```
```math
\begin{aligned}
& \mathbf{Near}(\mathbb{Loc1}, \mathbb{Loc2}) 
\\
\end{aligned}
```
```math
\mathord{\xlongequal{\hspace{24em}}}
```
```math
\mathbb{LocC}
\left\lbrace
    \begin{aligned}
    \quad 
    & \mathbb{ObjI1}, \mathbb{ObjI2}
    \\ \\
    & \mathbb{Loc1}
    \left\lbrace
    \begin{aligned}
      \quad 
      & \mathbb{Obj1}^\star + \mathbb{ObjI1}
        \xrightarrow[\mathtt{SelfCopy1}]{\mathbb{Org1}}
        \mathbb{Org1} + \mathbb{ObjI2}
    \end{aligned}
    \right.
    \\ \\
    & \mathbb{Loc2}
    \left\lbrace
    \begin{aligned}
      \quad 
      & \mathbb{Obj2}^\star + \mathbb{ObjI2} 
        \xrightarrow[\mathtt{SelfCopy2}]{\mathbb{Org2}}
        \mathbb{Org2} + \mathbb{ObjI1}
    \end{aligned}
    \right.
    \end{aligned}
\right.
```

Сходно с приведенным примером _союза_-симбиоза есть возможность трансформации в _союз_ всех способов _слияния_, рассмотренных ранее в работе. Приведем список наиболее значимых _союзов_:

- ++_независимый союз_++[^3-1--independent-union] - «обитание» _организмов_ в одной _локали_ без _взаимодействий_;
- ++_конкурирующий союз_++[^3-1--competitive-union] - «обитание» _организмов_ в одной _локали_ с конкуренцией за _материалы_;
- ++_зависимый союз_++[^3-1--dependent-union] - «обитание» _организмов_ в одной _локали_ с использованием одним _организмом_ _материалов_, производимых, но не требуемых другому _организму_;
- ++_союз-симбиоз_++[^3-1--symbiosis-union] - «обитание» _организмов_ в одной _локали_ и взаимное пользование _материалами_, производимыми и не требующимися другому _организму_ _союза_;
- ++_паразитный союз_++[^3-1--parasitic-union] - «обитание» _организмов_ в одной _локали_ с использованием одним _организмом_ _материалов_, производимых и требуемых другому _организму_;
- ++_союз поеданием_++[^3-1--food-union] - «обитание» _организмов_ в одной _локали_ с использованием одним _организмом_ другого _организма_ в качестве _материала_.

Особо выделим _союз_ одинаковых _организмов_, на основе которого есть возможность реализовать **развитие** **пространства** с **синтезом** **алгоритмов** _коммуникации_ на основе _виртуальных_ _признаков_ и в результате обеспечить появление **синтеза** с использованием _виртуальных_ _абстрактных моделей_.

_Союз_ одинаковых _организмов_ в своей самой простой реализации является _конкурирующим союзом_, в силу того что два одинаковых _организма_ для _исполнения_ используют одинаковые _материалы_. Появление такого _союза_ возможно в результате _процесса_ _деления_ _организма_ в _локали_, содержащей количество _материалов_, достаточное для _исполнения_ появившихся двух _организмов_. Для **сохранения** _союза_ одинаковых _организмов_ необходимо, чтобы в _соревновании_ _союзы_ _организмов_ были более успешными по сравнению с одиночными тождественными _организмами_.

Дополнительной _пользой_ _союза_ одинаковых _организмов_ является возможность коллективного **синтеза** _полезного_ **алгоритма**, состоящего в **синтезе** этого **алгоритма** в одном _организме_, принадлежащем _союзу_, и последующем распространении на все _организмы_ _союза_ с использованием _заимствования_ или _слияния_-симбиоза с _делением_.

Необходимо отметить, что два изначально эквивалентных _организма_ после **синтеза** **алгоритма**, произошедшем в одном из них, становятся разными. Различие _организмов_ (и изначально эквивалентных, и всегда различных) может быть разного характера. Например, при контакте таких разных _организмов_ может произойти _слияние_ поеданием. Зафиксируем _специализацию_ различия _организмов_ в следующем термине.

++==_Родственные_== _организмы_++[^3-1--relative-organism] - два не эквивалентных _организма_, которые при контакте могут участвовать в _слиянии_-симбиозе.

**Развитием** для _родственных_ _организмов_ является возможность при контакте использовать _заимствование_ [@taylor:biology:2019, т. 1, стр. 28-29].

_Родственные_ _специализации_ _заимствования_ и _слияния_-симбиоза стали ключевым моментом **развития** **пространства** на основе _медленного синтеза_, сделав возможным на основе _союза_ _родственных_ _организмов_ **синтез** _полезных_ такому _организму_ _под-алгоритмов_ и **группировку** различных _полезных_ **синтезированных** _под-алгоритмов_ в одном _организме_. Эти _процессы_ лежат в основе термина биологии «половое размножение».

<todo>

Проработать примеры к главе (уточнить формулировки)

</todo>


[^3-1--slow-synthesis]:
    Примеры _медленного синтеза_:

    * биологические эволюционные изменения, основанные:

      * на использовании базы _идентификатор_ + _работник_ («генетические буквы», «клеточные процессы»),
      * на использовании процедуры **сохранения** - создания копий (размножение биологических организмов),
      * на указании _пользы_ - функции оценки жизнеспособности новых организмов, контролирующей доступ к процедуре **сохранения** («естественный отбор»).

    * случайное появление (мутацией гена) нового сорта яблок с повышенным содержанием сахаров;
    * объединение микросервисов в программном проекте;
    * заимствование программного модуля работы с видеокамерой из одного проекта для другого проекта.

[^3-1--organism-division]:
    Примеры _деления_ _организма_:

    * деление биологической клетки;
    * деление вычислений в графической видеокарте на шейдеры;
    * деление сервера обслуживающего большое количества клиентов на под-процессы или под-потоки с распределением нагрузки.

[^3-1--organism-genesis]:
    Примеры _зарождения_ _организма_:

    * ? зарождение упрощающих _организмов_ - цепные реакции, например, распад урана 238 под действием медленного нейрона в среде, обеспечивающей замедление нейронов;
    * написание самокопирующихся программ с копированием на уровне исполняемых файлов (например, вирусные программы);
    * написание самокопирующихся программ с копированием на уровне исполнения, например, для распределения серверной нагрузки по сетевым узлам.

[^3-1--organism-implant]:
    Примеры _встраивания_ в _организм_:

    * искусственное сердце у человека;
    * цикл усваивания энергии света - гипотеза внедрения фотосинтеза в клетку;
    * разработка новой программы облегчающей производственный процесс;
    * обучение человека акробатическому трюку.

[^3-1--organism-merge]:
    Примеры _слияния_ _организмов_:

    * ? митохондрии как захват бактерий прокариотами;
    * цианобактерии у губок;
    * процесс оплодотворения яйцеклетки сперматозоидом.

[^3-1--organism-adoption]:
    Примеры _заимствований_ между _организмами_:

    * stack overflow для программистов;
    * обмен устойчивости к антибиотику между бактериями.

[^3-1--organism-modification]:
    Примеры _модификации_ _организма_:

    * мутация ДНК в живой клетке;
    * изменение оформления программы, используемой человеком;
    * переучивание левши для письма правой рукой.

[^3-1--organism-union]:
    Примеры _союзов_ между _организмами_:

    * биосфера;
    * экосистема леса;
    * бактериальная микрофлора кишечника человека;
    * растение и насекомое опылитель.

[^3-1--independent-union]:
    Примеры _независимых союзов_ между _организмами_:

    * не разговаривающие и не взаимодействующие соседи;
    * плодовые растения на соседних грядках.

[^3-1--competitive-union]:
    Примеры _конкурирующих союзов_ между _организмами_:

    * ? прайд львов;
    * ? коралловый риф;
    * несколько разных вирусных программ, работающих в однопроцессорной ЭВМ.

[^3-1--dependent-union]:
    Примеры _зависимых союзов_ между _организмами_:

    * комменсализм, например, рыба-прилипала;
    * бобовые и злаки;
    * молодая ель и береза.

[^3-1--symbiosis-union]:
    Примеры _союзов-симбиозов_ между _организмами_:

    * бобовые растения и азотфиксирующие бактерии;
    * тля и муравей.

[^3-1--parasitic-union]:
    Примеры _паразитных союзов_ между _организмами_:

    * вирусы;
    * москиты, клещи;
    * насекомые наездники;
    * паразитические грибы;
    * омела на деревьях.

[^3-1--food-union]:
    Примеры _союзов поеданием_ между _организмами_:

    * домашняя курица и человек;
    * пищевые цепочки экосистемы.

[^3-1--relative-organism]:
    Примеры _родственных_ _организмов_:

    * представители 'Homo sapiens';
    * ? два физика;
    * ? команда программистов;
    * разнополые особи одного биологического вида.