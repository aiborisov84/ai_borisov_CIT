## 2. Базис

### 2.1. Алгоритмическое пространство

Для построения ++**алгоритмической** ==**модели**==++ (по правилам _абстрактной модели_[^2-1--model]) выделяется минимальный набор **признаков** _прикладной области_, необходимый и достаточный для выполнения _символического_ описания всех процессов, которые будут изучаться с использованием _модели_. На основе выделенной совокупности _абстрактных_ **признаков** определяется **алгоритмическое** **пространство** (его _объекты_, _связи_ и _процессы_), и все последующие построения внутри **пространства** согласовываются с этими **признаками**. В силу использования такого подхода существует возможность все решения, полученные в **алгоритмическом** **пространстве**, _транслировать_ в требуемую _прикладную область_ на основе её соответствия выделенным _абстракциям_, для обеспечения которого достаточно, чтобы _прикладная область_ удовлетворяла всем **признакам**. Здесь и далее термин ++**алгоритмический**++ означает принадлежность **алгоритмической** **модели**.

++**Алгоритмическое** ==**пространство**==++[^2-1--space] - это _пространство_ **алгоритмической** **модели**, то есть:

- множество базовых сущностей (_символов_),
- множество возможных отношений базовых сущностей (_символических_ **структур**)
- и множество разрешенных преобразующих операций с сущностями и их отношениями (_преобразования_ _символических_ **структур**)

— совокупность которых сформирована на основе _абстрактных_ **признаков** _трансляции_ **алгоритмической** **модели**.

#### 2.1.1. Признаки

++**Признаки**++ **алгоритмической** **модели** — это множество _абстрактных_ _признаков_ _среды_, на основе которых формируется _трансляция_ между **пространством** **модели** и _прикладной областью_:

* ++**Признак** ==**Делимость**==++[^2-1--fissility] - существование в _прикладной области_ устойчивых во времени обособляемых частей — _объектов_.
* ++**Признак** ==**Однородность**==++[^2-1--uniform] - существование в _прикладной области_ эквивалентных _объектов_, не отличимых по свойствам друг от друга в любой момент времени.
* ++**Признак** ==**Локальность**==++[^2-1--locality] - существование в _прикладной области_ изменяющегося во времени параметра _близко_, который для каждой пары _объектов_ определяет возможность их _взаимодействия_.
* ++**Признак** ==**Иерархичность**==++[^2-1--hierarchy] - существование _макро-объектов_ и их **Делимость** на _под-объекты_, которые в свою очередь тоже могут быть _макро-объектами_ или же неделимыми _элементарными_ _объектами_.
* ++**Признак** ==**Эффектность**==++[^2-1--effect] - способность _объекта_ согласно своим свойствам _преобразовать_ _прикладную область_, то есть изменять во времени _близость_ и _параметры_ _объектов_, с которыми он _взаимодействует_.
* ++**Признак** ==**Динамичность**==++[^2-1--dynamism] - возможность самостоятельных изменений _близости_ и других _параметров_ _объектов_ в _прикладной области_ с течением времени согласно внутренним законам.
* ++**Признак** ==**Классифицируемость**==++[^2-1--enclass] - существование в _прикладной области_ _объектов_ и _процессов_, которые различаются по некоторой совокупности признаков.

#### 2.1.2. Сущности

Раскроем представленные **признаки** **алгоритмической** **модели**, задав на их основе _признаки_ базовых сущностей.

* ++_Взаимодействие_++[^2-1--interaction] - это взаимное влияние _близких_ _объектов_ на изменение во времени их _близости_ и _параметров_. Будем говорить, что _объекты_ ++_взаимодействуют_++ в некоторый момент времени, если в этот момент под влиянием _взаимодействия_ происходят такие изменения.
* ++_Связь_++[^2-1--connection] - устойчивое во времени _локальное_ соединение нескольких _объектов_, возникающее в результате их _взаимодействия_ и способное без вмешательства других _взаимодействий_ удерживать эти _объекты_ вместе в составе _макро-объекта_.
* ++_Конструкция_++[^2-1--construct] - рекурсивная система _связей_ формирующая _макро-объект_ на основе входящих в его состав _под-объектов_. Рекурсивно задаются все уровни _связей_ от _элементарных_ _объектов_, группирующихся в _макро-объекты_ первого уровня, до группировки _макро-объектов_ последнего уровня в результирующий _объект_.
* ++_Объект_++[^2-1--object] - обособляемый участок (в _макро-объекте_ или **пространстве**), который может длительное время сохранять свою внутреннюю _конструкцию_. Все свойства _объекта_ зависят только от составляющих его _под-объектов_ и _конструкции_ их _связей_. Свойства _объекта_ определяют все возможности _взаимодействия_ с другими _объектами_. Каждый _под-объект_ является полноправным _объектом_. Для удобства дальнейших построений закрепим вырожденный случай, когда _объект_ состоит из одного _под-объекта_ без _связей_, то есть _объект_ целиком является своим _под-объектом_.

Продолжим описывать **алгоритмическое** **пространство**, указав возможные в нём операции, которые обеспечивают изменение **пространства** во времени.

* ++_Динамика_++[^2-1--dynamic] - выполняемое в результате воздействия законов **пространства** изменение _близости_ и _параметров_ _объектов_ (реализация **признака** **Динамичность**).
* ++_Преобразование_++[^2-1--transform] - выполняемое в результате _взаимодействия_ _объектов_ изменение их _близости_ и _параметров_ (реализация **признака** **Эффектность**).

#### 2.1.3. Отношения и множества

Введем отношения, меры и специализированные подмножества на 'рекурсивном' множестве $`\mathbb{Objs}_{\mathbf{t}}`$, содержащем все _объекты_, существующие в **пространстве** в момент времени $`\mathbf{t}`$:

* ++==_Элементарный_== _объект_++ — это _объект_, в котором отсутствует или не важна для **модели** и потому не рассматривается его внутренняя _конструкция_. Унарное отношение «_Элементарный_ _объект_» для $`a\in\mathbb{Objs}_{\mathbf{t}}:\mathbf{Elm}(a)`$ - признак _элементарного_ _объекта_.
* Множество _элементарных_ _объектов_ $`\mathbb{Elms}_{\mathbf{t}}\equiv{\left\lbrace \mathbb{x} \mid {x}\in\mathbb{Objs}_{\mathbf{t}} \land \mathbf{Elm}(x)\right\rbrace}`$.
* ++_Близость_++ — бинарное отношение для пары _объектов_, которое на заданный момент времени определяет возможность _взаимодействия_ $`\mathbf{Near}_{\mathbf{t}}(a, b)`$, где $`a, b\in\mathbb{Objs}_{\mathbf{t}}`$. Отношение _близости_ не рефлексивно, симметрично, не транзитивно. _Объекты_, для которых истинно отношение _близости_, будем называть ++==_близкими_== _объектами_++.
* ++_Связь_++ — бинарное отношение для пары _объектов_, которое на заданный момент времени определяет наличие _взаимодействия_ поддерживающего их _близость_ $`\mathbf{Link}_{\mathbf{t}}(a, b)`$ , где $`{a, b}\in\mathbb{Objs}_{\mathbf{t}}`$. Отношение _связи_ не рефлексивно, симметрично, не транзитивно. Из определений верно следующее утверждение $`\forall{x,y\in\mathbb{Objs}_{\mathbf{t}}}:\mathbf{Link}_{\mathbf{t}}(x, y)\to\mathbf{Near}_{\mathbf{t}}(x, y)`$. Два _объекта_, для которых истинно отношение $`\mathbf{Link}`$, будем называть ++==_связанными_== _объектами_++.
* ++_Локаль_++[^2-1--locale] - это обособляемый участок в **пространстве** в некоторый момент времени, который задаётся указанием входящего в него множества _близких_ _объектов_.
* ++==_Параметры_== _объекта_++[^2-1--object-param] - формально не раскрываемое (то есть вводимое в виде «чёрного ящика») указание на влияние _динамики_ на _локальное_ множество _объектов_ $`\mathbb{Obj1}`$ в конкретный момент времени $`\mathbf{t1}`$. Это влияние будем обозначать $`\mathrm{Param}( \mathbb{Obj1}_{\mathbf{t1}} )`$. Для последующих формальных построений достаточно возможности оперировать мерой сходства _параметров_ для одного и того же _объекта_ в разные моменты времени и для любых _эквивалентных_ _объектов_ в разных _локалях_.

Введем бинарную меру сходства $`K`$ _параметров_ в двух разных _локалях_. Будем записывать $`K_{\mathrm{Param}}( \mathbb{Obj1}_{\mathbf{t1}}, \mathbb{Obj2}_{\mathbf{t2}} )`$. $`K`$ нормирован в диапазоне от $`0`$ (отсутствие сходства) до $`1`$ (полное сходство). Для минимизации последующих записей введём двойственную к $`K`$ меру различия _параметров_ $`D = 1 - K`$. Тогда запись $`D_{\mathrm{Param}}( \mathbb{Obj1}_{\mathbf{t1}}, \mathbb{Obj2}_{\mathbf{t2}} ) < \varepsilon`$ позволит указать количественное требование к сходству влияния, оказываемого _динамикой_, в двух _локалях_ $`\mathbb{Obj1}_{\mathbf{t1}}`$ и $`\mathbb{Obj2}_{\mathbf{t2}}`$.

Состояние **пространства** на указанный момент времени $`\mathbf{t}`$ задаётся:

- множеством существующих в нём _элементарных_ _объектов_ $`\mathbb{Elms}_{\mathbf{t}}`$;
- отношением _связи_ $`\mathbf{Link}_{\mathbf{t}}(a, b)`$ для каждой пары множества _объектов_ $`{a, b}\in\mathbb{Objs}_{\mathbf{t}}`$;
- отношением _близости_ $`\mathbf{Near}_{\mathbf{t}}(a, b)`$ для каждой пары _объектов_ $`{a, b}\in\mathbb{Objs}_{\mathbf{t}}`$;
- _параметрами_ каждого _объекта_.

При изменении состояния **пространства** во времени для каждого _элементарного_ _объекта_ обеспечивается _элементарная инвариантность_.

++_Элементарная инвариантность_++ — это свойство перехода _элементарных_ _объектов_ из предыдущего в последующий момент времени в **пространстве**, согласно которому для двух различающихся моментов $`\mathbf{t1} < \mathbf{t2}`$, если в момент $`\mathbf{t1}`$ существует непустое множество _элементарных_ _объектов_ $`\mathbb{ElmsConst}_{\mathbf{t1}} = \mathbb{Elms}_{\mathbf{t1}} \backslash \mathbb{ElmsDel}_{[\mathbf{t1}; \mathbf{t2})}`$, то в последующий момент $`\mathbf{t2}`$ оно так же существует и представлено в виде множества _элементарных_ _объектов_ $`\mathbb{ElmsConst}_{\mathbf{t2}} = \mathbb{Elms}_{\mathbf{t2}} \backslash \mathbb{ElmsAdd}_{(\mathbf{t1}; \mathbf{t2}]}`$, и при этом указываемые два множества сопоставлены биекцией $`f_{=}`$. Множество $`\mathbb{ElmsDel}_{[\mathbf{t1}; \mathbf{t2})}`$ содержит _элементарные_ _объекты_, удаляемые из **пространства**, которые существуют в момент времени $`\mathbf{t1}`$. Множество $`\mathbb{ElmsAdd}_{(\mathbf{t1}; \mathbf{t2}]}`$ содержит добавляемые в **пространство** _элементарные_ _объекты_, которые существуют в момент времени $`\mathbf{t2}`$:

```math
\begin{gathered}
f_{=} : \mathbb{ElmsConst}_{\mathbf{t1}} \leftrightarrow \mathbb{ElmsConst}_{\mathbf{t2}} \\
\forall{\mathbf{elm}_{\mathbf{t1}}\in{\mathbb{ElmsConst}_{\mathbf{t1}}}}, \\
\exists{\mathbf{elm}_{\mathbf{t2}}\in{\mathbb{ElmsConst}_{\mathbf{t2}}}} : \\
\mathbf{elm}_{\mathbf{t1}}=f_=(\mathbf{elm}_{\mathbf{t1}})=\mathbf{elm}_{\mathbf{t2}}\equiv\mathbf{elm}_{[\mathbf{t1}; \mathbf{t2}]}
\end{gathered}
```

Для многих физических _прикладных областей_ **модель** может быть упрощена введением следующих ограничений на множество $`\mathbb{Elms}_{\mathbf{t}}`$:

- $`\mathbb{Elms}_{\mathbf{t}}`$ - счётное множество;
- множество $`\mathbb{Elms}_{\mathbf{t}}`$ не изменяется во времени, то есть $`\forall{\mathbf{t1},\mathbf{t2}}: \mathbb{ElmsAdd}_{(\mathbf{t1}; \mathbf{t2}]} = \emptyset\ \land\ \mathbb{ElmsDel}_{[\mathbf{t1}; \mathbf{t2})} = \emptyset`$, то есть $`\mathbb{Elms}_{\mathbf{t1}}=\mathbb{Elms}_{\mathbf{t2}}`$.

Но в _прикладных областях_, использующих _символы_, эти ограничения не выполняются, а потому при изменениях **пространства** _элементарные_ _объекты_ могут быть в некоторый момент и добавлены, и удалены, при этом в общем случае множество _элементарных_ _объектов_ может являться несчётным, как, например, «точки на плоскости» в _прикладной области_ «Геометрия».

++==_Элементарное состояние_== **пространства**++ — состояние, при котором $`\mathbb{Elms}_{\mathbf{t}}=\mathbb{Objs}_{\mathbf{t}}`$, то есть отсутствуют _макро-объекты_ $`\forall{a}\in\mathbb{Objs}_{\mathbf{t}}:\mathbf{Elm}(a)`$.

++_Макро-объект_++ $`m`$, существующий на момент времени $`\mathbf{t}`$, задаётся:

- $`\underset{m}{\mathbb{Macro}_{\mathbf{t}}}`$ - множеством составляющих его _объектов_,
- $`\mathbf{Link}_{\mathbf{t}}(a, b)`$ (где $`a,b\in\underset{m}{\mathbb{Macro}_{\mathbf{t}}}`$) - отношениями _связи_ для каждой пары _объектов_ множества $`\underset{m}{\mathbb{Macro}_{\mathbf{t}}}`$, обеспечивающими одну компоненту связности _макро-объекта_ (аналогия со связным графом).

Объединение двух _макро-объектов_ $`m`$ и $`n`$ в один _объект_ $`u`$, в общем случае — осуществимо появлением _связей_ между любыми составляющими _под-объектами_ исходных $`m`$ и $`n`$, поэтому для _макро-объектов_ введем понятие ++_макро-связь_++ $`\mathscr{MacroLink}_{\mathbf{t}}(m, n)`$, описывающее _группу_ отношений _связи_. Эта группа на заданный момент времени $`\mathbf{t}`$ перечисляет все отношения _связей_ между их _под-объектами_:

```math
\begin{gathered}
\mathscr{MacroLink}_{\mathbf{t}}(m,n) \equiv
\mathbf{Link}_{\mathbf{t}}(m_1, n_1) \mid \\
{m_1}\in\underset{m}{\mathbb{Macro}_{\mathbf{t}}}, 
{n_1}\in\underset{n}{\mathbb{Macro}_{\mathbf{t}}},
\end{gathered}
```

Детали работы с _группами_ отношений приведены в следующем разделе, а здесь укажем образец записи наличия _макро-связи_ двух _макро-объектов_ ($`m`$ и $`n`$) - $`\exists{\Big\lbrace \mathscr{MacroLink}_{\mathbf{t}}(m, n) \Big\rbrace}`$ и запись отсутствия _макро-связи_ $`\forall{\Big\lbrace \overline{\mathscr{MacroLink}_{\mathbf{t}}(m, n)} \Big\rbrace}`$.

++_Под-объект_++ _макро-объекта_ $`m`$ - это _объект_ $`s`$, для которого на момент времени $`\mathbf{t}`$ истинно отношение $`\underset{m}{\mathbf{SubObj}_{\mathbf{t}}}(s)`$:

```math
\underset{m}{\mathbf{SubObj}_{\mathbf{t}}}(s) \equiv {s}\in\underset{m}{\mathbb{Macro}_{\mathbf{t}}} \lor \exists{obj}\in\underset{m}{\mathbb{Macro}_{\mathbf{t}}} \land \underset{obj}{\mathbf{SubObj}_{\mathbf{t}}}(s)
```

где $`m,s\in\mathbb{Objs}_{\mathbf{t}}`$.

Отношение $`\mathbf{SubObj}`$ рефлексивно, не симметрично, транзитивно.

++==_Свободный_== _объект_++ в локали $`\mathbb{Loc}`$ на момент времени $`\mathbf{t}`$ - это _объект_ $`a`$, для которого истинно отношение $`\mathbf{Free}_{\mathbf{t}}(\mathbb{Loc}, a)`$, определяющее отсутствие _связей_ _под-объектов_ этого _объекта_ со внешними _объектами_:

```math
\begin{gathered}
\mathbf{Free}_{\mathbf{t}}(\mathbb{Loc}, a) \equiv \forall{x,y,a\in\mathbb{Loc}_{\mathbf{t}}}: 
\underset{a}{\mathbf{SubObj}_{\mathbf{t}}}(x){\ \land\ } \\
\overline{\underset{a}{\mathbf{SubObj}_{\mathbf{t}}}(y)} \to \overline{\mathbf{Link}_{\mathbf{t}}(x,y)}
\end{gathered}
```

Обозначим множество свободных _объектов_ **алгоритмического** **пространства** в момент времени $`\mathbf{t}`$:

```math
 \mathbb{Free}_{\mathbf{t}} \equiv \left\lbrace a \mid a\in\mathbb{Objs}_{\mathbf{t}} \land \mathbf{Free}_{\mathbf{t}}(\mathbb{Objs}, a) \right\rbrace
```

#### 2.1.4. Отношение эквивалентности

Введем не зависящее от момента времени бинарное отношение ++_эквивалентность_++ _объектов_: $`m \sim n`$ или второй вариант записи $`\mathbf{Eq}(m, n)`$, где $`m\in\mathbb{Objs}_{\mathbf{t1}}`$, $`n\in\mathbb{Objs}_{\mathbf{t2}}`$. Для _элементарных_ _объектов_ _эквивалентность_ тривиальна (закрепляется **признаком** **Однородность**). _Эквивалентность_ двух _макро-объектов_ $`m`$ и $`n`$ сходна с изоморфизмом графов со взвешенными вершинами и определяется существованием биекции $`f_{\sim} : \underset{m}{\mathbb{Macro}_{\mathbf{t1}}} \leftrightarrow \underset{n}{\mathbb{Macro}_{\mathbf{t2}}}`$ такой, что:

* $`\forall{m_1} \in \underset{m}{\mathbb{Macro}_{\mathbf{t1}}} : f_{\sim}(m_1) \sim m_1`$ (_эквивалентность_ соответствующих _под-объектов_);
* $`\forall{m_1, m_2} \in  \underset{m}{\mathbb{Macro}_{\mathbf{t1}}} : \mathbf{Link}_{\mathbf{t1}}(m_1, m_2) = \mathbf{Link}_{\mathbf{t2}}(f_{\sim}(m_1), f_{\sim}(m_2))`$ (тождественность _конструкции_ $`m`$ и $`n`$).

++==_Эквивалентные_== множества _объектов_++ - это такие два множества $`\mathbb{Objs1}`$, $`\mathbb{Objs2}`$, для которых существует биекция $`f_{\sim} : \mathbb{Objs1} \leftrightarrow \mathbb{Objs2}`$ такая, что $`\forall{m_1} \in \mathbb{Objs1} : f_{\sim}(m_1) \sim m_1`$

Отношение _эквивалентности_ рефлексивно, симметрично, транзитивно. Введем сопутствующие понятия. При этом учитывая независимость отношения _эквивалентности_ от времени, для каждого следующего понятия укажем два варианта использования. Первый подразумевает формулировку для текущего момента времени и обозначается индексом $`\mathbf{t}`$. Второй вариант указывает на объединение всех моментов времени до текущего (с включением) и обозначается индексом $`\Delta \mathbf{t}`$.

* ++==_Класс_== _эквивалентности_ _объекта_++ $`a\in\mathbb{Objs}_{\mathbf{t}}`$ (сокращённо, _класс_ $`a`$) - задаёт в каждый момент времени множество всех _объектов_ _эквивалентных_ указанному: $`\underset{a}{\mathbb{Class}_{\mathbf{t}}} \equiv \left\lbrace x \mid x\in\mathbb{Objs}_{\mathbf{t}} \land x \sim a \right\rbrace`$. Для объединения по времени будем использовать запись - $`\underset{a}{\mathbb{Class}_{\Delta \mathbf{t}} }`$.
* Фактормножество $`\mathbb{Objs}_{\mathbf{t}} / \sim`$ - множество всех _классов_ _эквивалентности_ в момент времени $`\mathbf{t}`$. Фактормножество является разбиением $`\mathbb{Objs}_{\mathbf{t}}`$ и изменяется при развитии **пространства** во времени. Для объединения по времени будем использовать запись - $`\mathbb{Objs}_{\Delta \mathbf{t}} / \sim`$.
* Унарное отношение ++==_Экземпляр_== _класса_ _объекта_++ $`a`$ (сокращённо, _экземпляр_ $`a`$) - признак принадлежности _объекта_ множеству, задаваемому _классом_ _эквивалентности_ _объекта_ $`a`$: $`\underset{a}{\mathbf{Sample}_{\mathbf{t}}}(x) \equiv x \in \underset{a}{\mathbb{Class}_{\mathbf{t}}}`$, где $`a,x \in\mathbb{Objs}_{\mathbf{t}}`$. Для объединения по времени будем использовать запись $`\underset{a}{\mathbf{Sample}_{\Delta \mathbf{t}}}(x)`$.

Специальным случаем _эквивалентности_ _макро-объектов_ является их _инвариантность_.

++==_Инвариантные_== множества _объектов_++ $`\mathbb{Objs1}_{\mathbf{t1}} = \mathbb{Objs1}_{\mathbf{t2}} = \mathbb{Objs1}`$ в два отличающихся момента времени $`\mathbf{t1}, \mathbf{t2}`$ -- это _эквивалентные_ множества _объектов_ $`{\mathbb{Objs1}_{\mathbf{t1}} \sim \mathbb{Objs1}_{\mathbf{t2}}}`$, для которых соблюдается _элементарная инвариантность_ всех их _элементарных_ _под-объектов_, сопоставленных биекцией _эквивалентности_ $`( f_{\sim} : \mathbb{Objs1}_{\mathbf{t1}} \leftrightarrow \mathbb{Objs2}_{\mathbf{t2}} )`$:

```math
\forall{obj_1} \in \mathbb{Objs1}_{\mathbf{t1}} : 
\mathbf{Elm}(obj_1) \to f_{\sim}(obj_1) = obj_1
```

#### 2.1.5. Дополнительные обозначения

Для работы со множествами _объектов_ введем следующие записи.

* Множество всех _под-объектов_ множества $`\mathbb{Obj1}_{\mathbf{t}}`$ обозначим: $`{\langle \mathbb{Obj1}_{\mathbf{t}} \rangle} \equiv \left\lbrace b \mid b \in  \underset{a}{\mathbb{Macro}_{\mathbf{t}}} \land a \in \mathbb{Obj1}_{\mathbf{t}} \right\rbrace`$.
* Множество всех удовлетворяющих предикату $`\mathbf{Pred}(o)`$ _под-объектов_ множества $`\mathbb{Obj1}_{\mathbf{t}}`$ обозначим: $`{\langle \mathbb{Obj1}_{\mathbf{t}} : \mathbf{Pred} \rangle} \equiv \left\lbrace b \mid b \in  \underset{a}{\mathbb{Macro}_{\mathbf{t}}} \land \mathbf{Pred}(b) \land a \in \mathbb{Obj1}_{\mathbf{t}} \right\rbrace`$. Например, чтобы указать множество всех _элементарных_ _под-объектов_ множества объектов $`\mathbb{Obj1}_{\mathbf{t}}`$ будем использовать запись: $`{\langle \mathbb{Obj1}_{\mathbf{t}} : \mathbf{Elm} \rangle}`$.
* Отношение дополнения для множества _объектов_ $`\mathbb{Obj2}_{\mathbf{t}}`$ (обозначим $`\backslash \mathbb{Obj2}_{\mathbf{t}}`$) будет содержать все _объекты_ **пространства** в момент времени $`\mathbf{t}`$ за вычетом всех _объектов_ $`{\langle \mathbb{Obj2}_{\mathbf{t}} \rangle}`$. То есть $`\backslash \mathbb{Obj2}_{\mathbf{t}} \equiv \mathbb{Objs}_{\mathbf{t}} \backslash {\langle \mathbb{Obj2}_{\mathbf{t}} \rangle}`$.

Для описания _конструкции_ _макро-объектов_ введем формальную запись шаблона _макро-объекта_. Так для _макро-объекта_ $`\underset{m}{\mathbb{Macro}_{\mathbf{t}}}`$, состоящего из трех _под-объектов_ эквивалентных $`\mathbb{obj1}, \mathbb{obj2}, \mathbb{obj3}`$, два из которых ($`\mathbb{obj1}, \mathbb{obj2}`$) объединены в _макро-объект_ $`\underset{m1}{\mathbb{Macro}_{\mathbf{t}}}`$, _связанный_ в свою очередь с $`\mathbb{obj3}`$, запись будет следующей:

```math
\begin{aligned}
& \underset{m}{\mathbb{Macro}_{\mathbf{t}}} 
\langle \mathbb{a}, \mathbb{b}, \mathbb{c} \rangle 
\\
& \begin{cases}
& 
\underset{obj1}{\mathbf{Sample}}(\mathbb{a}) \land
\underset{obj2}{\mathbf{Sample}}(\mathbb{b}) \land
\underset{obj3}{\mathbf{Sample}}(\mathbb{c}) 
\\
& \underset{m1}{\mathbb{Macro}_{\mathbf{t}}} 
\langle \mathbb{d}, \mathbb{e} \rangle \lbrace \mathbf{Link}(\mathbb{d}, \mathbb{e})  \rbrace
\\
& \mathbb{inner1} \equiv \underset{m1}{\mathbb{Macro}_{\mathbf{t}}} 
\langle \mathbb{a}, \mathbb{b} \rangle
\\
& \mathbf{Link}(\mathbb{inner1}, \mathbb{c}) 
\end{cases}
\end{aligned}
```

Чтобы отличать два _эквивалентных_ _объекта_ в одной записи введем пред-индекс и тогда в записи: $`{^1}\mathbb{obj1}, {^2}\mathbb{obj1}, {^1}\mathbb{obj2}`$ - указаны два _экземпляра_ _объекта_ _эквивалентного_ $`\mathbb{obj1}`$ и один _экземпляр_ $`\mathbb{obj2}`$. Для конкретного _экземпляра_ описанного выше _макро-объекта_ $`\underset{m}{\mathbb{Macro}_{\mathbf{t}}}`$ будем использовать запись:

```math
{^1}\mathbb{mobj} \equiv \underset{m}{\mathbb{Macro}_{\mathbf{t}}} 
\langle {^2}\mathbb{obj1}, {^1}\mathbb{obj2}, {^1}\mathbb{obj3} \rangle
```

#### 2.1.6. Группы отношений

Для удобства дальнейшего изложения введем сокращённые записи для описания отношений с множествами _объектов_. Каждое такое отношение на множестве _объектов_ формирует ++==_группу_== отношений++. Для обозначения имени группы отношений будем использовать курсивное начертание $`\mathscr{RelGroup}`$.

##### 2.1.6.1. Отношения на множестве объектов

Для сокращения записи _группы_ всех отношений $`\mathbf{Relation}(a, b)`$ для всех _объектов_ некоторого множества $`\mathbb{Obj1}_{\mathbf{t}}`$ введем обозначение $`\mathbf{Relation}(\mathbb{Obj1}_{\mathbf{t}})`$ для группы внутренних отношений $`\mathscr{Inner}_{\mathbf{Relation}}`$ на множестве _объектов_, задаваемой следующим выражением:

```math
\mathbf{Relation}(\mathbb{Obj1}_{\mathbf{t}}) \equiv \mathscr{Inner}_{\mathbf{Relation}}(\mathbb{Obj1}_{\mathbf{t}}) = \mathbf{Relation}(a,b) \mid a,b\in\mathbb{Obj1}_{\mathbf{t}}
```

Определим ++==_покрытое_== отношениями множество _объектов_++ на примере множества _объектов_ $`\mathbb{Obj1}_{\mathbf{t}}`$, _покрытого_ отношениями _связи_ (запись $`\mathbf{Link}( {\langle \mathbb{Obj1}_{\mathbf{t}} \rangle} )`$), в котором задаётся перечисление отношений внутренних _связей_ для всех _под-объектов_ множества $`\mathbb{Obj1}_{\mathbf{t}}`$. Такая запись является сокращением следующего выражения:

```math
\mathbf{Link}(a,b) \mid a,b\in{\langle \mathbb{Obj1}_{\mathbf{t}} \rangle}
```

Введем переход от группы отношений на множестве _объектов_ к множеству, содержащему только истинные значения отношений в этой группе. Для этого будем использовать обозначение $`\Big\lbrace \mathscr{RelGroup}(...) \Big\rbrace`$. Так выражение $`\Big\lbrace \mathbf{Link}( \mathbb{Obj1}_{\mathbf{t}} ) \Big\rbrace`$ определяет множество всех присутствующих на момент времени $`\mathbf{t}`$ _связей_ между _объектами_ множества $`\mathbb{Obj1}`$.

##### 2.1.6.2. Отношения между двумя множествами объектов

Введем краткую запись $`\mathbf{Relation2}_{\mathbf{t}}(\mathbb{Obj1}, \mathbb{Obj2})`$ для группы отношений $`\mathscr{Pair}_{\mathbf{Relation2}}`$ между двумя множествами объектов $`\mathbb{Obj1}, \mathbb{Obj2}`$.

```math
\begin{gathered}
\mathscr{Pair}_{\mathbf{Relation2}}(\mathbb{Obj1}, \mathbb{Obj2}) \equiv \\ 
\mathbf{Relation2}_{\mathbf{t}}(a,b) \mid a\in\mathbb{Obj1}_{\mathbf{t}}, b\in\mathbb{Obj2}_{\mathbf{t}}
\end{gathered}
```

Дополнительным обозначением установим следующую запись:

```math
\mathbf{Relation2}_{\mathbf{t}}(\mathrm{obj1},\mathbb{Obj2}) \equiv 
\mathbf{Relation2}_{\mathbf{t}}(\lbrace \mathrm{obj1} \rbrace,\mathbb{Obj2})
```

Отметим что в записи отношений, определённых внутри одного момента времени, для упрощения записи возможно опускание индекса времени у объектов или у отношения, но необходимо наличие хотя бы одного его указания.

Особо стоит выделить _эквивалентность_ двух множеств _объектов_ $`\mathbb{Obj1}_{\mathbf{t1}} \sim \mathbb{Obj2}_{\mathbf{t2}}`$. Это выражение определяет не группу отношений, а одно отношение, проверяющее существование биекции $`f_{\sim} : \mathbb{Obj1}_{\mathbf{t1}} \leftrightarrow \mathbb{Obj2}_{\mathbf{t2}}`$ такой, что $`\forall m_1 \in \mathbb{Obj1}_{\mathbf{t1}} : f_{\sim}(m_1) \sim m_1`$.

Для сопоставления отношений между _под-объектами_ двух _эквивалентных_ множеств _объектов_ целесообразно ввести краткую запись, описанную далее. Рассмотрим в качестве примера сравнение _близости_ для двух _эквивалентных_ множеств _объектов_ $`\mathbb{Obj1}_{\mathbf{t1}} \sim \mathbb{Obj2}_{\mathbf{t2}}`$. _Эквивалентность_ задаётся биекцией $`f_{\sim} : \mathbb{Obj1}_{\mathbf{t1}} \leftrightarrow \mathbb{Obj2}_{\mathbf{t2}}`$. Тогда краткая запись:

```math
\mathbf{Near}(\mathbb{Obj1}_{\mathbf{t1}}) \stackrel{ \mathbb{Obj1}_{\mathbf{t1}} \sim \mathbb{Obj2}_{\mathbf{t2}} }{=} \mathbf{Near}(\mathbb{Obj2}_{\mathbf{t2}})
```

тождественна следующей группе отношений $`\mathscr{Eq}_{ \lbrace \mathbf{Near}, = \rbrace }`$:

```math
\begin{gathered}
{\mathscr{Eq}_{ \lbrace \mathbf{Near}, = \rbrace }(\mathbb{Obj1}_{\mathbf{t1}}, \mathbb{Obj2}_{\mathbf{t2}})} \equiv \\ 
{\mathbf{Near}_{\mathbf{t1}}(m_1, m_2)} = 
{\mathbf{Near}_{\mathbf{t2}}(f_{\sim}(m_1), f_{\sim}(m_2))} \mid \\ 
{m_1, m_2 \in \mathbb{Obj1}_{\mathbf{t1}}}, \\ 
{f_{\sim} :  \mathbb{Obj1}_{\mathbf{t1}} \leftrightarrow \mathbb{Obj2}_{\mathbf{t2}}}
\end{gathered}
```

##### 2.1.6.3. Высказывание по группе отношений

Для использования в работе введённых групп отношений необходимо описать операцию формирования высказывания по группе отношений. Обозначаться такое высказывание будет фигурными скобками вокруг группы с указанием уникального имени, выбранного для обозначения фиксированного набора значений всех отношений из указанной группы, например $`\rule{0pt}{8pt}^{start} \Big\lbrace\mathscr{Inner}_{\mathbf{Link}}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$. Возможно несколько вариантов указания требований к значениям отношений внутри группы:

* требование истинности всех отношений в группе, задаётся знаком $`\forall`$ и наименование для этого случая можно опустить. Например, $`\forall \Big\lbrace\mathscr{Inner}_{\mathbf{Link}}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$ или тождественная запись $`\forall \Big\lbrace\mathbf{Link}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$ - утверждение о наличии _связи_ для любой пары _объектов_ из множества $`\mathbb{Obj1}`$;
* требование истинности хотя бы одного отношения из группы, задаётся знаком $`\exists`$ и наименование для этого случая можно опустить. Например, $`\exists \Big\lbrace\mathbf{Link}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$ - утверждение о наличии _связи_ хотя бы одной пары _объектов_ из множества $`\mathbb{Obj1}`$;
* требование истинности конкретного числа $`N`$ отношений из группы обозначается именем $`{Count}(N)`$. Например, $`\rule{0pt}{8pt}^{{Count}(2)} \Big\lbrace\mathbf{Link}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$ - утверждение о наличии только двух _связей_ для всех пар _объектов_ из множества $`\mathbb{Obj1}`$;
* именованное требование истинности только перечисленных отношений из группы. Для краткой записи именованного требования используется, следующая запись:  $`\rule{0pt}{8pt}^{Some} \Big\lbrace\mathbf{Link}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace`$. Например, утверждение о наличии только двух _связей_ для пар _объектов_ $`(\mathrm{obj1}, \mathrm{obj2})`$, $`(\mathrm{obj2}, \mathrm{obj3})`$ из множества $`\mathbb{Obj1}`$.

```math
\rule{0pt}{8pt}^{Some} \Big\lbrace\mathbf{Link}(\mathbb{Obj1}_{\mathbf{t}})\Big\rbrace \equiv \mathbf{Link}(\mathrm{obj1}, \mathrm{obj2}) \land \mathbf{Link}(\mathrm{obj2}, \mathrm{obj3})
```

[^2-1--model]: _Модель_ может быть представлена примерами использования:
    
    * поле в алгебре,
    * физическая модель гравитационного (сильного, слабого...) взаимодействия,
    * способы однозначного соответствия между абстрактными материальной точкой, прямой, плоскостью в геометрии и объектами с практическим применением в деятельности человека, например, в строительстве дома или в возведении египетской пирамиды;
    * физическая модель идеального газа как набор допущений обеспечивающий _трансляцию_: не взаимодействующие друг с другом частицы газа, упругие столкновения частиц газа только со стенками сосуда, отсутствие между частицами дальнодействующего взаимодействия.
    
[^2-1--space]: _Пространство_ может быть представлено примерами:
    
    * описание устройства машины Тьюринга (лента, управляющее устройство, правила перехода, алфавит),
    * вводимое множество с заданным на нём набором операций и отношений в общей алгебре,
    * план сражения на столе у военачальника;
    * геометрические построения на рисунке у архитектора египетской пирамиды;
    * формулы расчета состояний и изменений в идеальном газе.
    
[^2-1--fissility]: **Делимость** проявляется существованием понятий:
    
    * элементарная частица, атом, молекула, клетка, организм, стая, экосистема;
    * атом, кристалл, камень, планеты, звёзды, галактики;
    * человек, семья, общество жителей дома, горожане, жители области, граждане страны, население планеты;
    * элементарный оператор, процедура, программный модуль, программный компонент, программа, комплекс программ, программная операционная система, распределённая сеть программных систем.
    
[^2-1--uniform]: **Однородность** проявляется:
    
    * повторимость эксперимента на одних исходных данных;
    * одинаковый результат алгоритма на одинаковых входных данных;
    * всегда и везде получение воды в результате взаимодействия и соединения атомов кислорода и водорода;
    * неотличимость друг от друга двух атомов кислорода (изотопа $`\overset{16}{}O`$) в одинаковых условиях;
    * неотличимость старого и дополнительного тиража книги кулинарных рецептов в процессе приготовления салата.
    
[^2-1--locality]: **Локальность** проявляется:
    
    * возможность химической реакции при перемешивании компонентов и невозможность при нахождении в разных банках;
    * выполнение процессором текущей команды и игнорирование всех остальных;
    * возможность создания салата рядом с полным ингредиентов холодильником и невозможность при пустом.
    
[^2-1--hierarchy]: **Иерархичность** проявляется:
    
    * возможность объединения атомов в молекулу;
    * молекул - в кристаллическую решётку;
    * клеток - в орган;
    * клеток - в организм;
    * множества организмов одного вида - в стаю;
    * множество организмов местности - в экосистему.
    
[^2-1--effect]: **Эффектность** проявляется:
    
    * способность объекта-повар приготовить салат;
    * способность ЭВМ выполнить программу;
    * возможность реакции окисления между кислородом и углеродом;
    * способность фермента реннина(химозина) ускорять процесс гидролизации и расщепления белка казеиногена;
    * способность содержимого ячейки памяти ЭВМ влиять на выполнение изменений других ячеек.
    
[^2-1--dynamism]: **Динамичность** проявляется внешним воздействием «среды» на объекты:
    
    * гравитационное притяжение планеты в процессах строительства здания;
    * способность программиста подменить содержимое переменной в отладке программы по отношению к процессам выполнения программы;
    * возможность реакции $`3O_2 \rightarrow 2O_3`$ при облучении ультрафиолетовым излучением;
    * повышение температуры газовой горелкой для запуска химической реакции в колбе;
    * процессы закупки продуктов в холодильник по отношении к процессу приготовления салата на кухне;
    * написание кода программистом по отношению к его выполнению на ЭВМ.

[^2-1--enclass]: **Классифицируемость** проявляется:

    * различие процессов для твердых, жидких и газообразных веществ;
    * различие свойств частиц, имеющих и не имеющих электрический заряд;
    * отличие воздействия на среду биологического организма и неживого объекта;
    * периодическая система химических элементов;
    * отличие вирусной и офисной программы;
    * различие возможностей участия в рабочих процессах между ребенком и взрослым;
    * деление алгоритмов по вычислительной сложности на константные, линейные, полиномиальные, экспоненциальные от размера входа;
    * биологическая систематика.
    
[^2-1--object]: _Объект_ может быть представлен примерами, приведёнными в сноске **признака** **Делимость**.
    
[^2-1--interaction]: _Взаимодействие_ может быть представлено примерами:
    
    * отталкивание одинаково заряженных элементов;
    * отталкивание воды от гидрофобного материала;
    * социальное поведение в семье;
    * симбиоз живых организмов;
    * работа фермента по отношению к субстрату;
    * влияние текущей инструкции на данные в программе ЭВМ.
    
[^2-1--connection]: _Связь_ может быть представлена примерами:
    
    * ковалентная химическая связь;
    * связь дочернего и родительского компонента в UI;
    * связи в структуре двунаправленный список;
    * заклёпочное крепление двух деталей;
    * экономическое сотрудничество компаний.
    
[^2-1--construct]: _Конструкция_ может быть представлена примерами:
    
    * иерархия государственных субъектов;
    * формообразующее объединение элементов связями в бензольном кольце;
    * структура красно-чёрное дерево;
    * структура фермента (третичная, четвертичная);
    * система шестеренок в часовом механизме;
    * структура программы от элементарных операторов и переменных, до объектов и компонентов.
    
[^2-1--dynamic]: _Динамика_ может быть представлено примерами, приведёнными в сноске к **признаку** **Динамичность**.
    
[^2-1--transform]: _Преобразование_ может быть представлено примерами, приведёнными в сноске к **признаку** **Эффектность**.
    
[^2-1--locale]: _Локаль_ может быть представлена примерами:
    
    * семья;
    * ореол обитания биологического вида;
    * внутриклеточное пространтсво бактерии;
    * биологический организм;
    * программная процедура;
    * программный класс;
    * организация в юридическом поле;
    * пробирка с химическим реагентом.
    
[^2-1--object-param]: _Параметры_ _объекта_ могут быть представлены примерами:
    
    * скорость, ускорение физического тела;
    * температура реактива в пробирке;
    * масса как мера влияния гравитационного притяжения, например для яблок на дереве;
    * электростатический потенциал;
