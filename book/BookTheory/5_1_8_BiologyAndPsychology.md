#### 5.1.8. Биология и психология

<todo>

- Теория эволюции (постулирование **развития** _организмов_);
- «Эгоистичный ген» (центральная роль **структуры** «генетического» **алгоритма** в эволюции _организмов_ и **алгоритма** в их жизни);
- «Эволюционная теория пола» (как способ создания устойчивой системы **синтеза** новых биологических **алгоритмов**);
- Организм - образец успешных приемов развития архитектуры;
- Функции и структуры мозга;
- Выявление закономерностей и особенностей поведения _организмов_ разного эволюционного «уровня».

</todo>