#### 2.4.3. Формализация машины Тьюринга

На данном этапе работы важным является формирование метода формальной записи конкретного **алгоритма** в разработанной **модели**. Приведем далее образец такой записи. Для этого опишем формализацию «машины Тьюринга» в виде _контактного_ _конструкционного_ **алгоритма**.

Абстрактная вычислительная машина Тьюринга в терминах **модели** представляет собой _реализацию_ _универсальной_ _организации_, обеспечивающей _контроль_ _порядка_ _исполнения_ в _алгоритмической группе_. _Порядок_ _исполнения_ поддерживается устройством двух основных элементов машины Тьюринга:

- ленты — бесконечной последовательности ячеек, в каждой из которых может быть записан символ конечного алфавита.
- управляющего устройства, способного взаимодействовать с одной ячейкой и перемещаться к левой и правой соседней ячейке, при этом способ взаимодействия определяется состоянием устройства, и это состояние выбирается из конечного множества состояний.

Взаимодействие между ячейкой и управляющим устройством обусловлено:

- текущим состоянием устройства;
- символом, хранимым в текущей ячейке;
- сопоставленным с символом ячейки текущим правилом текущего состояния.

Результаты взаимодействия зависят от текущего правила, заданного текущим состоянием, и возможно сочетание следующих результатов:

- изменение символа хранимого в ячейке;
- возможно относительное перемещение ленты и управляющего устройства до его совмещения с правой или левой соседней ячейкой (относительно текущего расположения);
- переход управляющего устройства в новое состояние;
- завершение работы в случае перехода устройством в состояние, помеченное терминальным.

Ключевым, но не единственным в описании является _взаимодействие_ содержимого текущей ячейки и текущего правила в текущем состоянии.

Формализуем одно из возможных описаний машины Тьюринга в виде _алгоритмической группы_ $`\mathbb{GroupTM}`$, обеспечивающей возможность _исполнения_ _макро-алгоритма_ $`\mathtt{AlgTM}`$.

##### 2.4.3.1. Формула объектов

```math
\mathbb{GroupTM} \equiv \underset{\mathrm{tape}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{control}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{inner}}{\mathbb{Macro}}
```

```math
\mathtt{AlgTM}^{\mathbf{t1}\rightarrow\mathbf{t4}} \left(
\underset{\mathrm{tape}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{control}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{inner}}{\mathbb{Macro}}
\right)
```

В **алгоритме** _макро-объект_ $`\underset{\mathrm{control}}{\mathbb{Macro}}`$ - это управляющее устройство, и этот объект является _работником_ **алгоритма**:

```math
\mathbf{Worker}_{\mathtt{AlgTM}} \left( \underset{\mathrm{control}}{\mathbb{Macro}}
\right)
```

_Макро-объект_ $`\underset{\mathrm{tape}}{\mathbb{Macro}}`$ - это лента, и этот _объект_ является _материалом_ **алгоритма**:

```math
\mathbf{Material}_{\mathtt{AlgTM}} \left( \underset{\mathrm{tape}}{\mathbb{Macro}}
\right)
```

Дополнительным _материалом_ **алгоритма** является _макро-объект_ $`\underset{\mathrm{inner}}{\mathbb{Macro}}`$, хранящий внутреннее состояние **алгоритма**:

```math
\mathbf{Material}_{\mathtt{AlgTM}} \left( \underset{\mathrm{inner}}{\mathbb{Macro}}
\right)
```

##### 2.4.3.2. Макрообъект «Лента»

В _цепи_ $`\underset{\mathrm{tape}}{\mathbb{Macro}}`$ выделимы _элементы_ («ячейки») $`\underset{\mathrm{cell}_i}{\mathbb{Macro}}`$:

```math
\forall\ i \in \mathbb{Z}:
\underset{\mathrm{cell}_i}{\mathbb{Macro}} \subset \underset{\mathrm{tape}}{\mathbb{Macro}}
```

```math
\forall\ i1, i2 \in \mathbb{Z}: i1 \neq i2 \implies
\underset{\mathrm{cell}_{i1}}{\mathbb{Macro}}
\cap
\underset{\mathrm{cell}_{i2}}{\mathbb{Macro}} = \emptyset
```

```math
\bigg(\ldots, \underset{\mathrm{cell}_{i-1}}{\mathbb{Macro}}, \underset{\mathrm{cell}_{i}}{\mathbb{Macro}}, \underset{\mathrm{cell}_{i+1}}{\mathbb{Macro}}, \underset{\mathrm{cell}_{i+2}}{\mathbb{Macro}}, \ldots\bigg)
```

В каждой ячейке может быть закодирован символ конечного алфавита (с общим количеством символов равным $`K`$). Символ будет кодироваться наличием _связей_ _под-объектов_ ячейки.

```math
\rule{0pt}{12pt}^{SomeLink} \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{cell}_i}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

```math
\underset{\mathrm{cell}_i}{\mathbb{Macro}} \sim
\underset{\mathrm{cell}}{\mathbb{Macro}}
```

Для упрощения записи, но без потери общности рассмотрим алфавит состоящий из двух символов $`\mathscr{Letter}_k`$, где $`k = \overline{1,K}`$ и $`K=2`$. При этом для удобства обозначим $`\mathscr{LetterA} \equiv \mathscr{Letter}_1`$ и $`\mathscr{LetterB} \equiv \mathscr{Letter}_2`$. Символы будем кодировать наличием связи между двумя _объектами_ $`\mathrm{obj\_cell1}`$, $`\mathrm{obj\_cell2}`$ множества $`\underset{\mathrm{cell}}{\mathbb{Macro}}`$:

```math
\mathscr{LetterA}\left(
\underset{\mathrm{cell}}{\mathbb{Macro}}
\right) \equiv
\Big\lbrace
\mathbf{Link} (\mathrm{obj\_cell1}, \mathrm{obj\_cell2})
\Big\rbrace
```

```math
\mathscr{LetterB}\left(
\underset{\mathrm{cell}}{\mathbb{Macro}}
\right) \equiv
\Big\lbrace
\overline{
\mathbf{Link} (\mathrm{obj\_cell1}, \mathrm{obj\_cell2})
}
\Big\rbrace
```

##### 2.4.3.3. Макрообъект «Управляющее устройство»

В $`\underset{\mathrm{control}}{\mathbb{Macro}}`$ выделимы:

- набор состояний $`\underset{\mathrm{state\_mas}}{\mathbb{Macro}}`$,

- $`N`$ состояний $`\underset{\mathrm{state}_{[1 \ldots N]}}{\mathbb{Macro}}`$,

```math
j = \overline{1,N}:
\underset{\mathrm{state}_j}{\mathbb{Macro}} \subset \underset{\mathrm{state\_mas}}{\mathbb{Macro}}
```

- _объект_, обеспечивающий перемещение управляющего устройства относительно ленты и переход к следующему состоянию управляющего устройства,

```math
\underset{\mathrm{mover}}{\mathbb{Macro}} \subset \underset{\mathrm{control}}{\mathbb{Macro}}
```

##### 2.4.3.4. Макрообъект «Внутренний»

В $`\underset{\mathrm{inner}}{\mathbb{Macro}}`$ выделимы:

- _объект_, хранящий информацию в какую сторону необходимо переместить управляющее устройства относительно ленты

```math
\underset{\mathrm{next\_move}}{\mathbb{Macro}} \subset \underset{\mathrm{inner}}{\mathbb{Macro}}
```

- _объект_, хранящий информацию к какому следующему состоянию управляющего устройства необходимо перейти

```math
\underset{\mathrm{next\_state}}{\mathbb{Macro}} \subset \underset{\mathrm{inner}}{\mathbb{Macro}}
```

##### 2.4.3.5. Стартовые условия

Задано стартовое состояние $`\mathbf{t1}`$, характеризуемое связью текущей ячейки $`ic`$ и текущего состояния $`jc`$:

```math
\mathbf{Link}_{\mathbf{t1}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state}_{jc}}{\mathbb{Macro}}
\right)
```

##### 2.4.3.6. Реализация группировкой

Реализация **алгоритма** $`\mathtt{AlgTM}`$ производится методом **группировки** _под-алгоритмов_ $`\mathtt{AlgState}`$ , $`\mathtt{AlgMove}`$:

##### 2.4.3.7. Под-алгоритм AlgMove

**Алгоритм** $`\mathtt{AlgMove}`$ анализирует содержимое внутреннего состояния, реализует:

- смещение управляющего устройства к одной из соседних ячеек;
- завершение выполнения **алгоритма** $`\mathtt{AlgTM}`$;
- переход управляющего устройства в другое состояние.

Опишем **алгоритм** $`\mathtt{AlgMove}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgMove}^{\mathbf{t2}\rightarrow\mathbf{t4}} \left( \mathbb{ObjMove}
\right)
\\
\mathbb{ObjMove} \equiv
\underset{\mathrm{mover}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic-1,\ ic,\ ic+1}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgMove}} \left(
\underset{\mathrm{mover}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}},
\underset{\mathrm{cell}_{ic-1,\ ic,\ ic+1}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgMove}} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}},
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
```

В _макро-объекте_ $`\underset{\mathrm{next\_move}}{\mathbb{Macro}}`$ хранится символ конечного алфавита. Этот символ кодируется наличием _связей_ его _под-объектов_.

```math
\rule{0pt}{12pt}^{SomeLink} \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

Необходим алфавит из четырёх символов $`\mathscr{MoveNone}`$, $`\mathscr{MoveLeft}`$, $`\mathscr{MoveRight}`$ и $`\mathscr{MoveEnd}`$, которые можно закодировать наличием связи между тремя одинаковыми _объектами_ $`\mathrm{obj\_move}`$ множества $`\underset{\mathrm{next\_move}}{\mathbb{Macro}}`$:

```math
\mathscr{MoveNone}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right) \equiv
\forall\ \Bigg\lbrace
\neg
\mathbf{Link} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

```math
\mathscr{MoveLeft}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right) \equiv
\rule{0pt}{12pt}^{{Count}(1)} \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

```math
\mathscr{MoveRight}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right) \equiv
\rule{0pt}{12pt}^{{Count}(2)} \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

```math
\mathscr{MoveEnd}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right) \equiv
\forall\ \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

- реализация группировкой:

Реализация **алгоритма** $`\mathtt{AlgMove}`$ производится методом **группировки** _под-алгоритмов_ $`\mathtt{AlgMoveLeft}`$, $`\mathtt{AlgMoveRight}`$, $`\mathtt{AlgMoveEnd}`$, $`\mathtt{AlgSelectState}`$.

##### 2.4.3.8. Под-алгоритм AlgMoveLeft

Опишем **алгоритм** $`\mathtt{AlgMoveLeft}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgMoveLeft}^{\mathbf{t2}\rightarrow\mathbf{t3}} \left( \mathbb{ObjMoveLeft}
\right)
\\
\mathbb{ObjMoveLeft} \equiv
\underset{\mathrm{mover}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic-1,\ ic}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgMoveLeft}} \left(
\underset{\mathrm{mover}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}},
\underset{\mathrm{cell}_{ic-1,\ ic}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgMoveLeft}} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
```

- стартовые условия:

```math
\begin{gathered}
\mathbf{Condition}_{\mathtt{AlgMoveLeft}}
{\ \equiv}\\
{\equiv\ }
\mathscr{MoveLeft}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathbf{Link}_{\mathbf{t2}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
\end{gathered}
```

- результаты:

```math
\begin{gathered}
\mathbf{Result}_{\mathtt{AlgMoveLeft}}
{\ \equiv}\\
{\equiv\ }
\mathscr{MoveNone}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\neg
\mathbf{Link}_{\mathbf{t3}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathbf{Link}_{\mathbf{t3}} \left(
\underset{\mathrm{cell}_{ic-1}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
\end{gathered}
```

##### 2.4.3.9. Под-алгоритм AlgMoveRight

**Алгоритм** $`\mathtt{AlgMoveRight}`$ во всем аналогичен $`\mathtt{AlgMoveLeft}`$, кроме использования _объекта_ $`\underset{\mathrm{cell}_{ic+1}}{\mathbb{Macro}}`$ вместо _объекта_ $`\underset{\mathrm{cell}_{ic-1}}{\mathbb{Macro}}`$ и условия $`\mathscr{MoveRight}\left( \underset{\mathrm{next\_move}}{\mathbb{Macro}} \right)`$ вместо условия $`\mathscr{MoveLeft}\left( \underset{\mathrm{next\_move}}{\mathbb{Macro}} \right)`$.

##### 2.4.3.10. Под-алгоритм AlgMoveEnd

Опишем **алгоритм** $`\mathtt{AlgMoveEnd}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgMoveEnd}^{\mathbf{t2}\rightarrow\mathbf{t3}} \left( \mathbb{ObjMoveEnd}
\right)
\\
\mathbb{ObjMoveEnd} \equiv
\underset{\mathrm{mover}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgMoveEnd}} \left(
\underset{\mathrm{mover}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}},
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgMoveEnd}} \left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
```

- стартовые условия:

```math
\begin{gathered}
\mathbf{Condition}_{\mathtt{AlgMoveEnd}}
{\ \equiv}\\
{\equiv\ }
\mathscr{MoveEnd}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathbf{Link}_{\mathbf{t2}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
\end{gathered}
```

- результаты:

```math
\begin{gathered}
\mathbf{Result}_{\mathtt{AlgMoveEnd}}
{\ \equiv}\\
{\equiv\ }
\mathscr{MoveNone}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\neg
\mathbf{Link}_{\mathbf{t3}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
\end{gathered}
```

##### 2.4.3.11. Под-алгоритм AlgSelectState

Опишем **алгоритм** $`\mathtt{AlgSelectState}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgSelectState}^{\mathbf{t3}\rightarrow\mathbf{t4}} \left( \mathbb{ObjS}
\right)
\\
\mathbb{ObjS} \equiv
\underset{\mathrm{mover}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgSelectState}} \left(
\underset{\mathrm{mover}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}},
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgSelectState}} \left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
```

В _макро-объекте_ $`\underset{\mathrm{next\_state}}{\mathbb{Macro}}`$ хранится символ конечного алфавита. Этот символ кодируется наличием _связей_ его _под-объектов_.

```math
\rule{0pt}{12pt}^{SomeLink} \Bigg\lbrace
\mathbf{Link} \left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

Необходим алфавит из конечного множества символов с мощностью, на 1 большей чем мощность множества состояний управляющего устройства. Для этого можно использовать индексированное
множество пар _объектов_ $`\underset{\mathrm{next\_state}}{\mathbb{Macro}}`$:

```math
j = \overline{1,N}: \mathrm{obj\_a}_j, \mathrm{obj\_b}_j \in \underset{\mathrm{next\_state}}{\mathbb{Macro}}
```

```math
\mathscr{State}_j\left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right) \equiv
\Bigg\lbrace
\mathbf{Link} \left(
\mathrm{obj\_a}_j, \mathrm{obj\_b}_j
\right)
\Bigg\rbrace
```

```math
\mathscr{StateNone}\left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right) \equiv
\forall \Bigg\lbrace
\neg
\mathbf{Link} \left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
\Bigg\rbrace
```

- стартовые условия:

```math
\begin{gathered}
\mathbf{Condition}_{\mathtt{AlgSelectState}}
{\ \equiv}\\
{\equiv\ }
\mathscr{State}_{jNext}\left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathscr{MoveNone}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathbf{Link}_{\mathbf{t3}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
\end{gathered}
```

- результаты:

```math
\begin{gathered}
\mathbf{Result}_{\mathtt{AlgSelectState}}
{\ \equiv}\\
{\equiv\ }
\mathscr{StateNone}\left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\neg
\mathbf{Link}_{\mathbf{t4}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state\_mas}}{\mathbb{Macro}}
\right)
{\wedge}\\
{\wedge\ }
\mathbf{Link}_{\mathbf{t4}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{state_{jNext}}}{\mathbb{Macro}}
\right)
\end{gathered}
```

##### 2.4.3.12. Под-алгоритм AlgState

Настраиваемый пользователем **алгоритм** $`\mathtt{AlgState}_{jc}`$ анализирует содержимое текущей ячейки и может поменять это содержимое, инициализировать переход в другое состояние, инициализировать смещение к одной из соседних ячеек, инициировать завершение выполнения **алгоритма** $`\mathtt{AlgTM}`$.

Опишем **алгоритм** $`\mathtt{AlgState}_{jc}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgState}_{jc}^{\mathbf{t1}\rightarrow\mathbf{t2}} \left( \mathbb{ObjSj}
\right)
\\
\mathbb{ObjSj} \equiv
\underset{\mathrm{state_{jc}}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgState}_{jc}} \left(
\underset{\mathrm{state_{jc}}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgState}_{jc}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{next\_move}}{\mathbb{Macro}},
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
```

В общем случае в $`\underset{\mathrm{state_{jc}}}{\mathbb{Macro}}`$ выделимы $`K`$ правил преобразования $`k = \overline{1,K}: \underset{\mathrm{rule_{jc,k}}}{\mathbb{Macro}} \subset \underset{\mathrm{state_{jc}}}{\mathbb{Macro}}`$.

- реализация группировкой:

Реализация **алгоритма** $`\mathtt{AlgState}_{jc}`$ производится методом **группировки** _под-алгоритмов_ $`\mathtt{AlgStateRule}_{jc,k}`$.

##### 2.4.3.13. Под-алгоритм AlgStateRule

Опишем структуру **алгоритмов** $`\mathtt{AlgStateRule}_{jc,k}`$:

- формула _объектов_:

```math
\begin{gathered}
\mathtt{AlgStateRule}_{jc,k}^{\mathbf{t1}\rightarrow\mathbf{t2}} \left( \mathbb{ObjSR}
\right)
\\
\mathbb{ObjSR} \equiv
\underset{\mathrm{rule_{jc,k}}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_move}}{\mathbb{Macro}}{\ \cup\ }
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\end{gathered}
```

```math
\mathbf{Worker}_{\mathtt{AlgStateRule}_{jc,k}} \left(
\underset{\mathrm{rule_{jc,k}}}{\mathbb{Macro}}
\right)
```

```math
\mathbf{Material}_{\mathtt{AlgStateRule}_{jc,k}} \left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}},
\underset{\mathrm{next\_move}}{\mathbb{Macro}},
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
```

- стартовые условия:

```math
\mathscr{Letter}_k\left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}
\right)
```

- результаты определяются программистом машины Тьюринга, который выбирает для каждого $`\mathtt{AlgStateRule}_{jc, k}`$ значения для следующих параметров:

  * $`k\_result = \overline{1,K}`$;
  * $`m\_result = \overline{1,4}`$;
  * $`j\_result = \overline{1,N}`$


```math
\mathscr{Letter}_{k\_result}\left(
\underset{\mathrm{cell}_{ic}}{\mathbb{Macro}}
\right)
```

```math
\mathscr{Move}_{m\_result}\left(
\underset{\mathrm{next\_move}}{\mathbb{Macro}}
\right)
```

```math
\mathscr{State}_{j\_result}\left(
\underset{\mathrm{next\_state}}{\mathbb{Macro}}
\right)
```

При этом используются следующие обозначения для движений:

- $`\mathscr{Move}_1 \equiv \mathscr{MoveNone}`$,
- $`\mathscr{Move}_2 \equiv \mathscr{MoveLeft}`$,
- $`\mathscr{Move}_3 \equiv \mathscr{MoveRight}`$,
- $`\mathscr{Move}_4 \equiv \mathscr{MoveEnd}`$

Итак — описаны все элементы _алгоритмической группы_, представляющей собой реализацию абстрактной машины Тьюринга. Формализация была выполнена в терминах разрабатываемой **модели**. Для текущей поставленной задачи: продемонстрировать способ формального описания способа преобразования, формируемого одним из образцов **алгоритма** — достаточно этого рассмотрения вычислительной машины Тьюринга. Более детальное описание разнородных средств реализации **алгоритмов** (в том числе и формализация нормального алгорифма Маркова) будет представлено в главе [5.2.2. Искусственные исполнители текста](5_2_2_FormalLanguageExecutor.md).

<todo>

Проверить примеры терминов в сносках главы.

Необходим ли пример записи **алгоритмов** формирования программистом машины Тьюринга начального состояния ленты, набора состояний управляющей машины и использование результата полученного в ленте?

Нужен ли $`\underset{\mathrm{mover}}{\mathbb{Macro}}`$?

</todo>