## 8. Благодарности

<todo>

Перечислить труды, послужившие основанием и близкие по теоретическим построениям

</todo>

Основания

- [«Эгоистичный ген» (англ. «The Selfish Gene»). Ричард Докинз](https://www.ozon.ru/context/detail/id/142030241/). [Wikipedia](https://ru.wikipedia.org/wiki/%D0%AD%D0%B3%D0%BE%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%B3%D0%B5%D0%BD)
- [«Отсутствующая структура. Введение в семиологию» (итал. «La struttura assente. Introduzione alla ricerca semiologica»). Умберто Эко](https://www.ozon.ru/context/detail/id/1579594/)
- [«Биология. В 3 томах». Стаут Уилф , Грин Найджел , Тейлор Деннис](https://www.ozon.ru/context/detail/id/144252289/) [@taylor:biology:2019]
- [«Что такое жизнь?» (англ. «What Is Life Mind and Matter»). Шредингер Эрвин](https://www.ozon.ru/context/detail/id/147020883/)
- [«Конструкция мозга». У. Росс Эшби](https://www.ozon.ru/context/detail/id/148990586/)
- [«Самоорганизация в неравновесных системах. От диссипативных структур к упорядоченности через флуктуации». Пригожин Илья Романович , Николис Грегуар](https://www.ozon.ru/context/detail/id/34351132/)
- [«Сумма технологии». Лем Станислав](https://www.ozon.ru/context/detail/id/145320712/)

Влияния

- [«Детерминированный хаос: Введение». Шустер Г.](https://www.ozon.ru/context/detail/id/4889598/)
- [«Искусственный интеллект. Современный подход. Второе издание» (англ. «Artificial Intelligence: A Modern Approach, 2nd Edition»). Норвиг Питер , Рассел Стюарт](https://www.ozon.ru/context/detail/id/149313896/)
- [«Глубокое обучение. Погружение в мир нейронных сетей». Архангельская Екатерина Владиславовна, Кадурин А., Николенко Сергей Игоревич](https://www.ozon.ru/context/detail/id/142987816/)
- [«Приемы объектно-ориентированного проектирования. Паттерны проектирования». Влиссидес Джон , Джонсон Р. , Хелм Ричард , Гамма Эрих](https://www.ozon.ru/context/detail/id/148946316/)
- [«Алгоритмы. Руководство по разработке». Скиена Стивен С.](https://www.ozon.ru/context/detail/id/143246191/)
- [Диссертация «Семантические словари в автоматической обработке текста». А.Сокирко](http://aot.ru/docs/sokirko/sokirko-candid-7.html)
- [«Фольклор и постфольклор: структура, типология, семиотика» А.А. Кретов](http://www.ruthenia.ru/folklore/kretov1.htm)

Близкие

- [«Эволюционная эпистемология» Карл Р. Поппер](http://www.keldysh.ru/pages/mrbur-web/philosophy/popper.html)
- [«Гёдель, Эшер, Бах: эта бесконечная гирлянда» Дуглас Хофштадтер](https://ru.wikipedia.org/wiki/%D0%93%D1%91%D0%B4%D0%B5%D0%BB%D1%8C,_%D0%AD%D1%88%D0%B5%D1%80,_%D0%91%D0%B0%D1%85)
- «Философия информации» Лучано Флориди
   - статья основа ([«Open problems in the philosophy of information» Luciano Floridi](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.127.5906&rank=1));
  - вики ([«Философия информации»](https://ru.wikipedia.org/wiki/%D0%A4%D0%B8%D0%BB%D0%BE%D1%81%D0%BE%D1%84%D0%B8%D1%8F_%D0%B8%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D0%B8));
  - обзор 10 лет спустя ([«Floridi’s “Open Problems in Philosophy of Information”, Ten Years Later» Gordana Dodig Crnkovic, Wolfgang Hofkirchner](https://www.researchgate.net/publication/220121538_Floridi's_Open_Problems_in_Philosophy_of_Information_Ten_Years_Later));
   - русскоязычный обзор: ([«Лучано Флориди: Философия информации» Евгений Рудный](http://blog.rudnyi.ru/ru/2019/08/luciano-floridi-the-philosophy-of-information.html)).
