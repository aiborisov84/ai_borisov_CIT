#### 4.1.1 Эвольвер - развивающийся организм

Новый шаг **развития**, послуживший прогрессу накопления **алгоритмов** _организма_, основывается на существующих способах _медленного синтеза_. При этом прорывом стала _обособление_ у _организма_ новой _функции_, а вернее нового _блока_ — _памяти_. Изменяемое в некоторых пределах внутреннее состояние этого _блока_ предоставило возможность _организму_ **синтезировать** и **сохранять** **алгоритмы** способом внешним по отношению к его _комплементарному_ _самокопированию_. Чтобы подчеркнуть значимость этой возможности, введем для такого типа _организма_ термин — _эвольвер_.

Способом появления _памяти_ в _организме_ стало формирование у него внутренней _функционально_ _обособленной_ части - _блока_, способного на динамическое изменение содержащихся в нём _конструкций_ в зависимости от _процессов_ в которых участвует этот _организм_. Эти динамически изменяющиеся _конструкции_ - ключевое отличие от **структур**, обслуживающих процессы _самокопирования_ _организма_, которые _специализированы_ так, что противодействуют своему изменению. Термин _память_ будем далее использовать для обозначения этого _блока_ динамических _конструкций_ без требования включения его именно в _организм_.

++_Память_++[^4-1--memory] — _локализованный_ в некотором _алгоритмическом_ _блоке_ (например, в _организме_) под-_блок_ с динамически изменяющимися в _процессах_ _блока_ _конструкциями_, представляющими собой основу для формирования **структур** (_путей активации_), которые являются _опорой_ для **синтезируемых** и _исполняемых_ _свя́зных_ **алгоритмов**.

При рассмотрении _памяти_ в _организме_ необходимо отметить _функциональное_ _обособление_ **структур** _памяти_ (_путей активации_) от **структур**, обеспечивающих _самокопирование_ этого _организма_.

В дополнение к формированию _памяти_ для **развития** _организма_ стала необходима _специализация_ и _функциональная_ _обособленность_ его _макро-работников_, _взаимодействующих_ с окружающей _средой_. Эти _макро-работники_ являются _интерфейсными_ **алгоритмами** _организма_, представляющего собой _алгоритмический_ _блок_. _Входной_ _интерфейс_ формируется на основе появления _детекторов_, а _выходной_ _интерфейс_ - на основе _акторов_. С их использованием совместно с _памятью_ _организм_ получил возможность **синтезировать** _полезные_ _свя́зные_ **алгоритмы** _взаимодействия_ со _средой_, то есть своё _поведение_.

++_Поведение_++[^4-1--behaviour] — _свя́зные_ _вспомогательные_ _интерфейсные_ _под-алгоритмы_ _организма_, в которых

* обязательными _опорными_ _макро-объектами_, формирующими _свя́зность_, являются _пути активации_ (**класс** **структур**) в _памяти_ _организма_;
* _работниками_ обязательно являются _акторы_;
* дополнительно _работниками_ могут быть _детекторы_;
* _материалами_, обуславливающими _исполнение_, являются остальные _объекты_ этого _организма_ и _опорные_ _объекты_ **алгоритмов**, внешних по отношению к _организму_ (то есть _объекты_ **алгоритмов** _среды_).

++_Признак_++[^4-1--sign] — _событие_ в _памяти_, сформированное на основе _исполнения_ _детектора_ и _обусловливающее_ **алгоритмы** изменения и использования _памяти_ _организма_ для **синтеза** и _исполнения_ **алгоритмов** _поведения_.

++_Действие_++[^4-1--action] — _событие_ в _памяти_, происходящее в _процессе_ _исполнения_ **алгоритма** _поведения_ и _обусловливающее_ _исполнение_ _актора_.

++_Детектор_++[^4-1--detector] (от 'detect' - обнаруживать) — включенная в _организм_ _функция_ _входного_ _интерфейса_, _обусловленная_ _событием_ _среды_, в результате своего _исполнения_ формирующая в _памяти_ _признак_, _комплементарный_ этому _событию_.

++_Актор_++[^4-1--actor] (от 'act' - действовать) — включенная в _организм_ _функция_ _выходного_ _интерфейса_, _обусловленная_ _действием_ _памяти_, которая выполняет изменения _связей_ и _параметров_ у _объектов_ _среды_.

**Алгоритмы** _поведения_ набором своих _работников_ (_детекторами_ и _акторами_) _специализированы_ для _взаимодействия_ со внешними по отношению к _памяти_ (а чаще с внешними и по отношению к _организму_) _объектами_. Этот момент делает уместным введение для множества таких _объектов_ следующего термина и обозначения.

++_Среда организма_++ ($`\mathbb{Env}_{\mathbb{Org}}`$) — это _среда_ для _алгоритмической системы_, _представленной_ этим _организмом_. В _среде_ имеет место _исполнение_ внешних по отношению к _организму_ **алгоритмов**, включая **алгоритмы** других _организмов_.

Объединяя вышеперечисленное получаем следующее определение.

++_Эвольвер_++ ($`\mathbb{Evol}`$)[^4-1--evolver] (от 'evolve' - развиваться) — это способный к **развитию** (**синтезу**, _исполнению_ и **сохранению** **алгоритмов** _поведения_) _алгоритмический_ _блок_, с поддержкой _представленности_, например, использованием _функций_ _самокопирования_ _организма_, и обладающий следующими характеристиками:

* наличие _памяти_;
* способность изменять состояние _памяти_ под влиянием _детекторов_;
* способность изменять состояние _памяти_ под влиянием внутренних _процессов_ _алгоритмического_ _блока_;
* способность использовать _акторы_, _обусловленные_ _событиями_ _памяти_;
* способность использовать _память_ для **синтеза** **алгоритмов** _поведения_;
* способность использовать _память_ для исполнения **алгоритмов** _поведения_.

У _эвольвера_, благодаря перечисленным характеристикам, появляется возможность оценивать и использовать закономерности (**признак** **Однородность**) окружающей _среды_. Это возможно, если доступен следующий набор _процессов_:

* оценка _полезности_ (_вредности_) текущего состояний и изменений _памяти_ по вероятности осуществления после них _процессов_ _самокопирования_ _организма_,
* _фиксация_ в **структуре** _памяти_ событий _исполнения_ _детекторов_;
* инициализация в _памяти_ _исполнения_ _акторов_ и _фиксация_ в её **структуре** этого события;
* формирование в _памяти_ _пути активации_ (**класс** **структуры**), описывающего _последовательность_ событий _исполнения_ _детекторов_ и _акторов_ (**синтез** **алгоритма**);
* _соревнование_ **алгоритма** _пути активации_ с **сохранением** в _памяти_ при следующих условиях:
   + при повторных обнаружениях с использованием _пути активации_ _зафиксированной_ _последовательности_ событий _исполнения_ _детекторов_ и _акторов_;
   + при получении оценки _полезности_ (_вредности_) _исполнения_ существующего _пути активации_;
* _исполнение_ **алгоритма** с _опорой_ на _путь активации_, обеспечивающее _последовательность_ инициализации _исполнения_ _акторов_, _зафиксированных_ в _пути активации_, при _исполнении_ всех предшествующих им _детекторов_ и _акторов_ в _последовательной цепи_, что реализует со стороны _организма_ обусловленный повтор (или блокировку при _вредной_ оценке _пути активации_) _исполнения_ всех _акторов_ в _пути активации_.

Теперь сформируем определения новых терминов, которые были использованы в описании перечисленных выше _процессов_.

++==_Узел_== _области памяти_++ — _специализированный_ _объект_ _памяти_, используемый в качестве _элемента_ для **синтеза** _пути активации_ и выявляющий _Повторимость_ своего _исполнения_ (_активации_) сопряжённо с _близкими_ _узлами_.

++==_Активация_== _узла_ _памяти_++ - _процесс_, сопровождающий  задействование этого _узла_ в _порядке_ _исполнения_ _пути активации_ и при исполнении _свя́зного_ **алгоритма**, объединяющего этот _узел_ с некоторым _актором_ или _детектором_.

++_Область памяти_++ — это _локализованная_ совокупность _узлов_, обеспечивающая возможность **синтеза** и _соревнований_ _пути активации_, объединяющего _узлы_, _активация_ которых произошла _последовательно_ и близко по времени, путем создания (или укрепления/ослабления имеющихся) _структурных_ _связей_ между ними.

++_Фиксация узла_++ - это _процесс_ формирования _комплементарного_ **алгоритма**, _опирающегося_ на _блок_ _детектора_ (или _актора_) и на некоторый _узел_ _памяти_. Формируемая _комплементарная связь_ между _детектором_ и _узлом_ обеспечивает _исполнение_ **синтезированного** **алгоритма** таким образом, что все последующие _исполнения_ _детектора_ приводят к _активации_ этого _узла_. И для _комплементарной связи_ между _узлом_ и _актором_ **синтезированный** **алгоритм** обеспечивает обратный _порядок_: _активация_ _узла_ приводит к _исполнению_ _актора_. _Детектор_ (или _актор_), для которого произведена _фиксация узла_, будем называть ++_зафиксированным_++ этим _узлом_.

++_Путь активации_++ ($`\mathbb{Path}`$) — это **класс** _последовательной цепи_, _элементами_ которой являются _узлы_ _памяти_ _эвольвера_. _Путь активации_ является _опорой_ **алгоритма** _поведения_ и **синтезируется** формированием в виде _последовательной цепи_ _структурных_ _связей_ между _узлами_, _зафиксировавшими_ _исполнение_ _эвольвером_ его _детекторов_ и _акторов_. При этом _порядок_ **синтезируемой** _последовательной цепи_ обеспечивает _последовательность_ _активации_ её _узлов_, которая тождественна исходной _последовательности_ _активации_ _зафиксированных_ _детекторов_ и _акторов_. Процесс формирования _пути активации_ для некоторой _последовательности_ _исполнения_ _детекторов_ и _акторов_ будем называть ++_фиксацией_++ _пути активации_.

_Путь активации_ **сохраняется** на основе:

- закрепления при осуществлении сопровождений повторных _исполнений_ этой _последовательности_ путём инициализации(блокировки) _исполнения_ зафиксированных _акторов_, _обусловленного_ предваряющими _исполнениями_ зафиксированных _детекторов_, в зависимости от получаемой _полезности_(_вредности_) _организму_ соответствующего **алгоритма** _поведения_;
- отбора наиболее _полезных_ среди всех **алгоритмов** _поведения_, _опирающихся_ на _пути активации_, при проведении _соревнований_ между _эвольверами_;
- расформирования редко используемых и не оцененных в _полезности_.

Процедура _фиксации_ _пути активации_ изменяет внутреннее состояние _памяти_ _эвольвера_ так, что в случае обнаружения сочетания _признаков_ (_последовательности_ _исполнения_ _детекторов_), тождественного ранее _зафиксированному_, есть возможность выбрать и выполнить самую _полезную_ из _зафиксированных_ _последовательностей_ _действий_ (то есть запустить _упорядоченное_ _исполнение_ _акторов_) или избежать выполнения _зафиксированной_ _вредной_ _последовательности_ _действий_.

Выбор последовательности _действий_ (_макро-действия_) для детектированного _макро-признака_ является **синтезом** _вспомогательного_ **алгоритма**. И механизмы **сохранения** этого _вспомогательного_ **алгоритма** в конечном счёте сводятся к _соревнованию_ _организмов_, то есть к оценке вероятности _самокопирования_ _организма_, использующего этот _вспомогательный_ **алгоритм**. На основе этого общего способа оценки _полезности_ _вспомогательного_ **алгоритма** есть возможность сформировать промежуточные критерии, которые опосредовано характеризуют указанную вероятность _самокопирования_, проистекая из **признаков** **алгоритмического** **пространства** и свойств **алгоритма**. При этом среди **признаков** для оценки _поведения_ наибольшей значимостью обладает _Повторимость_ **алгоритма**.

Здесь стоит отметить существенное отличие **структуры** _пути активации_ от **структур**, обслуживающих _медленный синтез_.

**Структуры** _медленного синтеза_ _специализированы_ так, чтобы минимизировать свои изменения во времени и тем самым обеспечить _самокопирование_ _организма_, поэтому их _элементами_ являются неизменные _работники_. Для **структуры** _пути активации_ нет необходимости поддерживать своё постоянство, и её _элементы_ могут иметь изменяемые _параметры_. Таким _параметром_ _элемента_ является его _активация_ после контакта с _комплементарным_ _признаком_ или _действием_. _Параметрами_ _элементов_ _пути активации_ _контролируется_ _процесс_ его участия в _исполнении_. Также от _параметров_ _элементов_ зависят _процессы_ _соревнования_ их _пути активации_, а значит и **сохранение** _опирающегося_ на него **алгоритма** _поведения_.

Второй характерной _специализацией_ **структуры** _медленного синтеза_ является её реализация в виде _цепи_, что вызвано необходимостью высокой вероятности _самокопирования_, обеспечиваемой, например, _комплементарной_ _трансляцией_. Линейность _цепи_ упрощает _самокопирование_, но при этом сильно ограничивает сложность _свя́зного_ **алгоритма**, _опирающегося_ на эту **структуру**. И в силу отсутствия _копирования_ _путей активации_ при _самокопировании_ _эвольвера_ подобного ограничения для _пути активации_ нет, а значит её **структура** может быть сложнее устроена. И как будет показано далее, действительно, _полезны_ _пути активации_, получаемые объединением двух деревьев у корня. При этом первое дерево основывается на _иерархии_, фиксирующей _последовательность_ появления _признаков_ (_макро-признак_), а второе дерево описывает _иерархию_, определяющую _исполнение_ _последовательности_ _действий_ (_макро-действие_). Объединение этих деревьев порождает **алгоритм** _поведения_, описываемый формулой {_макро-признак_ ⇒ _макро-действие_}. В общем же случае получается следующая запись _свя́зного_ **алгоритма** _поведения_.

```math
\begin{aligned}
& \mathtt{AlgPath} \left( \mathbb{Env}{\ \cup\ }\mathbb{Evol}, 
\mathbf{t_1} \ldots \mathbf{t_n}
\right)
\\
& \begin{cases}
& path \equiv [sign1, sign2, \ldots, act1, act2] 
\\
& path \subset \mathbb{Evol} 
\\
& \mathtt{AlgSign1} \left( \mathbb{Env} {\ \cup\ } sign1, \mathbf{t_1} \ldots \mathbf{t_2} \right)
\\
& \mathtt{AlgSign2} \left( \mathbb{Env} {\ \cup\ } sign2, \mathbf{t_2} \dots \mathbf{t_3} \right)
\\
& \ldots
\\
& \mathtt{AlgAct1} \left( act1 {\ \cup\ } \mathbb{Env}, \mathbf{t_{n-2}} \ldots \mathbf{t_{n-1}} \right) 
\lbrace \mathbf{Id}_{\mathtt{AlgAct1}}( act1 ) \rbrace
\\
& \mathtt{AlgAct2} \left( act2 {\ \cup\ } \mathbb{Env}, \mathbf{t_{n-1}} \ldots \mathbf{t_n} \right)
\lbrace \mathbf{Id}_{\mathtt{AlgAct2}}( act2 ) \rbrace
\end{cases}
\end{aligned}
```

Для такого _упорядоченного_ _исполнения_ введём дополнительное обозначение.

```math
\begin{aligned}
& \mathtt{AlgPath} \left( \mathbb{Env}{\ \cup\ }\mathbb{Evol}, 
\mathbf{t_1} \ldots \mathbf{t_n}
\right)
\\
& \left[
\begin{aligned}
\quad
& \mathtt{AlgSign1} \left( \mathbb{Env} {\ \cup\ } sign1, \mathbf{t_1} \right)
\\
& \mathtt{AlgSign2} \left( \mathbb{Env} {\ \cup\ } sign2, \mathbf{t_2} \right)
\\
& \dots
\\
& \mathtt{AlgAct1} \left( act1 {\ \cup\ } \mathbb{Env}, \mathbf{t_{n-2}} \right)
\\
& \mathtt{AlgAct2} \left( act2 {\ \cup\ } \mathbb{Env}, \mathbf{t_{n-1}} \right)
\end{aligned}
\right.
\end{aligned}
```

С целью упростить описание, сделав его ближе к естественному языку, для **алгоритма**, опирающегося на _путь активации_, введем следующий способ записи:

```cpp
Alg(«Описание алгоритма»)
[
    Sign(«описание признака 1»)
    Sign(«описание признака 2»)
    ...
    Act(«описание действия 1»)
    Act(«описание действия 2»)
]
```

И аналогичная запись в терминах на русском языке:

```cpp
Алгоритм(«Описание алгоритма»)
[
    признак(«описание признака 1»)
    признак(«описание признака 1»)
    ...
    действие(«описание действия 1»)
    действие(«описание действия 2»)
]
```

Здесь было приведено достаточно общих характеристик _пути активации_ без указания возможных способов его реализации. Одним из способов реализации является нервная система биологических организмов. Основные черты такого способа будут разобраны в следующей главе. Там же будет проведен анализ возможности и условий получения _организмом_ _пользы_ от использования **алгоритмов** _поведения_ с универсальной формулой {_макро-признак_ ⇒ _макро-действие_}.

![Обозначения пути активации](../Notes/20210212/20210212_0002.jpg)

![Разработка обозначений](../Notes/20210212/20210212_0001.jpg)

<todo>

* Наполнить и проверить примеры
* Описание _свя́зного_ **алгоритма** на основе нового **класса** **структуры** (в _памяти_ _организма_) по сравнению со **структурами** _эпохи генов_

</todo>


[^4-1--memory]:
    _Память_:

    * нервная система живых организмов;
    * память компьютера при записи макроса в офисной программе редактирования документов;
    * ?? текстовый редактор среды разработки программного обеспечения.

[^4-1--behaviour]:
    _Поведение_:

    * поведение особи биологического вида;
    * работа программы компьютера.

[^4-1--sign]:
    _Признак_:

    * зеленый свет светофора для алгоритмов дорожного движения;
    * уровень освещенности для алгоритмов открытия-закрытия соцветий растений, опыляемых насекомыми;
    * нулевой указатель ссылки на следующий элемент в алгоритме прохода односвязного списка;
    * интенсивное образование крупных пузырей пара в кастрюле в алгоритме приготовления супа.

[^4-1--action]:
    _Действие_:

    * остановка и начало движения автомобиля в алгоритмах дорожного движения;
    * управляемый изгиб лепестка в алгоритме открытия-закрытия цветка растения;
    * замена указателя на текущий элемент значением указателя на следующий элемент в алгоритме обхода односвязного списка;
    * опускание ингредиентов в закипевшую воду в алгоритмах приготовления супа.
    
[^4-1--detector]:
    _Детектор_:
    
    * счетчики соприкосновения с внешними простыми частицами - для фиксации колебаний, температуры, давления среды,
    * фоторецепторы для фиксации света,
    * химические рецепторы для поиска (или избегания) окружающих значимых(опасных) _объектов_
    * глаз, ухо, вкусовой рецептор, телескоп, сейсмограф, термометр для человека,
    * клавиатура для ЭВМ.

[^4-1--actor]:
    _Актор_ (==поменять примеры==):
    
    * вещества меняющие пространственную форму при _исполнении_ (мышечное волокно), для алгоритмов перемещения и воздействия на окружающее пространство;
    * ноги у животных, жгутики у инфузории-туфельки для алгоритмов поведения: нападать, убегать, передвигаться в поиске еды;
    * зубы у животных в алгоритмах охоты, измельчения пищи, формирования звуков;
    * лопата, оружие, самолет в алгоритмах жизни человека;
    * внешний диск, экран, принтер у ЭВМ в алгоритмах вывода рассчитанных данных.

[^4-1--evolver]:
    _Эвольвер_:
    
    * ?? нейрон - в зависимости от наличия вызываемой входами внутренней активности может вызвать действие или не вызывать,
    * ?? живая клетка - от наличия пищи в окружающей среде запасать энергию в полисахаридах в ситуации избытка энергии, расходовать запасы в режиме недостатка, осуществить деление на определенном этапе клеточного цикла
    * слой нейронов - в зависимости от внутреннего состояния, изменяемого входами, может вызвать любой из выходных действий или не вызывать;
    * человек - может от внешней ситуации выбрать _действие_: петь, есть, гладить, отдыхать, общаться, рассматривать
    * ЭВМ - от запроса пользователя может вести расчет, играть музыку, простаивать