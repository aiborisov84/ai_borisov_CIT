# Общая теория алгоритмов

Лицензия:

```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```

Полный текст лицензии ([LICENSE](/LICENSE.txt))

Авторы: 

- Борисов Алексей Игоревич ([aiborisov84@gmail.com](mailto:aiborisov84@gmail.com))
([публикации habr.com](https://habr.com/ru/users/ai_borisov/))

Страница проекта: ([GitLab ai_borisov_cit](https://gitlab.com/aiborisov84/ai_borisov_CIT))

Обратная связь: ([GitLab Issues](https://gitlab.com/aiborisov84/ai_borisov_CIT/issues))

[TOC]