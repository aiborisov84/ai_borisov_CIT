import bookprocess

import datetime
import os
import os.path as op
import argparse


# ----------------------------------------------------------------------------
def theory_to_pdf(i_args):
    c_book_path = op.normpath(op.join(op.dirname(__file__), '..'))
    c_src_path = op.join(c_book_path, "BookTheory")
    if i_args.dst_path:
        c_dst_path = i_args.dst_path
    else:
        c_dst_path = op.normpath(op.join(c_book_path, '..', 'out', '.export'))

    os.makedirs(c_dst_path, exist_ok=True)

    bookprocess.theory_convert_pdf.produce(
        c_src_path,
        c_dst_path,
        datetime.datetime.now().strftime('%Y%m%d'),
        i_args
    )


# ----------------------------------------------------------------------------
def pdf_to_google(i_args):
    bookprocess.book_to_googledrive.main()


# ----------------------------------------------------------------------------
def theory_to_wiki(i_args):
    c_book_path = op.normpath(op.join(op.dirname(__file__), '..'))
    c_src_path = op.join(c_book_path, "BookTheory")
    c_dst_path = op.normpath(op.join(c_book_path, '..', 'out', 'wiki', 'BookTheory'))
    os.makedirs(c_dst_path, exist_ok=True)
    bookprocess.theory_convert_wiki.produce(
        c_src_path,
        c_dst_path,
        i_args
    )


# ----------------------------------------------------------------------------
def theory_spelling(i_args):
    c_book_path = op.normpath(op.join(op.dirname(__file__), '..'))
    c_src_path = op.join(c_book_path, "BookTheory")
    c_dict_file1 = op.normpath(op.join(c_book_path, '..', '.idea', 'dictionaries', '.xml'))
    c_dict_file2 = op.normpath(op.join(c_book_path, '..', '.epsilon', 'dict.xml'))
    c_dst_file = op.normpath(op.join(c_book_path, '..', 'out', 'spelling.txt'))

    bookprocess.theory_spelling.perform(
        c_src_path,
        [c_dict_file1, c_dict_file2],
        c_dst_file,
        i_args
    )


# ----------------------------------------------------------------------------
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compile book into .export\\<date>.html')
    subparsers = parser.add_subparsers(help='sub-command help', dest='cmd')
    subparsers.required = True

    parser_theory_to_html = subparsers.add_parser('theory_to_pdf',
                                                  help='convert BookTheory to out/.export/Book<date>.pdf '
                                                       'via Html or LaTeX')
    parser_theory_to_html.add_argument('--override', dest='is_override', action='store_true',
                                       help='flag for override current date file')
    parser_theory_to_html.add_argument('--local-script', dest='local_script', action='store_true',
                                       help='flag for local script using')
    parser_theory_to_html.add_argument('--full-md', dest='is_full_md', action='store_true',
                                       help='flag for save intermediate md file')
    parser_theory_to_html.add_argument('--prepare-latex', dest='is_prepare_latex', action='store_true',
                                       help='flag for save latex md file')
    parser_theory_to_html.add_argument('--filter', dest='filter', action='store',
                                       default='', type=str,
                                       help='re filter to chapters list')
    parser_theory_to_html.add_argument('--dst-path', dest='dst_path', action='store',
                                       default='', type=str,
                                       help='dst directory [default out/.export]')
    parser_theory_to_html.set_defaults(func=theory_to_pdf)

    parser_theory_to_wiki = subparsers.add_parser('theory_to_wiki', help='convert BookTheory to wiki/BookTheory')
    parser_theory_to_wiki.set_defaults(func=theory_to_wiki)

    parser_theory_to_wiki = subparsers.add_parser('pdf_to_google', help='convert BookTheory.pdf to GoogleDisk')
    parser_theory_to_wiki.set_defaults(func=pdf_to_google)

    parser_theory_spelling = subparsers.add_parser('theory_spelling', help='convert BookTheory to wiki/BookTheory')
    parser_theory_spelling.add_argument('--filter', dest='filter', action='store',
                                        default='', type=str,
                                        help='re filter to file list')
    parser_theory_spelling.set_defaults(func=theory_spelling)

    args = parser.parse_args()
    args.func(args)
