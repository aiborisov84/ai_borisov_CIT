#!/bin/bash

echo "gat begin"

OUT_DIR=/usr/src/data/out/.export_docker
WORK_DIR=/usr/src/data/book/\!scripts
mkdir -p $OUT_DIR
cd $OUT_DIR
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r $WORK_DIR/required_package.txt
cd $WORK_DIR
python3 Book.py theory_to_pdf --override --prepare-latex --dst-path "$OUT_DIR"
cd $OUT_DIR
file_date=$(date '+%Y%m%d')
lualatex --shell-escape $file_date.tex
biber $file_date.bcf
lualatex --shell-escape $file_date.tex
lualatex --shell-escape $file_date.tex

echo "gat end"

#/usr/src/data
#/usr/src/app
