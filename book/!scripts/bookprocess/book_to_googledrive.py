import json
import pickle
import os.path
from googleapiclient.discovery import build
from google.oauth2 import service_account
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload

import glob

SCOPES = ['https://www.googleapis.com/auth/drive']

c_dir = os.path.dirname(os.path.abspath(__file__))
service_account_info = os.environ.get('GCP_SERVICE_ACCOUNT_KEY', None)
if service_account_info:
    service_account_info = json.loads(service_account_info)
else:
    credentials_json = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS', None)
    if not credentials_json:
        credentials_json = os.path.join(c_dir, 'gat-book-ac7621036aa6.json')
    if os.path.exists(credentials_json):
        service_account_info = json.load(open(credentials_json))

credentials_pickle = os.path.join(c_dir, 'token.pickle')


def get_creds():
    creds = None
    if not service_account_info:
        return
        
    # Obtain OAuth token / user authorization.
    if os.path.exists(credentials_pickle):
        with open(credentials_pickle, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            creds = service_account.Credentials.from_service_account_info(
                service_account_info, scopes=SCOPES)
            # creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(credentials_pickle, 'wb') as token:
            pickle.dump(creds, token)
    return creds


def main():
    c_pdf_mas = glob.glob('*.pdf')
    if len(c_pdf_mas) != 1:
        print('!!!!!!!!! No pdf single file')

    c_upload_src_file = c_pdf_mas[0]
    c_upload_file = c_upload_src_file.removesuffix('.pdf')

    creds = get_creds()
    if not creds:
        print('No google credentials')
        return

    # Build the drive service.
    drive_service = build('drive', 'v3', credentials=creds)

    print("==== to google drive")
    results = drive_service.files().list(
        pageSize=10,
        fields="nextPageToken, files(id, name, mimeType, parents)"
    ).execute()

    next_page_token = results.get('nextPageToken')
    while next_page_token:
        next_page = drive_service.files().list(
            pageSize=10,
            fields="nextPageToken, files(id, name, mimeType, parents)",
            pageToken=next_page_token
        ).execute()
        next_page_token = next_page.get('nextPageToken')
        results['files'] = results['files'] + next_page['files']

    print('File item count: ', len(results.get('files')))

    shared_folder_id = None
    file_names = {}
    for it in results['files']:
        if 'parents' not in it:
            shared_folder_id = it['id']
        else:
            file_names[it['name']] = it['id']
            # print(it)

    file_name = c_upload_file + '{}.pdf'
    file_sfx = ''

    del_id_mas = []
    sfx_idx = 1
    v_test_file_name = file_name.format(file_sfx)
    while v_test_file_name in file_names:
        del_id_mas.append(file_names[v_test_file_name])
        file_sfx = '_' + str(sfx_idx)
        v_test_file_name = file_name.format(file_sfx)
        sfx_idx += 1

    file_name = v_test_file_name

    cmd = 'create'
    match cmd:
        case 'info':
            print('Root folder id: ', shared_folder_id)
        case 'del':
            for it in del_id_mas:
                drive_service.files().delete(fileId=it).execute()
                print('Delete: ' + it)
        case 'create':
            file_metadata = {
                'name': file_name,
                'parents': [shared_folder_id]
            }

            media = MediaFileUpload(c_upload_src_file, mimetype='application/pdf')
            f = drive_service.files().create(
                body=file_metadata,
                media_body=media,
                supportsAllDrives=True
            ).execute()

            print("Created file '{}' id: {}.".format(f.get('name'), f.get('id')))


if __name__ == '__main__':
    main()
