import typing
import os
import re

CONST_yaml_RE = re.compile(r'^---\r.*\r\n\.\.\.\r\n', flags=re.DOTALL)

CONST_FileName_RE = re.compile(r'^([_\d]+)_(.+)$')

CONST_Header_RE = re.compile(r'^(#+?) (([.\d]*) ?(.+))$')

CONST_Notes_RE = re.compile(r'!?(\[[^]]*]\()\.\./Notes/([^])]*\))')
CONST_Notes_RE_Replace = r'\1https://gitlab.com/aiborisov84/ai_borisov_CIT/-/blob/master/book/Notes/\2'

CONST_TodoStart_RE = re.compile(r'^<todo>.?$', flags=re.MULTILINE)
CONST_TodoEnd_RE = re.compile(r'^</todo>.?$', flags=re.MULTILINE)
CONST_ImportantStart_RE = re.compile(r'^<important>.?$', flags=re.MULTILINE)
CONST_ImportantEnd_RE = re.compile(r'^</important>.?$', flags=re.MULTILINE)

CONST_Definition_RE = re.compile(r'\+\+(.*?)\+\+')
CONST_Index_RE = re.compile(r'^\[IDX]$', flags=re.MULTILINE)


# ----------------------------------------------------------------------------
def scan_src_file_list(i_src_path) -> typing.List[str]:
    v_file_list = list()
    with os.scandir(i_src_path) as it:
        for entry in it:
            if entry.name.startswith('.'):
                continue
            if not entry.is_file():
                continue

            v_file_list.append(entry.name)

    v_file_list.sort()
    return v_file_list


# ----------------------------------------------------------------------------
def parse_file_name(i_filename):
    c_match = CONST_FileName_RE.match(i_filename)
    if not c_match:
        raise ValueError(f"Bad file name format: {i_filename}")

    c_index_str = c_match.group(1)
    c_idx_list = list(map(int, filter(len, c_index_str.split('_'))))
    c_file_title = c_match.group(2)

    return c_file_title, c_idx_list


# ----------------------------------------------------------------------------
def definition_term_to_sort(i_orig_term: str):
    o_res = i_orig_term.replace('_', '')
    o_res = o_res.replace('*', '')
    return o_res.lower()


# ----------------------------------------------------------------------------
def definition_term_to_link(i_orig_term: str):
    v_res = []
    v_is_first = True
    for it in i_orig_term:
        if it.isalpha():
            if v_is_first:
                v_res.append(it.upper())
                v_is_first = False
                continue
        v_res.append(it)

    o_res = ''.join(v_res)

    return o_res


# ----------------------------------------------------------------------------
class DefinitionRE_Item:
    def __init__(self):
        self.item_def_mas = []
        self.last_header_caption = ''

    def __call__(self, i_match):
        o_result = i_match.group(0)
        if i_match.group(1) != 'Подчеркивание':
            self.item_def_mas.append(
                (i_match.group(1), self.last_header_caption)
            )
            o_result = o_result + f'<a name = "index_{len(self.item_def_mas)}"></a>'
        return o_result

    def set_header_caption(self, i_header_caption):
        self.last_header_caption = i_header_caption

    def clear(self):
        self.item_def_mas.clear()


# ----------------------------------------------------------------------------
class DefinitionSortIndex:
    def __init__(self):
        self.index_map = []

    def append(self, i_def_list, i_link_constructor=None):
        for it_idx_def, it_def in enumerate(i_def_list):
            v_link_ref = f'index_{it_idx_def + 1}'
            c_def_caption = definition_term_to_link(it_def[0])
            # print(c_def_caption)
            if i_link_constructor:
                v_caption = '- {} ({})'.format(
                    i_link_constructor.get_link(
                        c_def_caption,
                        v_link_ref
                    ),
                    it_def[1] if it_def[1] else i_link_constructor.str
                )
            else:
                v_caption = '- [{}](#{}) ({})'.format(
                    c_def_caption,
                    v_link_ref,
                    it_def[1]
                )

            self.index_map.append(
                (
                    definition_term_to_sort(it_def[0]),
                    v_caption
                )
            )

    def get_index_list(self):
        v_index_map = sorted(self.index_map)
        return [it[1] for it in v_index_map]


# ----------------------------------------------------------------------------
class DefinitionRE_GlobalIndex:
    def __init__(self):
        self.items = DefinitionRE_Item()

    def __call__(self, i_match):
        v_def_index = DefinitionSortIndex()
        v_def_index.append(self.items.item_def_mas)
        return '\n'.join(v_def_index.get_index_list())


# ----------------------------------------------------------------------------
class FileHeader:
    prev_header: typing.Optional['FileHeader']
    next_header: typing.Optional['FileHeader']

    def __init__(
            self,
            i_header_str: str,
            i_prev_header: typing.Optional['FileHeader'],
            i_src_file_title: str
    ):
        self.src_file_title = i_src_file_title
        self.prev_header = i_prev_header
        self.next_header = None
        self.is_content = False
        self.is_file_first_header = False

        v_match = CONST_Header_RE.match(i_header_str)
        if not v_match:
            raise ValueError(f"Bad header format: {i_header_str}")

        self.str = v_match.group(2)

        v_level_str = v_match.group(1)
        v_level = len(v_level_str) - 1
        v_index_str = v_match.group(3)
        v_idx_list = list(map(int, filter(len, v_index_str.split('.'))))

        self.auto_number_header = (not v_idx_list and v_level == 4)
        v_correct_header = (len(v_idx_list) == v_level) or self.auto_number_header
        if not v_correct_header:
            raise ValueError(f"Wrong header '{v_level_str}' level with idx:'{v_index_str}' str: {i_header_str}")

        if i_prev_header is not None:
            i_prev_header.next_header = self

            if self.auto_number_header:
                if i_prev_header.idx_list:
                    v_idx_list = i_prev_header.idx_list.copy()
                    if i_prev_header.auto_number_header:
                        v_idx_list[-1] += 1
                    else:
                        i_prev_header.idx_list.append(0)
                        v_idx_list.append(1)

                    self.str = '.'.join(map(str, v_idx_list)) + ' ' + self.str
            else:
                if len(i_prev_header.idx_list) + 1 == len(v_idx_list):
                    if i_prev_header.idx_list == v_idx_list[:-1] and v_idx_list[-1] == 1:
                        i_prev_header.idx_list.append(0)

        self.idx_list = v_idx_list
        self.name = v_match.group(4)

    def set_content(self):
        self.is_content = True

    def set_file_first_header(self):
        self.is_file_first_header = True

    def __repr__(self):
        return ', '.join(map(repr, [self.idx_list, self.str]))

    def do_correction(self, i_file_idx_list):
        if len(self.idx_list) == len(i_file_idx_list):
            return self.idx_list == i_file_idx_list

        if len(self.idx_list) + 1 == len(i_file_idx_list):
            if i_file_idx_list[-1] == 0 and self.idx_list == i_file_idx_list[:-1]:
                self.idx_list = i_file_idx_list
                return True

        return False

    def get_file_name(self):
        return '_'.join(map(str, self.idx_list)) + '_' + self.src_file_title

    def is_main(self):
        return self.idx_list == [0]

    def get_link(self, i_caption=None, i_tag=None):
        if self.is_content:
            v_link = os.path.splitext(self.get_file_name())[0]
        else:
            if not self.next_header:
                raise AssertionError("No content header must be followed")

            v_link = os.path.splitext(self.next_header.get_file_name())[0]

        c_caption = i_caption if i_caption else self.str
        c_tag = '#' + i_tag if i_tag else ''
        return f'[{c_caption}](./{v_link}{c_tag})'

    def find_prev_content_header(self):
        v_c_header = self.prev_header
        while v_c_header:
            if v_c_header.is_content:
                return v_c_header

            v_c_header = v_c_header.prev_header

        return None

    def find_next_content_header(self):
        v_c_header = self.next_header
        while v_c_header:
            if v_c_header.is_content:
                return v_c_header

            v_c_header = v_c_header.next_header

        return None

    def get_navigation_links(self, i_main_file_link: typing.Optional[str], i_index_file_link: typing.Optional[str]):
        if self.is_main():
            return None

        c_prev_content_header = self.find_prev_content_header()
        c_next_content_header = self.find_next_content_header()

        if not c_prev_content_header and not c_next_content_header and not i_main_file_link:
            return None

        o_res = i_main_file_link if i_main_file_link else ''

        if i_index_file_link:
            o_res = o_res + ' | ' + i_index_file_link

        o_res = '{ ' + o_res + ' }'

        if c_prev_content_header:
            o_res = '<< ' + c_prev_content_header.get_link() + '\n\n' + o_res

        if c_next_content_header:
            o_res = o_res + '\n\n' + c_next_content_header.get_link() + ' >>'

        return o_res

    def get_content_link(self):
        if self.is_main():
            return None

        v_header_level = '####' if self.is_file_first_header else '#####'

        return v_header_level + ' ' + self.get_link()
