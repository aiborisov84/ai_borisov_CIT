from markdown.extensions import Extension
from markdown.inlinepatterns import SimpleTagInlineProcessor, InlineProcessor
from markdown.preprocessors import Preprocessor
from markdown.postprocessors import Postprocessor

import re
import typing
import xml.etree.ElementTree

UNDERLINE_RE = r"(\+\+)(((?!\+\+).)*)(\+\+)"
MARK_RE = r"(==)(((?!==).)*)(==)"
SMILE_RE = r"((?<=\W)|(?<=^))({})(?=\W|$)".format('|'.join(map(re.escape, r':-) :smile:'.split())))

INLINE_MARKER = r'<span id="katex', r'"></span>'
INLINE_MARKER_RE = re.compile(re.escape(INLINE_MARKER[0]))
SEPARATE_MARKER = '<div id="katex', r'"></div>'
SEPARATE_MARKER_RE = re.compile(re.escape(SEPARATE_MARKER[0]))
INLINE_MATH = r'<span class="katex">{}</span>'
SEPARATE_MATH = '<div class="katex">{}</div>'


def make_marker(i_marker_id: int, i_is_inline: bool) -> str:
    if i_is_inline:
        return INLINE_MARKER[0]+str(i_marker_id)+INLINE_MARKER[1]
    else:
        return SEPARATE_MARKER[0] + str(i_marker_id) + SEPARATE_MARKER[1]


def make_math(i_math: str, i_is_inline: bool) -> str:
    if i_is_inline:
        return INLINE_MATH.format(i_math)
    else:
        return SEPARATE_MATH.format(i_math)


# ----------------------------------------------------------------------------
class ReplaceInlineProcessor(InlineProcessor):
    def __init__(self, pattern, i_replace_str, i_class):
        InlineProcessor.__init__(self, pattern)

        self.replace_str = i_replace_str
        self.node_class = i_class

    def handleMatch(self, m, data):
        # el.text = m.group(2)

        v_el = xml.etree.ElementTree.Element('span')
        v_el.text = self.replace_str
        v_el.set('class', self.node_class)

        return v_el, m.start(0), m.end(0)


# ----------------------------------------------------------------------------
class BookExtension(Extension):
    def __init__(self, **kwargs) -> None:
        self.math_map: typing.Dict[int, typing.Tuple[bool, str]] = {}
        self.unique_id: int = 0
        super(BookExtension, self).__init__(**kwargs)

    def reset(self) -> None:
        self.math_map.clear()
        self.unique_id = 0

    def get_new_id(self) -> int:
        c_value = self.unique_id
        self.unique_id += 1
        return c_value

    def extendMarkdown(self, md, *args, **kwargs) -> None:
        md.inlinePatterns.register(SimpleTagInlineProcessor(UNDERLINE_RE, "u"), "book_underline", 40)
        md.inlinePatterns.register(SimpleTagInlineProcessor(MARK_RE, "mark"), "book_mark", 40)
        md.inlinePatterns.register(ReplaceInlineProcessor(SMILE_RE, '😊', 'emoji'), 'book_emoji', 40)

        md.preprocessors.register(BookPreprocessor(md, self), name='book_math_code_block', priority=50)
        md.postprocessors.register(BookPostprocessor(md, self), name='book_math_code_block', priority=0)

        md.registerExtension(self)


MATH_START_BLOCK_RE = re.compile(r"^(```|~~~)math")
MATH_INLINE_BLOCK_RE = re.compile(r"\$`(.*?)`\$")


# ----------------------------------------------------------------------------
class BookPreprocessor(Preprocessor):
    def __init__(self, md, ext: BookExtension) -> None:
        super(BookPreprocessor, self).__init__(md)
        self.ext: BookExtension = ext

    def _line_math(self, i_line):
        c_match = MATH_INLINE_BLOCK_RE.search(i_line)
        while c_match:
            math_html = c_match.group(1)
            marker_id = self.ext.get_new_id()
            v_is_inline = True
            marker = make_marker(marker_id, v_is_inline)
            i_line = i_line[:c_match.start()] + marker + i_line[c_match.end():]
            self.ext.math_map[marker_id] = (v_is_inline, math_html)

            c_match = MATH_INLINE_BLOCK_RE.search(i_line)

        return i_line

    def run(self, lines: typing.List[str]) -> typing.List[str]:
        block_lines: typing.List[str] = []
        out_lines: typing.List[str] = []

        expected_close_fence = ''

        for line in lines:
            if not expected_close_fence:
                start_match = MATH_START_BLOCK_RE.match(line)
                if start_match:
                    expected_close_fence = start_match.group(1)
                else:
                    c_res_line = self._line_math(line)
                    out_lines.append(c_res_line)

                continue

            if line.strip() != expected_close_fence:
                block_lines.append(line)
                continue

            expected_close_fence = ''
            block_text = "\n".join(block_lines).rstrip()
            block_lines.clear()

            math_html = block_text
            marker_id = self.ext.get_new_id()
            v_is_inline = False
            self.ext.math_map[marker_id] = (v_is_inline, math_html)

            marker = make_marker(marker_id, v_is_inline)
            out_lines.append(marker)

        return out_lines


# ----------------------------------------------------------------------------
class BookPostprocessor(Postprocessor):
    def __init__(self, md, ext: BookExtension) -> None:
        super(BookPostprocessor, self).__init__(md)
        self.ext: BookExtension = ext

    def run(self, text: str) -> str:
        if not INLINE_MARKER_RE.search(text) and not SEPARATE_MARKER_RE.search(text):
            return text

        for it_marker_id, it_html in self.ext.math_map.items():
            c_is_inline = it_html[0]
            c_marker = make_marker(it_marker_id, c_is_inline)
            text = text.replace(c_marker, make_math(it_html[1], c_is_inline))

        return text

# ----------------------------------------------------------------------------
