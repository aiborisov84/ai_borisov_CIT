from . import theory

import pymorphy3

import os.path as op
import codecs
import re
import pprint
import xml.etree.ElementTree as ET


# ----------------------------------------------------------------------------
def split_to_words(i_str):
    return list(filter(
        lambda word: word and word.isalpha(),
        # re.split(r'\W+', i_str)
        re.split(r'[^а-яА-Яя́óёëË]+', i_str)
    ))
            

# ----------------------------------------------------------------------------
def parse_file(i_path, i_filename, i_morphy, c_self_dict, i_file_out):
    with codecs.open(op.join(i_path, i_filename), mode="r", encoding="utf-8") as input_file:
        v_content_text = input_file.read()
        
    v_content_text = theory.CONST_yaml_RE.sub('', v_content_text)
    v_content_lines = v_content_text.splitlines()
    
    for it_idx, it_line in enumerate(v_content_lines):
        c_word_mas = split_to_words(it_line)
        if not c_word_mas:
            continue
            
        for it_word in c_word_mas:
            if not re.search(r'[а-яА-ЯëË]', it_word):
                continue
            
            if i_morphy.word_is_known(it_word):
                continue

            if it_word in c_self_dict:
                continue
                
            if it_word.lower() in c_self_dict:
                continue

            r = i_morphy.parse(it_word)
            v_variant_mas = set(it.normal_form for it in r)
            print(
                f'{i_filename}[{it_idx+1}]: {it_word} => ',
                '; '.join(v_variant_mas), 
                file=i_file_out
            )
            # pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint()
        
            break


# ----------------------------------------------------------------------------
def perform(i_src_path, i_dict_files, i_file_out, i_args):
    print(f'Spelling from "{i_src_path}" dict {i_dict_files} to "{i_file_out}"')
    v_file_list = theory.scan_src_file_list(i_src_path)
    
    # print(split_to_words("""Определим формально ключевую сущность модели - **алгоритм**. Соберëм все сведения, отмеченные в предыдущей главе. **Алгоритм** представляет собой повторимый способ изменения _связей_ и _локальных_ _параметров движений_, запуск которого обусловлен _близостью_ конечного множества _экземпляров_ _объектов_ и целиком определяется _преобразующими_ свойствами _классов_ этих _объектов_ и  предыдущими _параметрами движений_."""))
    
    c_self_dict = set()
    for it_dict_file in i_dict_files:
        if not op.exists(it_dict_file):
            continue
            
        tree = ET.parse(it_dict_file)
        root = tree.getroot()
        c_word_list = root[0][0]
        for it_word in c_word_list:
            c_self_dict.add(it_word.text)
    
    if i_args.filter:
        regex = re.compile(i_args.filter)
        v_new_file_list = []
        v_filter_file_list = []
        for it in v_file_list:
            if re.match(regex, it):
                v_new_file_list.append(it)
                v_filter_file_list.append(op.splitext(it)[0])

        if not v_filter_file_list:
            print('None to export with filter [{}]'.format(i_args.filter))
            exit(1)

        v_file_list = v_new_file_list
        print('[filtered]{' + ';'.join(v_filter_file_list) + '}')
    
    try:
        v_morphy = pymorphy3.MorphAnalyzer()
        
        with open(i_file_out, 'w') as v_file_out:
            for it_file in v_file_list:
                try:
                    parse_file(
                        i_src_path, 
                        it_file, 
                        v_morphy,
                        c_self_dict, 
                        v_file_out
                    )
                except Exception as e:
                    print(f'Error load "{it_file}" file from "{i_src_path}":\r\n\t{e}')
                
    except Exception as e:
        print(f'Error process "{i_src_path}": {e}')

