from . import theory
from .theory import FileHeader

import typing
import os
import glob
import os.path as op
import codecs
import re


# ----------------------------------------------------------------------------
CONST_FooterLink_RE = re.compile(r'\[\^([\d\-_]+?)(--|__)(.+?)\](?!:)')
CONST_UniversalFooterLink_RE = re.compile(r'\[\^(.+?)\](?!:)')
CONST_UniversalFooter_RE = re.compile(r'^\[\^(.+?)\]:')


# ----------------------------------------------------------------------------
class FileItemData:
    def __init__(
            self,
            i_lines: typing.List[str],
            i_headers: typing.List[FileHeader],
            i_def_mas: typing.List[str]
    ):
        if not i_lines:
            raise ValueError("Can't init null lines item")

        if not i_headers:
            raise ValueError("Can't init null headers item")

        self.lines = i_lines.copy()
        self.headers = i_headers.copy()
        self.definition_mas = i_def_mas.copy()
        self.footer_link_mas = []

        v_title_header = self.headers[-1]
        v_title_header.set_content()

        for it_line in self.lines:
            for it_match in CONST_UniversalFooterLink_RE.finditer(it_line):
                self.footer_link_mas.append(it_match.group(1))

    def save(self, i_dst_path: str, i_footer_map, i_main_file_item: 'FileItemData', i_index_file_item: 'FileItemData'):
        v_main_file_header = i_main_file_item.headers[-1]
        v_index_file_header = i_index_file_item.headers[-1]
        v_title_header = self.headers[-1]

        c_is_gitlab_error_bypass = len(self.footer_link_mas) > 0

        c_navigation = v_title_header.get_navigation_links(
            v_main_file_header.get_link('Содержание'),
            v_index_file_header.get_link('Индекс')
            )

        c_dst_filename = v_title_header.get_file_name()
        c_file_out = op.join(i_dst_path, c_dst_filename)

        with codecs.open(c_file_out, "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
            if c_navigation:
                output_file.write('\n'.join([
                    c_navigation + ('[^1]' if c_is_gitlab_error_bypass else ''),
                    '', ''
                ]))

            output_file.write('\n'.join(self.lines))

            if self.footer_link_mas:
                if c_is_gitlab_error_bypass:
                    output_file.write('\n[^1]: Временно необходим обход ошибки gitlab '
                                      'wiki при разборе первой сноски со списком\n')

                for it_footer_link in self.footer_link_mas:
                    if it_footer_link not in i_footer_map:
                        print(f'!!! Error: no footer {it_footer_link}')
                        continue

                    output_file.write('\n')
                    v_footer_content = i_footer_map[it_footer_link]
                    output_file.write('\n'.join(v_footer_content))


# ----------------------------------------------------------------------------
class FileData:
    filename: str
    items: typing.List[FileItemData]

    def __init__(
        self, 
        i_path, 
        i_filename,
        i_last_header: typing.Optional[FileHeader] 
    ):
        self.filename = i_filename
        self.items = []

        self.file_title, self.idx_list = theory.parse_file_name(i_filename)

        with codecs.open(op.join(i_path, i_filename), mode="r", encoding="utf-8") as input_file:
            v_content_text = input_file.read()

        v_content_text = theory.CONST_yaml_RE.sub('', v_content_text)

        v_content_lines = v_content_text.splitlines()
        v_item_lines: typing.List[str] = []
        v_item_headers: typing.List[FileHeader] = []
        v_last_header = i_last_header
        v_footer_map: typing.Dict[str, typing.List[str]] = {}
        v_definition_mapper = theory.DefinitionRE_Item()
        v_last_footer_name = None
        v_content = False

        for it_line in v_content_lines:
            if it_line.startswith('#'):
                if v_content and v_item_lines:
                    self._add_item(
                        v_item_lines, 
                        v_item_headers, 
                        v_definition_mapper.item_def_mas
                    )
                    v_item_lines.clear()
                    v_item_headers.clear()
                    v_definition_mapper.clear()
                    
                v_new_header = FileHeader(it_line, v_last_header, self.file_title)
                v_item_headers.append(v_new_header)
                v_last_header = v_new_header
                v_last_footer_name = None
                v_item_lines.append(it_line)
                v_content = False
                continue
                
            v_footer_match = CONST_UniversalFooter_RE.match(it_line)
            if v_footer_match:
                v_last_footer_name = v_footer_match.group(1)
                v_footer_map[v_last_footer_name] = []
            elif theory.CONST_TodoStart_RE.match(it_line):
                it_line = '>>>\n**Требуется**'
            elif theory.CONST_TodoEnd_RE.match(it_line):
                it_line = '>>>'
            elif theory.CONST_ImportantStart_RE.match(it_line):
                it_line = '>>>\n**Важно**'
            elif theory.CONST_ImportantEnd_RE.match(it_line):
                it_line = '>>>'
            else:
                it_line = re.sub(theory.CONST_Definition_RE, v_definition_mapper, it_line)
                it_line = re.sub(theory.CONST_Notes_RE, theory.CONST_Notes_RE_Replace, it_line)

            if it_line.strip():
                v_content = True

            if v_last_footer_name:
                v_footer_map[v_last_footer_name].append(it_line)
                continue

            v_item_lines.append(it_line)

        if v_item_lines:
            self._add_item(
                v_item_lines, 
                v_item_headers, 
                v_definition_mapper.item_def_mas
            )

        self.footer_map = v_footer_map
        self.last_header = v_last_header

    def _add_item(
            self,
            i_lines: typing.List[str],
            i_headers: typing.List[FileHeader],
            i_def_mas: typing.List[str]
    ):
        if not self.items:
            if not i_headers[-1].do_correction(self.idx_list):
                print(f'Error start header {i_headers[-1]} in file with name {self.filename} and idx:{self.idx_list}')

        if not self.items:
            if i_headers:
                i_headers[0].set_file_first_header()

        self.items.append(FileItemData(i_lines, i_headers, i_def_mas))

    def save(self, i_dst_path, i_main_file_item: FileItemData, i_index_file_item: FileItemData):
        # print('file', self.idx_list, self.file_title)

        for it_item in self.items:
            it_item.save(i_dst_path, self.footer_map, i_main_file_item, i_index_file_item)

        # if self.footer_map:
        #     for it, it_lines in self.footer_map.items():
        #         print(it, it_lines)


# ----------------------------------------------------------------------------
def produce(i_src_path, i_dst_path, i_args):
    print(f'Convert to wiki from "{i_src_path}" to "{i_dst_path}"')
    v_file_list = theory.scan_src_file_list(i_src_path)

    for f in glob.glob(os.path.join(i_dst_path, "*.md")):
        os.remove(f)

    if not os.path.exists(i_dst_path):
        os.makedirs(i_dst_path)

    v_file_data_list: typing.List[FileData] = []

    try:
        v_last_header: typing.Optional[FileHeader] = None
        for it_file in v_file_list:
            try:
                v_file_data = FileData(i_src_path, it_file, v_last_header)
                v_file_data_list.append(v_file_data)
                v_last_header = v_file_data.last_header
            except Exception as e:
                print(f'Error load "{it_file}" file from "{i_src_path}":\r\n\t{e}')

        v_main_file_item = None
        v_content = ['## Оглавление', '']
        v_def_index = theory.DefinitionSortIndex()
        for it_file_data in v_file_data_list:
            for it_item in it_file_data.items:

                v_out_header = it_item.headers[-1]
                if v_out_header.is_main():
                    v_main_file_item = it_item

                v_def_index.append(it_item.definition_mas, v_out_header)

                for it_header in it_item.headers:
                    v_header_link = it_header.get_content_link()
                    if v_header_link:
                        v_content.append(v_header_link)

        v_index_file_item = v_file_data_list[-1].items[-1]
        if theory.CONST_Index_RE.match(v_index_file_item.lines[-1]):
            v_index_file_item.lines[-1:] = v_def_index.get_index_list()

        if v_main_file_item.lines[-1] == '[TOC]':
            v_main_file_item.lines[-1:] = v_content

        for it_file_data in v_file_data_list:
            it_file_data.save(i_dst_path, v_main_file_item, v_index_file_item)
    except Exception as e:
        print(f'Error process "{i_src_path}": {e}')
