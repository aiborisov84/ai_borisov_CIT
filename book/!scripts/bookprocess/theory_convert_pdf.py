from datetime import date
import shutil
import sys
import unittest

import markdown
import typing
import os

import pymorphy3

from markdown.extensions.footnotes import FootnoteExtension
from markdown.extensions.codehilite import CodeHiliteExtension
from markdown.extensions.toc import TocExtension

from . import markdown_extension, theory

import codecs
import re
import regex
import os.path as op


c_re_mark_important_begin = r'REMARKIMPB'
c_re_mark_important_end = r'REMARKIMPE'
c_re_mark_todo_begin = r'REMARKTODOB'
c_re_mark_todo_end = r'REMARKTODOE'
c_re_mark_idx = r'REMARKIDX'
c_re_note_link = r'REMARKNONELINK'


# ----------------------------------------------------------------------------
def process_latex_content(i_latex_content: str):
    morph = pymorphy3.MorphAnalyzer()

    rare_words = [
        (
            re.compile(
                r'^локал('
                r'ь|и|ью|ей|ям|ями|ях|'
                r'ьных|ьному|ьном|ьно|ьны|ьною|ьную|ьная|ьный|ьными|ьна|ьные|ен|ьного|ьной|ьным|ьное'
                r')$'
            ),
            'локаль'
        ),
        (
            re.compile(
                r'^эвольвер('
                r'|а|у|ом|е|ы|ов|ам|ами|ах'
                r')$'
            ),
            'эвольвер'
        )
    ]
    exception_morph_form = {
        'дать': morph.parse('данные')[2],  # NOUN
    }
    fixed_morph_form = {
        'автоактивация': morph.parse('автоактивировать')[0],
        'активация': morph.parse('активировать')[0],
        'активированный': morph.parse('активировать')[0],
        'акторный': morph.parse('актор')[0],
        'близко': morph.parse('близкий')[0],
        'близкие': morph.parse('близкий')[0],
        'близость': morph.parse('близкий')[0],
        'виртуализация': morph.parse('виртуализировать')[0],
        'вредно': morph.parse('вредный')[0],
        'вред': morph.parse('вредный')[0],
        'группировка': morph.parse('группировать')[0],
        'детектор': morph.parse('детектировать')[0],
        'детекторный': morph.parse('детектировать')[0],
        'детектирование': morph.parse('детектировать')[0],
        'запоминание': morph.parse('запомнить')[0],
        'запоминать': morph.parse('запомнить')[0],
        'исполнение': morph.parse('исполнять')[0],
        'исполнимый': morph.parse('исполнять')[0],
        'исполняемый': morph.parse('исполнять')[0],
        'исполнимость': morph.parse('исполнять')[0],
        'исполнить': morph.parse('исполнять')[0],
        'классификация': morph.parse('классифицировать')[0],
        'кодовый': morph.parse('код')[0],
        'коммуникационный': morph.parse('коммуникация')[0],
        'конструктивный': morph.parse('конструкция')[0],
        'контроль': morph.parse('контролировать')[0],
        'контакт': morph.parse('контактировать')[0],
        'контактный': morph.parse('контактировать')[0],
        'контекстный': morph.parse('контекст')[0],
        'копирование': morph.parse('копировать')[0],
        'копия': morph.parse('копировать')[0],
        'насыщенный': morph.parse('насытить')[0],
        'насыщенность': morph.parse('насытить')[0],
        'ограничение': morph.parse('ограничивать')[0],
        'обособление': morph.parse('обособить')[0],
        'обособленный': morph.parse('обособить')[0],
        'обусловлено': morph.parse('обусловить')[0],
        'обусловленность': morph.parse('обусловить')[0],
        'обусловливать': morph.parse('обусловить')[0],
        'объектный': morph.parse('объект')[0],
        'организация': morph.parse('организовать')[0],
        'организационный': morph.parse('организовать')[0],
        'осмысленно': morph.parse('осмысленный')[0],
        'параметрический': morph.parse('параметр')[0],
        'перемещение': morph.parse('перемещать')[0],
        'перенос': morph.parse('переносить')[0],
        # 'полезность': morph.parse('полезно')[0],
        'полезно': morph.parse('полезный')[0],
        'польза': morph.parse('полезный')[0],
        'последовательно': morph.parse('последовательный')[0],
        'предыдущий': morph.parse('предшествовать')[0],
        'преобразование': morph.parse('преобразовать')[0],
        'признаковый': morph.parse('признак')[0],
        'программный': morph.parse('программа')[0],
        'путём': morph.parse('путь')[0],
        'развитие': morph.parse('развивать')[0],
        'развитой': morph.parse('развивать')[0],
        'развитость': morph.parse('развивать')[0],
        'резидент': morph.parse('резидентный')[0],
        'самокопирование': morph.parse('самокопировать')[0],
        'самокопируемость': morph.parse('самокопировать')[0],
        'синтез': morph.parse('синтезировать')[0],
        'символьный': morph.parse('символ')[0],
        'символический': morph.parse('символ')[0],
        'следующий': morph.parse('следовать')[0],
        'специализация': morph.parse('специализировать')[0],
        'специализированный': morph.parse('специализировать')[0],
        'соревнование': morph.parse('соревновать')[0],
        'сохранение': morph.parse('сохранять')[0],
        'сохранить': morph.parse('сохранять')[0],
        'структурно': morph.parse('структура')[0],
        'структурный': morph.parse('структура')[0],
        'текстовый': morph.parse('текст')[0],
        'торможение': morph.parse('тормозить')[0],
        'трансляция': morph.parse('транслировать')[0],
        'трансформация': morph.parse('трансформировать')[0],
        'универсализация': morph.parse('универсальный')[0],
        'упорядочивание': morph.parse('упорядочивать')[0],
        'фиксация': morph.parse('фиксировать')[0],
        'функционально': morph.parse('функционал')[0],
        'функциональный': morph.parse('функционал')[0],
        'часто': morph.parse('частый')[0],
        'шаблонный': morph.parse('шаблон')[0],
        'эволюция': morph.parse('эволюционировать')[0],
        'эволюционно': morph.parse('эволюционировать')[0],
        'эволюционный': morph.parse('эволюционировать')[0],
        'языковой': morph.parse('язык')[0],
    }
    v_term_label_set = set()

    def get_rare_words(i_word):
        for it_rare_record in rare_words:
            if it_rare_record[0].match(i_word):
                return it_rare_record[1]
        return None

    def word_to_norm_form(i_word: str, i_is_label: bool):
        norm_word = get_rare_words(i_word)
        if norm_word is not None:
            return norm_word

        p = morph.parse(i_word)[0]  # делаем анализ
        morph_word = p.normalized

        exception_morph_word = exception_morph_form.get(morph_word.word, None)
        if exception_morph_word:
            morph_word = exception_morph_word

        if i_is_label:
            if morph_word.tag.POS == 'INFN':
                # в индекс продвигается возвратный глагол, а в ссылке всегда возвратность убирается
                if morph_word.word.endswith('ся'):
                    morph_word = morph.parse(morph_word.word.removesuffix('ся'))[0]

            fixed_morph = fixed_morph_form.get(morph_word.word, None)

            # обработка выбранной формы из нескольких альтернативных
            if fixed_morph is not None:
                morph_word = fixed_morph

            if morph_word.tag.POS == 'NOUN':
                if morph_word.word.endswith('ость'):
                    morph_word = morph.parse(morph_word.word.removesuffix('ость')+'ый')[0]
                    morph_word = morph_word.normalized
        else:
            # в индекс продвигается причастие, а в ссылке всегда инфинитив
            if p.tag.POS == 'PRTF':
                morph_word = p.inflect({'masc', 'sing', 'nomn'})

        return morph_word.word

    def term_to_label(i_str: str):
        c_main_term_prefix = 'MT'
        c_term_prefix = 'T'

        o_res = i_str.lower()
        # удаление удаления из ссылки на термин {я́ => я}
        o_res = o_res.replace(r'́', '')
        o_res = o_res.replace(r'\textbf{', ' ' + c_main_term_prefix + ' ')
        o_res = o_res.replace(r'\emph{', ' ' + c_term_prefix + ' ')
        o_res = re.compile(r'\\[^{]+{').sub('', o_res)
        o_res = o_res.replace(r'}', '')
        lst = o_res.split()
        words = []
        for word in lst:
            if word == c_main_term_prefix or word == c_term_prefix:
                words.append(word)
            else:
                norm_word = word_to_norm_form(word, True)
                words.append(norm_word)
        return ' '.join(words)

    def term_target(i_str: str, i_exists: bool):
        c_suffix = '' if i_exists else '*'
        o_res = i_str.replace(r'\textbf', r'\mainterm' + c_suffix)
        o_res = o_res.replace(r'\emph', r'\term' + c_suffix)
        o_res = o_res.replace('==', '')
        return o_res

    def term_strip(i_str: str):
        o_res = i_str.replace(r'\textbf{', '')
        o_res = o_res.replace(r'\emph{', '')
        o_res = o_res.replace('}', '')
        return o_res

    def word_list_to_normal_from(i_word_list):
        if len(i_word_list) == 1:
            word = i_word_list[0]
            norm_word = word_to_norm_form(word, False)
            return [norm_word]
        if len(i_word_list) == 2:
            p1 = morph.parse(i_word_list[0])[0]
            p2 = morph.parse(i_word_list[1])[0]
            if p1.tag.POS in ['ADJF', 'ADJS'] and p2.tag.POS == 'NOUN':
                p1n = p1.normalized
                p2n = p2.normalized
                return [p1n.inflect({p2.tag.gender}).word, p2n.word]

        return i_word_list

    def term_normalize(i_term_str: str):
        term_tag = re.compile(r'^(\\(?:mainterm|term){)?(.*?)(})?$')
        term_mas = i_term_str.split()
        match_mas = [
            [
                it_group if it_group else ''
                for it_group in term_tag.match(it_word).groups()
            ]
            for it_word in term_mas
        ]

        if match_mas:
            word_mas = [
                it_term[1]
                for it_term in match_mas
            ]
            word_mas = word_list_to_normal_from(word_mas)
            word_mas[0] = word_mas[0].capitalize()
            for it_word, it_match in zip(word_mas, match_mas):
                it_match[1] = it_word

        term_mas = [
            ''.join(it_match)
            for it_match in match_mas
        ]

        return ' '.join(term_mas)

    def term_to_index(i_str: str):
        o_res = i_str.strip()
        sub_term = re.compile(r'^(.*)==(.*)==(.*)$')
        c_match = sub_term.match(o_res)
        if c_match:
            return term_normalize(
                term_strip(c_match.group(1)) +
                term_target(c_match.group(2), True) +
                term_strip(c_match.group(3))
            )

        return term_normalize(term_target(i_str, True))

    def do_term_link(i_str: str):
        c_term_label = term_to_label(i_str)
        c_def_exists = c_term_label in v_term_label_set
        c_term_str = term_target(i_str, c_def_exists)

        if not c_def_exists:
            return c_term_str

        return r'\protect\hyperlink{{idx:{0}}}{{\samepage{1}}}'.format(
            c_term_label,
            c_term_str
        )

    def do_term_def(i_str: str):
        sub_term = re.compile(r'^(.*)==(.*)==(.*)$')
        c_match = sub_term.match(i_str)
        if c_match:
            c_term_label = term_to_label(c_match.group(2))
            c_def_string = (
                    term_strip(c_match.group(1)) +
                    term_target(c_match.group(2), True) +
                    term_strip(c_match.group(3))
            )
        else:
            c_term_label = term_to_label(i_str)
            c_def_string = term_target(i_str, True)

        v_term_label_set.add(c_term_label)

        return (
                r'\hypertarget{{idx:{1}}}' +
                r'{{\hyperlink{{midx:{1}}}{{\underline{{\smash{{{0}}}}}}}}}' +
                r'\index{{\hypertarget{{midx:{1}}}{{{2}}}}}'
        ).format(
            c_def_string,
            c_term_label,
            term_to_index(i_str.lower())
        )

    def do_process_table(i_str: str):
        i_str = re.compile(r'(\\\\ *)\n(?!\\(?:mid|bottom)rule)').sub(
            r'\\\\ \\addlinespace[5pt] \\hline \\addlinespace[5pt]\n',
            i_str
        )

        return i_str

    def do_section_link(i_str: str):
        i_str = re.compile(r'\\[^{]+{').sub('', i_str)
        i_str = i_str.replace(r'}', '')
        return 'sec:' + ('_'.join(i_str.split()))

    c_replacements_latex = [
        (re.compile(r'\\includegraphics{(.*?)\.svg}'), r'\\includesvg{\1}'),
        (re.compile(r'\n\n\\\['), r'\n\\['),
        # (re.compile(r'я́'), r"\\'{я}"),
        # (re.compile(r'о́'), r"\\'{о}"),
        (
            re.compile(r'(\\(?:sub)?section){(.*)}'),
            lambda x: x.group(1) + '{' + x.group(2) + '}\n\\label{' + do_section_link(x.group(2)) + '}'
        ),
        (
            # re.compile(r'\\href{(?!https://)(.*?\.md)}{(.*?)}'),
            regex.compile(r'\\href{(?!https://)(.*?\.md)}({([^}{]*(?:(?2)[^}{]*)*)})'),
            lambda x: r'\hyperref[' + do_section_link(x.group(3)) +
                      ']{\\protect \\_emph{<<' + x.group(3) + '>>}}'
        ),
        (
            re.compile(r'\+\+([^+\r]*)\+\+'),
            lambda x: do_term_def(x.group(1))
        ),
        (re.compile(re.escape(r':smile:')), r'\\smiley'),
        (re.compile(re.escape(c_re_mark_important_begin)),
         r'\\begin{important}' + '\n' + r'{\\textimportant{Важно !!!}}' + '\n\n'),
        (re.compile(re.escape(c_re_mark_important_end)), r'\\end{important}'),
        (re.compile(re.escape(c_re_mark_todo_begin)),
         r'\\begin{todo}' + '\n' + r'{\\texttodo{Требуется}}' + '\n\n'),
        (re.compile(re.escape(c_re_mark_todo_end)), r'\\end{todo}'),
        (re.compile(re.escape(c_re_mark_idx)), r''),
        (
            # re.compile(r'\\(textbf|emph){[^{}]+}'),
            regex.compile(r'\\(?:textbf|emph)({[^}{]*(?:(?1)[^}{]*)*})'),
            lambda x: do_term_link(x.group(0))
        ),
        (
            re.compile(r'(?<=\\begin{longtable}).*?(?=\\end{longtable})', re.DOTALL),
            lambda x: do_process_table(x.group(0))
        ),
        (re.compile(re.escape(r'\_textbf')), r'\\textbf'),
        (re.compile(re.escape(r'\_emph')), r'\\emph'),
        (re.compile(c_re_note_link + '(.*?)' + c_re_note_link), r'\\textbf{Внешняя ссылка на записи: }<<\1>>'),
        (re.compile(re.escape(r'\begin{longtable}')), r'\\begin{tabular}'),
        (re.compile(re.escape(r'\end{longtable}')), r'\\end{tabular}'),
        (re.compile(re.escape(r'\endhead')), r''),
    ]

    for it in c_replacements_latex:
        i_latex_content = it[0].sub(it[1], i_latex_content)

    return i_latex_content


# ----------------------------------------------------------------------------
def produce(i_src_path, i_dst_path, i_export_file_prefix, i_args):
    print(f'Start export from "{i_src_path}" to "{i_dst_path}"')

    v_file_list = theory.scan_src_file_list(i_src_path)

    v_file_filter_infix = ''
    if i_args.filter:
        regex = re.compile(i_args.filter)
        v_new_file_list = []
        v_filter_file_list = []
        for idx, it in enumerate(v_file_list):
            if idx == 0:
                v_new_file_list.append(it)
                continue

            if re.match(regex, it) or idx == 0:
                v_new_file_list.append(it)
                v_filter_file_list.append(op.splitext(it)[0])

        if not v_filter_file_list:
            print('None to export with filter [{}]'.format(i_args.filter))
            exit(1)

        v_file_list = v_new_file_list
        v_file_filter_infix = '[filtered]{' + ';'.join(v_filter_file_list) + '}'

    c_dst_filename = i_export_file_prefix + v_file_filter_infix
    c_dst_file_path = op.join(i_dst_path, c_dst_filename)
    c_file_ext = '.html'

    if not i_args.is_override:
        v_suffix = ''
        v_num = 1
        while op.exists(c_dst_file_path + v_suffix + c_file_ext):
            v_suffix = f'.{v_num}'
            v_num += 1

        c_dst_file_path += v_suffix

    c_file_out = c_dst_file_path + c_file_ext

    v_definition_index = theory.DefinitionRE_GlobalIndex()
    c_replacements = [
        (theory.CONST_ImportantStart_RE,
         '<div class="important" markdown="1"> <b><font color="green">Важно !!!</font></b>'),
        (theory.CONST_ImportantEnd_RE, '</div>'),
        (theory.CONST_TodoStart_RE,
         '<div class="todo" markdown="1"> <b><font style="color:hsl(60, 69%, 35%)">Требуется</font></b>'),
        (theory.CONST_TodoEnd_RE, '</div>'),
        (theory.CONST_Definition_RE, v_definition_index.items),
        (theory.CONST_Index_RE, v_definition_index),
        (theory.CONST_Notes_RE, theory.CONST_Notes_RE_Replace)
    ]
    v_text_list = []
    v_raw_text_list = []
    v_last_header: typing.Optional[theory.FileHeader] = None

    for it_file in v_file_list:
        v_file_title, _ = theory.parse_file_name(it_file)

        with codecs.open(op.join(i_src_path, it_file), mode="r", encoding="utf-8") as input_file:
            v_content_text = input_file.read()

        v_content_text = theory.CONST_yaml_RE.sub('', v_content_text)
        v_content_lines = v_content_text.splitlines()

        if i_args.is_full_md or i_args.is_prepare_latex:
            v_raw_text_list.append(v_content_text)

        if v_text_list:
            v_text_list.append('')

        for it_line in v_content_lines:
            if it_line.startswith('#'):
                v_last_header = theory.FileHeader(it_line, v_last_header, v_file_title)
                v_definition_index.items.set_header_caption(v_last_header.str)

            for it in c_replacements:
                it_line = re.sub(it[0], it[1], it_line)

            v_text_list.append(it_line)

    v_full_md = '\n\n'.join(v_raw_text_list)
    if i_args.is_full_md:
        with codecs.open(c_dst_file_path + ".md", "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
            output_file.write(v_full_md)

    if i_args.is_prepare_latex:
        c_dst_latex_md = c_dst_file_path + "_pre_latex.md"

        c_replacements_md_latex = [
            (re.compile(r'(?m)^```math([^`]*?)^```', flags=re.DOTALL), r'$$\1$$'),
            (re.compile(r'(?m)^Лицензия([^#]*?)^\[TOC\]', flags=re.DOTALL), r''),
            (re.compile(r'\$`? *(.*?) *`?\$', flags=re.DOTALL), r'$\1$'),
            (re.compile(re.escape(r'\mathscr')), r'\\mathcal'),
            (re.compile(r'\\mathbb\{([a-z][^\}]*)\}'), r'{\1}'),
            (re.compile(r'\\mathbb\{(.)([^\}]*)\}'), r'{\\mathbb{\1}\2}'),
            (theory.CONST_ImportantStart_RE, c_re_mark_important_begin),
            (theory.CONST_ImportantEnd_RE, c_re_mark_important_end),
            (theory.CONST_TodoStart_RE, c_re_mark_todo_begin),
            (theory.CONST_TodoEnd_RE, c_re_mark_todo_end),
            (re.compile(re.escape(r'\underset')), r'\\smallunderset'),
            (re.compile(r'(?m)^## [^\n]*Индекс(.|\n)*\[IDX\]', flags=re.DOTALL), c_re_mark_idx),
            (theory.CONST_Notes_RE, c_re_note_link + theory.CONST_Notes_RE_Replace + c_re_note_link)
        ]

        for it in c_replacements_md_latex:
            v_full_md = it[0].sub(it[1], v_full_md)

        with codecs.open(c_dst_latex_md, "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
            output_file.write(v_full_md)

        print('Prepare LaTeX done')

        c_dst_latex = c_dst_file_path + '.tex'
        c_alm_pandoc_path = r'f:\alm\bin\pandoc''\\'
        c_pandoc_path = c_alm_pandoc_path if op.exists(c_alm_pandoc_path) else ''
        c_pandoc_bin = r'pandoc' + (r'.exe' if sys.platform == "win32" else '')
        c_current_path = os.path.dirname(__file__)
        c_file_latex_template = op.join(c_current_path, 'latex_template.tex')
        c_file_latex_metafile = op.join(c_current_path, 'latex_metadata.yaml')
        c_dir_latex_extract_media = op.join(i_dst_path, 'Data')
        c_dir_bibliography = op.normpath(op.join(i_src_path, r'..'))
        c_dir_latex_resource = i_src_path

        shutil.copyfile(op.join(c_dir_bibliography, r'book.bib'), op.join(i_dst_path, r'book.bib'))

        if os.path.exists(c_dst_latex):
            os.unlink(c_dst_latex)

        import locale
        locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
        c_latex_date_str = date.today().strftime("%d %B %Y")

        c_pandoc_cmd = (
                c_pandoc_path + c_pandoc_bin + ' ' +
                r'--template=' + c_file_latex_template + ' ' +
                r'--metadata-file=' + c_file_latex_metafile + ' ' +
                r'--shift-heading-level-by=-1 ' +
                r'--wrap=none ' +
                r'-s -f markdown-auto_identifiers -t latex ' +
                r'--biblatex --bibliography=book.bib ' +
                r'--extract-media=' + c_dir_latex_extract_media + ' ' +
                r'--resource-path=' + c_dir_latex_resource + (';' if sys.platform == "win32" else ':') + c_dir_bibliography + ' ' +
                ('' if sys.platform == "win32" else '-M alm-os-linux ') +
                f'-V "date={c_latex_date_str}" ' +
                c_dst_latex_md+' ' +
                rf'-o {c_dst_latex}'
        )

        print(c_pandoc_cmd)
        os.system(c_pandoc_cmd)

        if not os.path.exists(c_dst_latex):
            print('!!! Fail pandoc convert\n\tfrom [{}]\n\tto [{}]'.format(c_dst_latex_md, c_dst_latex))
        else:
            shutil.copyfile(op.join(c_dir_bibliography, r'book.bib'), op.join(i_dst_path, r'book.bib'))

            with codecs.open(c_dst_latex, mode="r", encoding="utf-8") as input_file:
                v_text_content_tex = input_file.read()

            v_text_content_tex = process_latex_content(v_text_content_tex)

            with codecs.open(c_dst_latex, "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
                output_file.write(v_text_content_tex)

            print('Exported [{}]'.format(c_dst_latex))

    v_full_text = '\n'.join(v_text_list)
    print('Prepare Html done')

    md = markdown.Markdown(
        extensions=[
            'admonition',
            'extra',
            'nl2br',
            'sane_lists',
            TocExtension(title="Содержание", toc_depth="2-4"),
            CodeHiliteExtension(guess_lang=False),
            FootnoteExtension(BACKLINK_TITLE="Вернуться к сноске %d в тексте"),
            markdown_extension.BookExtension()
        ],
        tab_length=2
    )
    v_html_content = md.convert(v_full_text)
    md.reset()

    with codecs.open(c_file_out, "w", encoding="utf-8", errors="xmlcharrefreplace") as output_file:
        output_file.write(G_HtmlHeader1)
        if i_args.local_script:
            output_file.write(G_HtmlHeader_LocalScript)
        else:
            output_file.write(G_HtmlHeader_GlobalScript)
        output_file.write(G_HtmlHeader2)
        output_file.write(v_html_content)
        output_file.write(G_HtmlFooter)

    print('Exported [{}]'.format(c_file_out))



# ----------------------------------------------------------------------------
G_HtmlHeader1 = r"""<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
"""
G_HtmlHeader_LocalScript = r"""
  <link rel="stylesheet" href="katex/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">
  <script src="katex/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>
"""
G_HtmlHeader_GlobalScript = r"""
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>
"""
G_HtmlHeader2 = r"""
  <style>
    html{font-family:Arial,Helvetica; word-wrap: break-word; line-height: 150%; text-align: justify}
    p {text-indent: 2em}
    li p{text-indent: 0em}
    strong, em{color:#b58900}
    @media print {
      h2 {page-break-before:always}
      table, div.toc, .important, .todo, .codehilite {page-break-inside: avoid}
      h1,h2,h3,h4,h5,h6{page-break-after: avoid}
    }
    h1 { text-align: center }
    h1,h2,h3,h4,h5,h6{line-height:150%; color:#268bd2}
    a {text-decoration-style: dotted}
    .toctitle {line-height:150%; color:#268bd2; font-weight:bold; font-size: 1.17em}
    h2,h3,h4,h5,h6{font-size: 1.17em}
    table{width:100%;}
    table,th,td{border-collapse:collapse; padding:10px; vertical-align: top; border:1px solid #93a1a1;}
    noheader th{display:none;}
    th{background:#eee8d5;}
    tbody tr:nth-child(even) {background:#eee8d5;}
    img {max-width:100%; display: block; height: auto; margin-left: auto; margin-right: auto;}
    div.toc a:link {color: color:#268bd2; text-decoration: none;}
    div.toc a:hover {color: Black; text-decoration: underline; }
    div.toc ul {list-style: none;}
    div.toc > ul {margin-left:0px; padding-left: 20px;}
    .important { margin: 5px; padding: 0px 20px 0px 20px;    background-color:hsl(120, 50%, 95%);border:1px solid hsl(120, 50%, 85%); }
    .todo {      margin: 5px; padding: 0px 20px 0px 20px;    background-color:hsl(60, 68%, 95%); border:1px solid hsl(60, 68%, 85%); }
    .codehilite {margin: 5px; padding: 0px 20px 0px 20px;    background-color:hsl( 0,  0%, 95%); border:1px solid hsl( 0,  0%, 85%); }
    .important, .todo, .codehilite {border-radius:10px; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);}
    .katex .katex-html>.newline {height: 1em;}
    .codehilite .hll { background-color: #ffffcc }
    .codehilite  { background: #f8f8f8; }
    .codehilite .c { color: #408080; font-style: italic } /* Comment */
    .codehilite .err { border: 1px solid #FF0000 } /* Error */
    .codehilite .k { color: #008000; font-weight: bold } /* Keyword */
    .codehilite .o { color: #666666 } /* Operator */
    .codehilite .ch { color: #408080; font-style: italic } /* Comment.Hashbang */
    .codehilite .cm { color: #408080; font-style: italic } /* Comment.Multiline */
    .codehilite .cp { color: #BC7A00 } /* Comment.Preproc */
    .codehilite .cpf { color: #408080; font-style: italic } /* Comment.PreprocFile */
    .codehilite .c1 { color: #408080; font-style: italic } /* Comment.Single */
    .codehilite .cs { color: #408080; font-style: italic } /* Comment.Special */
    .codehilite .gd { color: #A00000 } /* Generic.Deleted */
    .codehilite .ge { font-style: italic } /* Generic.Emph */
    .codehilite .gr { color: #FF0000 } /* Generic.Error */
    .codehilite .gh { color: #000080; font-weight: bold } /* Generic.Heading */
    .codehilite .gi { color: #00A000 } /* Generic.Inserted */
    .codehilite .go { color: #888888 } /* Generic.Output */
    .codehilite .gp { color: #000080; font-weight: bold } /* Generic.Prompt */
    .codehilite .gs { font-weight: bold } /* Generic.Strong */
    .codehilite .gu { color: #800080; font-weight: bold } /* Generic.Subheading */
    .codehilite .gt { color: #0044DD } /* Generic.Traceback */
    .codehilite .kc { color: #008000; font-weight: bold } /* Keyword.Constant */
    .codehilite .kd { color: #008000; font-weight: bold } /* Keyword.Declaration */
    .codehilite .kn { color: #008000; font-weight: bold } /* Keyword.Namespace */
    .codehilite .kp { color: #008000 } /* Keyword.Pseudo */
    .codehilite .kr { color: #008000; font-weight: bold } /* Keyword.Reserved */
    .codehilite .kt { color: #B00040 } /* Keyword.Type */
    .codehilite .m { color: #666666 } /* Literal.Number */
    .codehilite .s { color: #BA2121 } /* Literal.String */
    .codehilite .na { color: #7D9029 } /* Name.Attribute */
    .codehilite .nb { color: #008000 } /* Name.Builtin */
    .codehilite .nc { color: #0000FF; font-weight: bold } /* Name.Class */
    .codehilite .no { color: #880000 } /* Name.Constant */
    .codehilite .nd { color: #AA22FF } /* Name.Decorator */
    .codehilite .ni { color: #999999; font-weight: bold } /* Name.Entity */
    .codehilite .ne { color: #D2413A; font-weight: bold } /* Name.Exception */
    .codehilite .nf { color: #0000FF } /* Name.Function */
    .codehilite .nl { color: #A0A000 } /* Name.Label */
    .codehilite .nn { color: #0000FF; font-weight: bold } /* Name.Namespace */
    .codehilite .nt { color: #008000; font-weight: bold } /* Name.Tag */
    .codehilite .nv { color: #19177C } /* Name.Variable */
    .codehilite .ow { color: #AA22FF; font-weight: bold } /* Operator.Word */
    .codehilite .w { color: #bbbbbb } /* Text.Whitespace */
    .codehilite .mb { color: #666666 } /* Literal.Number.Bin */
    .codehilite .mf { color: #666666 } /* Literal.Number.Float */
    .codehilite .mh { color: #666666 } /* Literal.Number.Hex */
    .codehilite .mi { color: #666666 } /* Literal.Number.Integer */
    .codehilite .mo { color: #666666 } /* Literal.Number.Oct */
    .codehilite .sa { color: #BA2121 } /* Literal.String.Affix */
    .codehilite .sb { color: #BA2121 } /* Literal.String.Backtick */
    .codehilite .sc { color: #BA2121 } /* Literal.String.Char */
    .codehilite .dl { color: #BA2121 } /* Literal.String.Delimiter */
    .codehilite .sd { color: #BA2121; font-style: italic } /* Literal.String.Doc */
    .codehilite .s2 { color: #BA2121 } /* Literal.String.Double */
    .codehilite .se { color: #BB6622; font-weight: bold } /* Literal.String.Escape */
    .codehilite .sh { color: #BA2121 } /* Literal.String.Heredoc */
    .codehilite .si { color: #BB6688; font-weight: bold } /* Literal.String.Interpol */
    .codehilite .sx { color: #008000 } /* Literal.String.Other */
    .codehilite .sr { color: #BB6688 } /* Literal.String.Regex */
    .codehilite .s1 { color: #BA2121 } /* Literal.String.Single */
    .codehilite .ss { color: #19177C } /* Literal.String.Symbol */
    .codehilite .bp { color: #008000 } /* Name.Builtin.Pseudo */
    .codehilite .fm { color: #0000FF } /* Name.Function.Magic */
    .codehilite .vc { color: #19177C } /* Name.Variable.Class */
    .codehilite .vg { color: #19177C } /* Name.Variable.Global */
    .codehilite .vi { color: #19177C } /* Name.Variable.Instance */
    .codehilite .vm { color: #19177C } /* Name.Variable.Magic */
    .codehilite .il { color: #666666 } /* Literal.Number.Integer.Long */
  </style>
</head>
<body>
"""

# ----------------------------------------------------------------------------
G_HtmlFooter = r"""
<script>
(() => {
    var mathElems = document.getElementsByClassName("katex");
    var elems = [];
    for (const i in mathElems) {
        if (mathElems.hasOwnProperty(i)) elems.push(mathElems[i]);
    }
    elems.forEach(elem => {
        katex.render(elem.textContent, elem, { throwOnError: false, displayMode: elem.nodeName !== 'SPAN', });
    });
})();
</script>
</body>
</html>
"""


# ==============================
class TestPrePdfMethods(unittest.TestCase):
    class ArgTmp:
        def __init__(self, i_is_override, i_filter, i_is_full_md, i_local_script):
            self.is_override = i_is_override
            self.filter = i_filter
            self.is_full_md = i_is_full_md
            self.local_script = i_local_script
            self.is_prepare_latex = True

    def test_do(self, i_arg: ArgTmp):
        import os
        import datetime

        c_book_path = op.normpath(op.join(op.dirname(__file__), '../..'))
        c_src_path = op.join(c_book_path, "BookTheory")
        c_dst_path = op.normpath(op.join(c_book_path, '..', 'out', '.export'))
        os.makedirs(c_dst_path, exist_ok=True)

        c_file_name = 'test_' + datetime.datetime.now().strftime('%Y%m%d')

        produce(
            c_src_path,
            c_dst_path,
            c_file_name,
            i_arg
        )

    def test_do_full(self):
        self.test_do(TestPrePdfMethods.ArgTmp(True, None, True, False))

    def test_do_intro(self):
        self.test_do(TestPrePdfMethods.ArgTmp(True, "1_", True, False))
