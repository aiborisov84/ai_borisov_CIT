# Общая теория алгоритмов

## Работа

Лицензия:
```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```
Полный текст лицензии ([LICENSE](/LICENSE.txt))

Авторы:

- Борисов Алексей Игоревич (aiborisov84@gmail.com).
([Доска публикаций Хабр](https://habr.com/ru/users/ai_borisov/))

## Записи20200111

- Формализация алгоритма (условие, инвариант, изменение)

![Разделение и общее в примерах термина](20200111-0001.jpg)