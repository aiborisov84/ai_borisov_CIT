---
title: Общая теория алгоритмов
author: Борисов Алексей Игоревич (aiborisov84@gmail.com)
description: Внутренний документ
prerender: |
<important>
<div class="important"> <b><font color="green">Важно !!!</font></b>
</important>
</div>
<todo>
<div class="todo"> <b><font style="color:hsl(60, 69%, 35%)">Требуется</font></b>
</todo>
</div>
...

<style>
.important {padding: 10px 10px 10px 10px;background-color:hsl(120, 50%, 95%);border:1px solid hsl(120, 50%, 95%); border-radius:10px; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);}
.todo {padding: 2px 4px 0px 4px;background-color:hsl(60, 68%, 95%);border:1px solid hsl(60, 68%, 95%); border-radius:4px; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);}
</style>

# Моделирование процесса запоминания и обучения

<todo>

Постановка задачи:

- среда (с заданными законами)
- базовые операции **эвольвера**
- базовые рецепторы **эвольвера**
- оптимизируемая целевая функция (назначение **эвольверу** понятия _полезно_)

Решение:

- некоммуницирующая группа эвольверов с начальными способностями, необходимыми для генерации новых алгоритмов
- эволюционные этапы поиска алгоритма с отсевом неудачных

Требуемый результат:

- автоматическое нахождение поведения, обеспечивающего выживание **эвольвера**, то есть алгоритма приведения целевой функции к оптимуму

Пример реализации:

- Робот пылесос с одной розеткой и лабиринт

Подзадачи:

1. Выполнить моделирование **цепочки**(подсистемы **эвольвера**):

- запоминающей составной **символ**, 
- распознающей последующие появления этого **символа**,
- подбирающей _полезное_ действие для ситуации, характеризуемой детектируемым **символом**.

2. Среда выполнения моделирования Android, Python, Qml

</todo>

## Ограничения на среду выполнения

Был необходим переход Qt&C++&PythonPlugins (компьютер) на Qml&Python (смартфон Android).

- Использование git

**!!!Важно** Проект (OpenSource Python) [AiPy.bitbucket](https://bitbucket.org/aibor/aipy)

![image](./Screenshot_2019-04-25-20-17-58-738.jpeg)

- Pydroid 3

![image](./Screenshot_2019-04-25-20-09-15-195.jpeg)

![image](./Screenshot_2019-04-25-20-09-53-830.jpeg)

- переход qt на qml

![image](./Screenshot_2019-04-25-20-07-32-427.jpeg)

![image](./Screenshot_2019-04-25-20-08-41-026.jpeg)

- переход qchart на текст

![image](./Screenshot_2019-04-25-20-11-22-169.jpeg)

## Моделирование взаимодействия среды и эвольвера**

- Построение модели взаимодействия

![image](./win_Net.jpg)

- Просмотр реакции и состояния эвольвера

![image](./win_Layer.jpg)

- Контроль состояния среды 

![image](./win_Environment.jpg)

## Моделирование входного сигнала

## Моделирование процесса запоминания

## Отождествление сигнала

- Повторение узора входного сигнала приводит к аналогичному выходному сигналу эвольвера

![image](./model_1.jpg)

- Разные узоры входного сигнала приводят к разным выходным сигналам эвольвера

![image](./model_2.jpg)

- При обучении комплексному узору входного сигнала впоследствии автоматически производится разбиение по составляющим

![image](./model_3.jpg)

![image](./00_SelectModel.png)
![image](./01_Long_pattern_detect.png)
![image](./02_Long_same_signal.png)
![image](./03_Action_chain.png)
![image](./04_1_Labyrinth.png)
![image](./04_2_Labyrinth.png)
![image](./05_1_Flat_field_analyse.png)
![image](./05_2_Flat_field_analyse.png)
![image](./05_3_Flat_field_analyse_move.png)
![image](./05_4_Flat_field_analyse_border.png)
![image](./05_5_Flat_field_analyse_attention.png)
![image](./05_6_Flat_field_analyse_attention_control.png)

## Подбор действия

## Вывод

## Отзывы