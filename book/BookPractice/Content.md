# Приложения и практические статьи

## Работа

Лицензия:
```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```
Полный текст лицензии ([LICENSE](/LICENSE.txt))

Авторы:

- Борисов Алексей Игоревич (aiborisov84@gmail.com).
([Доска публикаций Хабр](https://habr.com/ru/users/ai_borisov/))

## Статьи в хронологическом порядке

### Статья №0 (Развитие архитектуры ПО)

Приложение к главе [6.1.3. Эволюция программы](/book/BookTheory/6_1_3_CodeOrganizationEvolution.html)

- [Как не понимать принципы развития архитектуры SOLID](./article_000_ArchitectureDesign/content.md)

### Статья №1 (Введение в тему)

Приложение к главе [1. Введение](/book/BookTheory/1_Intro.md)

- Вариант 1: [Как опубликовать теорию в современном IT-мире (пособие для чайников)](./article_001_Intro/content.md)
- Конечный вариант: [Разрабатываем теорию алгоритмов как проект с открытым исходным кодом](./article_001_Intro/content_v1.md)

### Статья №2 (Коммуникация, язык и цепочки)

Приложение к главе [4.2.2. Виртуализация](/book/BookTheory/4_2_2_Virtualization.md)

- Алгоритм ([Chain analyse method](./article_002_Internal_Chain_analyse_method/content.md)) "Способ анализа образования **цепочки**"
- Черновик статьи: [Как в языке сформировать существительное? Сигнал("Видел мамонта")](./article_002_Internal_Chain_analyse_method/content_v1.md)

### Статья №3 (Эволюция кода и ООП)

Приложение к главе [6.1.3. Эволюция программы](/book/BookTheory/6_1_3_CodeOrganizationEvolution.html)

- [Эволюция программного проекта и ООП](./article_003_ObjectOrientedProgramming/content.md)

### Статья №4 (Определение алгоритма)

Приложение к главе [2.2. Определение алгоритма](/book/BookTheory/2_2_0_Algorithm.html)

- Черновик статьи: [Что такое алгоритм?! (часть 1) "Действие"](./article_004_WhatIsAlgorithm/part1.md)
- Черновик статьи: [Что такое алгоритм?! (часть 2) "Обусловленная и связная последовательность"](./article_004_WhatIsAlgorithm/part2.md)
- Черновик статьи: [Что такое алгоритм?! (часть 3) "Синтез алгоритма запоминанием"](./article_004_WhatIsAlgorithm/part3_0.md)
- Черновик статьи: [Что такое алгоритм?! (часть 3.1) "Эволюция памяти"](./article_004_WhatIsAlgorithm/part3_1.md)
- Черновик статьи: [Что такое алгоритм?! (часть 3.14) "Иерархия памяти"](./article_004_WhatIsAlgorithm/part3_14.md)
- Черновик статьи: [Что такое алгоритм? ?? Часть 3 1/4 "Коммуникация"](./article_004_WhatIsAlgorithm/part3_25.md)

### Статья №5 (Моделирование синтеза запоминанием)

Приложение к главе [4.1.2. Запоминание](/book/BookTheory/4_1_2_Memorizing.md)

- Алгоритм ([Python](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/wikis/home)) "Моделирование **цепочек**"
- Черновик статьи: [Моделирование процесса запоминания и обучения](./article_005_ChainModel_PythonQmlAndroid/content.md)

### Статья №6 (Моделирование процесса обучения)

Приложение к главе [4.2.1. Обучение](/book/BookTheory/4_2_1_0_Learning.md)

- Черновик статьи: [Детская сказка программисту на ночь](./article_006_LearningAlgorithm/part1_fairytale.md)
- Черновик статьи: [Алгоритмы обучения или зачем мозгу смех?](./article_006_LearningAlgorithm/part2_laugh.md)